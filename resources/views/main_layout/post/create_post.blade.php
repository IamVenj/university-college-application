@extends ('main_layout.layout_uno.index')



@section('info_post_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create Post</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create Post

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/post" method="post" enctype="multipart/form-data">

	                  		@csrf

	                  		<div class="row">

	                  			<div class="col-md-6">

				                    <div class="form-group">

				                      	<label for="post_Title" style="font-size: 20px;">Course Name / Admission Notice Title</label>

				                      	<input type="text" class="form-control" id="post_Title" name="title" placeholder="Title" required>

				                    </div>

				                </div>

				                <div class="col-md-6">

				                    <div class="form-group">
		                                  
		                                <label style="font-size: 20px;">Information Type</label>
		                        
		                                <select name="info_type" class="form-control">
		            
		                                    <option selected disabled>Choose Type</option>
		                
		                                    <option value="admission_notice">Admission Notice</option>
		                
		                                    <option Value="available_courses">Available Courses</option>
							                        
		                                </select>
		                                
		                            </div>

		                        </div>

                        	</div>

		                    <div class="form-group">

		                      	<label for="post_description" style="font-size: 20px;">Description</label>

		                      	<textarea type="text" class="form-control" id="post_description" rows='5' name="post_description" placeholder="Description" required></textarea>

		                    </div>

		                    <div class="row">


		                    	<div class="col-md-2 ">

		                    		<button type="submit" class="btn btn-gradient-info mr-2" style="font-size: 18px;">Post<i class="fa fa-check-circle ml-2"></i></button>

		                    	</div>


		                    </div>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.post.edit_infoPost')

    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>

@endsection