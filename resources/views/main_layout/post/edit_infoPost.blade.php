<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title" style="font-size: 20px;">Created Posts</h4>
    
        <p class="card-description">
    
          Number of Created Posts: <code>{{$info_post->count()}}</code>
    
        </p>
    
        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

        <div class="table-responsive">

          <table class="table table-hover" id="dataTables-example">
      
            <thead>
      
              <tr>
      
                <th style="font-size: 18px;">Title</th>

                <th style="font-size: 18px;">Description</th>

                <th style="font-size: 18px;">Information Type</th>
      
                <th></th>

                <th></th>
      
              </tr>
      
            </thead>
      
            <tbody>
      
              @foreach($info_post as $post)

              <tr>

                @include('main_layout.modals.infoPostModal')
      
                <td style="font-size: 15px;">{{ $post->title }}</td>

                <td style="font-size: 15px;">{{ $post->description }}</td>

                <td style="font-size: 15px;">{{ $post->info_type }}</td>

                <td style="font-size: 17px;"><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#edit_info_Modal'.$post->id; ?>>Edit</button></td>

                <td style="font-size: 17px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_info_Modal'.$post->id; ?>>Delete</button></td>
                
              </tr>

              @endforeach
      
            </tbody>
      
          </table>
    
        </div>

      </div>
    
    </div>

  </div>

</div>