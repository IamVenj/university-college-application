<div id="father_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="father_first_name" class="form-control form-control-sm" placeholder="Fathers' First Name"/>
          
        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="father_middle_name" class="form-control form-control-sm" placeholder="Fathers' Middle Name"/>

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="father_last_name" class="form-control form-control-sm" placeholder="Fathers' Last Name"/>

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          <input type="email" name="father_email" class="form-control form-control-sm" placeholder="Fathers' Email"/>

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          <input type="text" name="father_phone_number" class="form-control form-control-sm" placeholder="Fathers' Phone Number"/>

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          <input type="text" name="father_address" class="form-control form-control-sm" placeholder="Address"/>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="mother_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          
          <input type="text" name="mother_first_name" class="form-control form-control-sm" placeholder="Mothers' First Name"/>


        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          
          <input type="text" name="mother_middle_name" class="form-control form-control-sm" placeholder="Mothers' Middle Name"/>


        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          
          <input type="text" name="mother_last_name" class="form-control form-control-sm" placeholder="Mothers' Last Name"/>


        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          
          <input type="email" name="mother_email" class="form-control form-control-sm" placeholder="Mothers' Email"/>


        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          
          <input type="text" name="mother_phone_number" class="form-control form-control-sm" placeholder="Mothers' Phone Number"/>


        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          
          <input type="text" name="mother_address" class="form-control form-control-sm" placeholder="Address"/>


        </div>

      </div>

    </div>

  </div>

</div>

<div id="guardian_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="guardian_first_name" class="form-control form-control-sm" placeholder="Guardians' First Name"/>

        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="guardian_middle_name" class="form-control form-control-sm" placeholder="Guardians' Middle Name"/>

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          <input type="text" name="guardian_last_name" class="form-control form-control-sm" placeholder="Guardians' Last Name"/>

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          <input type="email" name="guardian_email" class="form-control form-control-sm" placeholder="Guardians' Email"/>

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          <input type="text" name="guardian_phone_number" class="form-control form-control-sm" placeholder="Guardians' Phone Number"/>

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          <input type="text" name="guardian_address" class="form-control form-control-sm" placeholder="Address"/>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="other_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          

          <input type="text" name="other_first_name" class="form-control form-control-sm" placeholder="First Name"/>


        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          

          <input type="text" name="other_middle_name" class="form-control form-control-sm" placeholder="Middle Name"/>


        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          
          <input type="text" name="other_last_name" class="form-control form-control-sm" placeholder="Last Name"/>


        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          
          <input type="email" name="other_email" class="form-control form-control-sm" placeholder="Email"/>


        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          

          <input type="text" name="other_phone_number" class="form-control form-control-sm" placeholder="Phone Number"/>


        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          
          <input type="text" name="other_address" class="form-control form-control-sm" placeholder="Address"/>


        </div>

      </div>

    </div>

  </div>

</div>