@extends ('main_layout.layout_uno.index')



@section('main_question_content')



<div class="main-panel">        

    <div class="content-wrapper-index">

		    <div class="row">

            <div class="col-12 grid-margin">

              <div class="card">

                <div class="card-body">

                  <h4 class="card-title">Activity</h4>

                  <form class="form-sample" action="/activities/{{auth()->user()->id}}" method="post">

                    @csrf

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      Activity

                    </p>

                    <p class="card-description">

                      Reporting Activities can help a college better understand your life outside of the classroom.<br>

                      Your Activities may include arts, athletics, clubs, employment, personal commitments, and other pursuits.

                    </p>

                    

                    <div class="row">

                      <div class="col-md-6">

                        <label class="col-form-label">Do You have any activities?</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-transcribe-close text-primary"></i></span>
                       
                            </div>

                            @if($count > 0)

                            

                            <select class="form-control form-control-lg" id="activity" name="number_of_activities" disabled>

                              <option selected disabled>Select How Much Extra Carricular Activity You Do</option>

                              <option value="0">0</option>
                              
                              <option value="1" >1</option>
                              
                              <option value="2" >2</option>
                              
                              <option value="3" >3</option>
                              
                              <option value="4" >4</option>

                              <option value="5" >5</option>
                              
                              <option value="6" >6</option>
                              
                              <option value="7" >7</option>
                              
                              <option value="8" >8</option>
                              
                              <option value="9" >9</option>
                              
                              <option value="10" >10</option>

                            </select>

                            

                            <i class="mdi mdi-plus ml-4 mt-1" id="activity_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @else

                            <select class="form-control form-control-lg" id="activity" name="number_of_activities">

                              <option selected disabled>Select How Much Extra Carricular Activity You Do</option>

                              <option value="0">0</option>
                              
                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>

                              <option value="5">5</option>
                              
                              <option value="6">6</option>
                              
                              <option value="7">7</option>
                              
                              <option value="8">8</option>
                              
                              <option value="9">9</option>
                              
                              <option value="10">10</option>

                            </select>

                            <i class="mdi mdi-delete-sweep ml-4 mt-1" id="activity_minus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @endif

                            

                          </div>

                        </div>

                      </div>

                    </div>

                    

                    <div class="row">
                      
                      <div class="col-md-3">
                        
                        <div id="InputAreaForActivityName"></div>

                      </div>

                      <div class="col-md-6">
                        
                        <div id="InputAreaForActivityRecognition"></div>

                      </div>

                      <div class="col-md-3">
                        
                        <div id="InputAreaForActivityHoursSpent"></div>

                      </div>

                    </div>

                    <div class="row">
                      
                      <div class="col-md-3">
                        
                        <div id="InputAreaForActivityName2"></div>

                      </div>

                      <div class="col-md-6">
                        
                        <div id="InputAreaForActivityRecognition2"></div>

                      </div>

                      <div class="col-md-3">
                        
                        <div id="InputAreaForActivityHoursSpent2"></div>

                      </div>

                    </div>

                    <div class="row mt-5 mb-5">

                      <div class="col-md-6">

                        <button type="submit" class="btn btn-gradient-success mr-2">Save<i class=" mdi mdi-chevron-double-right ml-2"></i></button>

                      </div>

                    </div>
                    
                    @if($count > 0)

                    <!-- <div class="row"> -->

                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Activities' name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Activities' Description</strong></th>

                            <th style="font-size: 16px;"><strong>Hours Spent per week</strong></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>
                  
                          @foreach($activities as $activity)

                          @if(!is_null($activity->activity_name))

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $activity->activity_name }}</td>
                  
                            <td style="font-size: 14px;">{{ $activity->description }}</td>

                            <td style="font-size: 14px;">{{ $activity->hours_spent_per_week }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_activity_Modal'.$activity->id; ?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_activity_Modal'.$activity->id; ?>>Delete</button></td>
                            
                          </tr>

                          @endif

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif

                    

                    <div class="text-center">

                      @include('index-page.success_nd_error_page.success')

                      @include('index-page.success_nd_error_page.error_')

                    </div>

                  </form>

                </div>

              </div>

            </div>



        </div>



    </div>


    @if($count > 0)

      @foreach($activities as $activity)

        @include('main_layout.modals.mq-modals.activity_modal')

      @endforeach

    @endif


    @include('main_layout.layout_uno.footer')



</div>

<script type="text/javascript" src="{{URL::asset('/js/activity.js')}}"></script>


@endsection