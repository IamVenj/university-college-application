@extends ('main_layout.layout_uno.index')



@section('main_question_content')

<style type="text/css">
  
  #certificate_status_length{

    display: none;

  }

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		    <div class="row">

            <div class="col-12 grid-margin">

              <div class="card">

                <div class="card-body">

                  <h4 class="card-title">Education</h4>

                  <form class="form-sample" method="post" action="/education/{{auth()->user()->id}}">

                    @csrf

                    @method('PATCH')

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      Previous Education

                    </p>

                    @foreach($ed as $education)

                    <div class="row">
                        
                      <div class="col-md-4">

                        <label class="col-form-label">Have You Obtained A Diploma/Degree Certificate?</label>
                        
                        <div class="col-md-6">
                          
                          <div class="form-group">

                            <div class="row">

                              <div class="col-md-6">
                        
                                <div class="form-check">
                            
                                  <label class="form-check-label">

                                    
                            
                                    <input type="radio" class="form-check-input" name="certificate_status" id="certificate_status_yes" value="yes" <?php if( $education->previous_education == 'yes' ){ ?> checked <?php }?>>
                            
                                    Yes

                                    
                            
                                  </label>
                            
                                </div>
                                
                              </div>

                              <div class="col-md-6">
                            
                                <div class="form-check">
                            
                                  <label class="form-check-label">
                                
                                    <input type="radio" class="form-check-input" name="certificate_status" id="certificate_status_no" value="no"  <?php if( $education->previous_education == 'no' ){ ?> checked <?php }?>>
                                
                                    No
                                
                                  </label>
                                
                                </div>

                              </div>

                            </div>
                          
                          </div>
                        
                        </div>

                      </div>

                    </div>

                    <div id="certificate_status_length">

                      <div class="row">

                        <div class="col-md-4">

                          <div class="name_of_university_certificate">

                            <label class="col-form-label">Name of University</label>

                            <div class="form-group">

                              <div class="input-group">

                                <div class="input-group-prepend">

                                  <span class="input-group-text">

                                    <i class=" mdi mdi-school text-success"></i> 

                                  </span> 

                                </div> 

                                <select class="form-control form-control-lg dynamics" name="id_of_university_certificate" data-dependent="academics_name" id="university_id"> 

                                  <option selected disabled>Choose Name of University</option> 

                                  <?php 

                                  $previous_education = App\previous_education::where('user_id', auth()->user()->id)->first();

                                  $check_count = App\previous_education::where('user_id', auth()->user()->id)->count();

                                  ?>

                                  @foreach($universities as $university) 

                                  <option value="{{ $university->id }}" <?php if($check_count>0){ if($university->id == $previous_education->university_id){ ?>selected <?php } } ?>>{{ $university->university_name }}</option> 

                                  @endforeach 

                                </select>

                              </div>

                            </div>

                          </div>

                        </div>

                        <div class="col-md-4">

                          <div class="name_of_certificate">

                            <label class="col-form-label">Certificate</label>

                            <div class="form-group">

                              <div class="input-group">

                                <div class="input-group-prepend">

                                  <span class="input-group-text">

                                    <i class="mdi mdi-book-variant text-success"></i>

                                  </span>

                                </div>

                                <select class="form-control form-control-lg" id="academics_name" name="name_of_certificate">
                                  

                                </select>

                              </div>

                            </div>

                          </div>

                        </div>

                        <div class="col-md-4">

                          <div class="date_of_receiving_certificate">

                            <label class="col-form-label">Date When You Obtained Certificate</label>

                            <div class="form-group">

                              <div class="input-group">

                                <div class="input-group-prepend">

                                  <span class="input-group-text">

                                    <i class="mdi mdi-calendar-plus text-primary"></i>

                                  </span>

                                </div>

                                <input class="form-control datepicker" name="date_obtain_certificate" placeholder="Select date" type="text" value="{{ $date }}" readonly>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                    @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      High School

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">Number of High School</label>

                        <div class="form-group">     

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-school text-success"></i></span>
                       
                            </div>     

                            @if($count_hs > 0)                

                            <select class="form-control form-control-lg" id="num_high_schools" name="num_high_schools" disabled>

                              <option selected disabled>Select Number of Highschools</option>
                              
                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>

                            </select>

                            <i class="mdi mdi-plus ml-4 mt-1" id="highschool_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @else

                            <select class="form-control form-control-lg" id="num_high_schools" name="num_high_schools">

                              <option selected disabled>Select Number of Highschools</option>
                              
                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>

                            </select>

                            <i class="mdi mdi-delete-sweep ml-4 mt-1" id="highschool_minus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @endif

                          </div>
                        
                        </div>

                      </div>

                    </div>

                    <div class="row">

                      <div class="col-md-4">
                        
                        <div id="InputAreaForHighSchoolName"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForHighSchoolEmail"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForHighSchoolPhone"></div>

                      </div>

                    </div>

                    @if($count_hs > 0)

                    <!-- <div class="row"> -->

                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Highschools' name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Highschools' Email</strong></th>

                            <th style="font-size: 16px;"><strong>Highschools' Address</strong></th>

                            <th></th>

                            <th></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>
                          <?php

                          $highschool = App\highschool::where('user_id', auth()->user()->id)->get();

                          ?>

                  
                          @foreach ($highschool as $hs)

                          

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $hs->name }}</td>
                  
                            <td style="font-size: 14px;">{{ $hs->email }}</td>

                            <td style="font-size: 14px;">{{ $hs->address }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_highschool_Modal'.$hs->id; ?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger" data-toggle="modal" data-target=<?= '#delete_highschool_Modal'.$hs->id; ?>>Delete</button></td>
                            
                          </tr>

                          

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif

                    @endif

                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>

                    <p class="card-description">

                      Community Based Organization

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <p class="card-description">Indicate the number of community programs that have assisted you with free assistance in your application process</p>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account-convert text-danger"></i></span>
                       
                            </div>

                            @if($count_cbo > 0)

                            <select class="form-control form-control-lg" id="CBO" name="number_of_CBO" disabled>

                            	<option selected disabled>How Many Organizations?</option>
                              
                              <option value="0">0</option>

                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>
                              
                              <option value="5">5</option>
                              
                              <option value="6">6</option>
                              
                              <option value="7">7</option>
                              
                              <option value="8">8</option> 

                              <option value="9">9</option>                             
                              
                              <option value="10">10</option>

                            </select>

                            <i class="mdi mdi-plus ml-4 mt-1" id="CBO_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @else

                            <select class="form-control form-control-lg" id="CBO" name="number_of_CBO">

                              <option selected disabled>How Many Organizations?</option>
                              
                              <option value="0" <?php if($education->number_of_cbo == 0){ ?>selected<?php } ?>>0</option>

                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>
                              
                              <option value="5">5</option>
                              
                              <option value="6">6</option>
                              
                              <option value="7">7</option>
                              
                              <option value="8">8</option> 

                              <option value="9">9</option>                             
                              
                              <option value="10">10</option>

                            </select>

                            <i class="mdi mdi-delete-sweep ml-4 mt-1" id="CBO_minus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @endif

                          </div>

                        </div>

                      </div>
	                      
                    </div>  

                    <div class="row">
                      
                      <div class="col-md-3">

                        <div id="InputAreaForCBOName"></div>

                      </div>

                      <div class="col-md-3">

                        <div id="InputAreaForCBOCounselorPrefix"></div>

                      </div>

                      <div class="col-md-3">

                        <div id="InputAreaForCBOCounselorFullName"></div>

                      </div>

                      <div class="col-md-3">

                        <div id="InputAreaForCBOCounselorEmail"></div>

                      </div>

                    </div>  

                    @if($count_cbo > 0)


                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Organizations' Name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Counselor Prefix</strong></th>

                            <th style="font-size: 16px;"><strong>Counselor/Mentor Fullname</strong></th>

                            <th style="font-size: 16px;"><strong>Counselor/Mentor Email</strong></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>

                          <?php

                          $cbo = App\community_based_organization::where('user_id', auth()->user()->id)->get();
                            
                          ?>

                  
                          @foreach ($cbo as $_cbo_)

                          

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $_cbo_->cbo_name }}</td>
                  
                            <td style="font-size: 14px;">{{ $_cbo_->counselor_prefix }}</td>

                            <td style="font-size: 14px;">{{ $_cbo_->fullname }}</td>

                            <td style="font-size: 14px;">{{ $_cbo_->email }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_cbo_Modal'.$_cbo_->id;?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_cbo_Modal'.$_cbo_->id;?>>Delete</button></td>
                            
                          </tr>

                          

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif 

                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>               

                    <p class="card-description">

                      Grades

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">Cummulative GPA</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-transcribe-close text-primary"></i></span>
                       
                            </div>

                            <input type="text" class="form-control form-control-sm" placeholder="Cummulative GPA" name="cgpa" value="{{$education->cgpa}}" required/>

                          </div>

                        </div>

                      </div>

                    </div>

                    @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">National Examination Result</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-transcribe-close text-primary"></i></span>
                       
                            </div>

                            <input type="text" class="form-control form-control-sm" placeholder="National Examination Result" name="national_exam_result" value="{{$education->national_exam_result}}" required/>

                          </div>

                        </div>

                      </div>

                    </div>

                    @endif

                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>

                    <p class="card-description">

                      Honors

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">How Many Honors Have You Got?(if any)</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-star-half text-danger"></i></span>
                       
                            </div>

                            @if($count_honor > 0)

                            <select class="form-control form-control-lg" id="honor" name="num_of_honor" disabled>

                              <option selected disabled>How Many Honors Have You Got?</option>
                              
                              <option value="0">0</option>

                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>
                              
                              <option value="5">5</option>

                            </select>

                            <i class="mdi mdi-plus ml-4 mt-1" id="honor_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @else

                            <select class="form-control form-control-lg" id="honor" name="num_of_honor">

                              <option selected disabled>How Many Honors Have You Got?</option>
                              
                              <option value="0" <?php if($education->number_of_honor == 0){ ?>selected<?php } ?>>0</option>

                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>
                              
                              <option value="5">5</option>

                            </select>

                            <i class="mdi mdi-delete-sweep ml-4 mt-1" id="honor_minus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @endif

                          </div>

                        </div>

                      </div>
                          
                    </div> 

                    <div class="row">
                      
                      <div class="col-md-4">

                        <div id="InputAreaForHonorTitle"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForHonorGradeLevel"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForHonorLevelOfRecognition"></div>

                      </div>

                    </div> 

                    @if($count_honor > 0)

                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Honor Name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Grade Level</strong></th>

                            <th style="font-size: 16px;"><strong>Level of Recognition</strong></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>

                        <?php 

                        $honor = App\honors::where('user_id', auth()->user()->id)->get();
                          
                        ?>
                  
                          @foreach($honor as $this_honor)

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $this_honor->name }}</td>
                  
                            <td style="font-size: 14px;">{{ $this_honor->grade_level }}</td>

                            <td style="font-size: 14px;">{{ $this_honor->level_of_recognition }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_honor_Modal'.$this_honor->id; ?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_honor_Modal'.$this_honor->id; ?>>Delete</button></td>
                            
                          </tr>

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif

                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>               

                    <p class="card-description">

                      Future Plans

                    </p>

                    <?php

                    $future_plans = App\future_plans::where('user_id', auth()->user()->id)->first();

                    $count_future_plans = App\future_plans::where('user_id', auth()->user()->id)->count();

                    ?>

                    <div class="row">

                      <div class="col-md-6">

                        <label class="col-form-label">What are your Future Plans</label>

                        <div class="form-group">

                            <textarea type="text" class="form-control form-control-sm" rows="5" required name="future_plans_description">@if($count_future_plans > 0) {{ $future_plans->description }} @endif</textarea>

                        </div>

                      </div>

                    </div>

                    <div class="row">

                      <div class="col-md-6">

                        <label class="col-form-label">Highest Degree you Intend to Earn</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class=" mdi mdi-book-open-page-variant text-danger"></i></span>
                       
                            </div>

                            <select class="form-control form-control-lg" name="highest_degree_intended_to_be_earned">

                              <option selected disabled>Choose Degree</option>
                              
                              <option value="Associate's" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Associate's"){ ?> selected <?php } } ?>>Associate's(AA, AS)</option>
                              
                              <option value="Bachelor's" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Bachelor's"){ ?> selected <?php } }?>>Bachelor's(BA, BS)</option>
                              
                              <option value="Master's" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Master's"){ ?> selected <?php } }?>>Master's(MA, MS)</option>
                              
                              <option value="Business" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Business"){ ?> selected <?php } }?>>Buisness(MBA, MAcc)</option>
                              
                              <option value="Law" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Law"){ ?> selected <?php } }?>>Law(JD, LLM)</option>
                              
                              <option value="Medicine" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Medicine"){ ?> selected <?php } }?>>Medicine(MD, DO, DVM, DOS)</option>
                              
                              <option value="Doctorate" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Doctorate"){ ?> selected <?php } }?>>Doctorate(PhD, EdD, etc)</option>

                              <option value="Undecided" <?php if($count_future_plans > 0){ if($future_plans->highest_degree_intended == "Undecided"){ ?> selected <?php } }?>>Undecided</option>

                            </select>

                          </div>

                        </div>

                      </div>

                    </div>

                    @endforeach

                    <div class="row mt-5">

                      <div class="col-md-6">

                        <button type="submit" class="btn btn-gradient-success mr-2">Save<i class=" mdi mdi-chevron-double-right ml-2"></i></button>

                      </div>

                    </div>

                    <div class="text-center">

                      @include('index-page.success_nd_error_page.success')

                      @include('index-page.success_nd_error_page.error_')

                    </div>

                  </form>

                </div>

              </div>

            </div>



        </div>



    </div>

    <!-- highschool -->

    @if($count_hs > 0)

    @foreach($highschool as $hs)

    @include('main_layout.modals.mq-modals.education_highschool_modal')

    @endforeach

    @endif

    <!-- communit based organization -->

    @if($count_cbo > 0)

    @foreach($cbo as $_cbo_)

    @include('main_layout.modals.mq-modals.education_cbo_modal')

    @endforeach

    @endif

    <!-- honor -->

    @if($count_honor > 0)

    @foreach($honor as $this_honor)

    @include('main_layout.modals.mq-modals.education_honor_modal')

    @endforeach

    @endif



    @include('main_layout.layout_uno.footer')



</div>


<script type="text/javascript" src="../../../js/education.js"></script>

<script type="text/javascript" src="../../../js/datepicker.js"></script>

@if($education->previous_education == 'yes')

  <script type="text/javascript">document.getElementById('certificate_status_length').style.display = 'block';</script>

@endif


@endsection