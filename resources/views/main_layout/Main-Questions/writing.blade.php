@extends ('main_layout.layout_uno.index')



@section('main_question_content')

<style type="text/css">
  
  #violation{

    display: none;

  }

  #crime{

    display: none;

  }

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		    <div class="row">

            <div class="col-12 grid-margin">

              <div class="card">

                <div class="card-body">

                  <h4 class="card-title">Writing</h4>

                  <form class="form-sample" action="/writing/{{auth()->user()->id}}" method="post">

                    @csrf

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      Personal Essay

                    </p>

                    <p class="card-description">

                      The essay demonstrates your ability to write clearly and helps you distinguish yourself in your own voice.<br>

                      What do you want the readers of your application to know about you apart from courses, grades and test scores?

                    </p>

                    

                    <div class="row">

                      <div class="col-md-6">

                        <label class="col-form-label">Essay</label>

                        <div class="form-group">   

                          @if($count > 0)      

                          @foreach($writings as $writing)               

                          <textarea class="form-control" rows="8" name="essay" required>{{ $writing->essay }}</textarea>

                          @endforeach

                          @else

                          <textarea class="form-control" rows="8" name="essay" required></textarea>

                          @endif

                        </div>

                      </div>

                    </div>

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      Disciplinary History

                    </p>

                    <p class="card-description">

                      Have you ever been found responsible for a disciplinary violation at any educational institution you have attended, <br>

                      whether related to academic misconduct or behavioural misconduct, that resulted in a disciplinary action?<br>

                      These actions could include, but are not limited to: probation, suspension, removal, dismissal, or expulsion from the 

                      institution.

                    </p>

                    <div class="row">
                        
                      <div class="col-md-4">
                        
                        <div class="col-md-6">
                          
                          <div class="form-group">

                            <div class="row">

                              <div class="col-md-6">
                        
                                <div class="form-check">
                            
                                  <label class="form-check-label">

                                    @if($count > 0)

                                    @foreach($writings as $writing) 
                            
                                    <input type="radio" class="form-check-input" name="disciplinary_status" id="disciplinary_status_yes" value="yes" <?php if($writing->disciplinary_violation == 'yes'){ ?> checked <?php } ?>>

                                    @endforeach

                                    @else

                                    <input type="radio" class="form-check-input" name="disciplinary_status" id="disciplinary_status_yes" value="yes">

                                    @endif
                            
                                    Yes
                            
                                  </label>
                            
                                </div>
                                
                              </div>

                              <div class="col-md-6">
                            
                                <div class="form-check">
                            
                                  <label class="form-check-label">

                                    @if($count > 0)

                                    @foreach($writings as $writing) 
                                
                                    <input type="radio" class="form-check-input" name="disciplinary_status" id="disciplinary_status_no" value="no" <?php if($writing->disciplinary_violation == 'no'){ ?> checked <?php } ?>>

                                    @endforeach

                                    @else

                                    <input type="radio" class="form-check-input" name="disciplinary_status" id="disciplinary_status_no" value="no">

                                    @endif
                                
                                    No
                                
                                  </label>
                                
                                </div>

                              </div>

                            </div>
                          
                          </div>
                        
                        </div>

                      </div>

                    </div>

                    <div id="violation">

                      <p class="card-description">

                        Please give the approximate date(s) of each incident, explain the circumstances and reflect on what you learned <br>from the experience

                      </p>

                      <div class="row">

                        <div class="col-md-6">

                          <div class="form-group">     

                            @if($count > 0)

                            @foreach($writings as $writing)                     

                            <textarea class="form-control" rows="8" name="violation_description" required>{{ $writing->disciplinary_violation_description }}</textarea>

                            @endforeach

                            @else

                            <textarea class="form-control" rows="8" name="violation_description" ></textarea>

                            @endif

                          </div>

                        </div>

                      </div>

                    </div>

                    <p class="card-description">

                      Have you ever been adjudicated guilty or convicted of a misdemeanor or felony? Note that you are not required to answer 'yes' <br> to this question, or provide an explanation, if the criminal adjudication or conviction has been expunged, sealed, annulled, pardoned, <br>destroyed, erased, impounded, or otherwise required by law or ordered by court to be kept confidential.

                    </p>

                    <div class="row">
                        
                      <div class="col-md-4">
                        
                        <div class="col-md-6">
                          
                          <div class="form-group">

                            <div class="row">

                              <div class="col-md-6">
                        
                                <div class="form-check">
                            
                                  <label class="form-check-label">
                            
                                    

                                    @if($count > 0)

                                    @foreach($writings as $writing) 

                                    <input type="radio" class="form-check-input" name="crime_status" id="crime_status_yes" value="yes" <?php if($writing->conviction == 'yes'){ ?> checked <?php } ?>>

                                    @endforeach

                                    @else

                                    <input type="radio" class="form-check-input" name="crime_status" id="crime_status_yes" value="yes">

                                    @endif

                                                                
                                    Yes
                            
                                  </label>
                            
                                </div>
                                
                              </div>

                              <div class="col-md-6">
                            
                                <div class="form-check">
                            
                                  <label class="form-check-label">

                                    @if($count > 0)

                                    @foreach($writings as $writing) 
                                
                                    <input type="radio" class="form-check-input" name="crime_status" id="crime_status_no" value="no" <?php if($writing->conviction == 'no'){ ?> checked <?php } ?>>

                                    @endforeach

                                    @else

                                    <input type="radio" class="form-check-input" name="crime_status" id="crime_status_no" value="no">

                                    @endif
                                
                                    No
                                
                                  </label>
                                
                                </div>

                              </div>

                            </div>
                          
                          </div>
                        
                        </div>

                      </div>

                    </div>

                    <div id="crime">

                      <p class="card-description">

                        Please provide the details of your criminal conviction or adjudication and reflect on what you have learned from the experience 

                      </p>

                      <div class="row">

                        <div class="col-md-6">

                          <div class="form-group"> 

                            @if($count > 0)

                            @foreach($writings as $writing)                        

                            <textarea class="form-control" rows="8" name="crime_description" required>{{ $writing->conviction_description }}</textarea>

                            @endforeach

                            @else

                            <textarea class="form-control" rows="8" name="crime_description" ></textarea>

                            @endif

                          </div>

                        </div>

                      </div>

                    </div>

                    

                    <div class="row mt-5">

                      <div class="col-md-6">

                        <button type="submit" class="btn btn-gradient-success mr-2">Save<i class=" mdi mdi-chevron-double-right ml-2"></i></button>

                      </div>

                    </div>

                    <div class="text-center">

                      @include('index-page.success_nd_error_page.success')

                      @include('index-page.success_nd_error_page.error_')

                    </div>


                  </form>

                </div>

              </div>

            </div>



        </div>



    </div>



    @include('main_layout.layout_uno.footer')



</div>

<script type="text/javascript" src="../../../js/writing.js"></script>

@if($count > 0)

@if($writing->conviction == 'yes')

  <script type="text/javascript">document.getElementById('crime').style.display = 'block';</script>

@endif

@endif

@if($count > 0)

@if($writing->disciplinary_violation == 'yes')

  <script type="text/javascript">document.getElementById('violation').style.display = 'block';</script>

@endif

@endif

@endsection