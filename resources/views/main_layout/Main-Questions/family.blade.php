@extends ('main_layout.layout_uno.index')



@section('main_question_content')



<style type="text/css">
  
  #father_about{

    display: none;

  }

  #mother_about{

    display: none;

  }

  #guardian_about{

    display: none;

  }

  #other_about{

    display: none;

  }

  #children_stats{

    display: none;

  }

  #children_status_length{

    display:none;

  }

</style>


<div class="main-panel">        

    <div class="content-wrapper-index">

		    <div class="row">

            <div class="col-12 grid-margin">

              <div class="card">

                <div class="card-body">

                  <h4 class="card-title">Family</h4>

                  <form class="form-sample" action="/fam" method="post">

                    @csrf

                    

                    <div class="dropdown-divider" style="width: 80px;"></div>

                    <p class="card-description">

                      Parents or Guardian

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">Currently Living With</label>

                        <div class="form-group">     

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account-circle text-success"></i></span>
                       
                            </div>                     

                            <select class="form-control form-control-lg" name="family_parent" id="family_parent">

                              <option selected disabled>Select Who You are Currently Living With</option>

                              @if($count > 0)

                              @foreach($families as $family)

                              <option value="Father" <?php if($family->currently_living_with == 'Father') { ?> selected <?php } ?>>Father</option>
                              
                              <option value="Mother" <?php if($family->currently_living_with == 'Mother') { ?> selected <?php } ?>>Mother</option>

                              <option value="Both" <?php if($family->currently_living_with == 'Both') { ?> selected <?php } ?>>Both Parents</option>
                              
                              <option value="Guardian" <?php if($family->currently_living_with == 'Guardian') { ?> selected <?php } ?>>Guardian</option>
                              
                              <option value="Other" <?php if($family->currently_living_with == 'Other') { ?> selected <?php } ?>>Other</option> 

                              <option value="Alone" <?php if($family->currently_living_with == 'Alone') { ?> selected <?php } ?>>Alone</option>

                              @endforeach

                              @else

                              <option value="Father">Father</option>
                              
                              <option value="Mother">Mother</option>

                              <option value="Both">Both Parents</option>
                              
                              <option value="Guardian">Guardian</option>
                              
                              <option value="Other">Other</option> 

                              <option value="Alone">Alone</option>

                              @endif                   

                            </select>

                            
                          </div>


                        
                        </div>

                        

                      </div>
                        
                    </div>

                    @if($count > 0)

                    @foreach($families as $family)
                    
                    @include('main_layout.Main-Questions.currently_living_with_family')

                    @endforeach

                    @else

                    @include('main_layout.Main-Questions._currently_living_with_family')

                    @endif             

                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>

                    <p class="card-description">

                      Marital Status

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">Marital Status</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class=" mdi mdi-account-multiple-outline text-success"></i></span>
                       
                            </div>

                            <select class="form-control form-control-lg" id="marital_status" name="marital_status">

                            	

                              @if($count > 0)

                              <option selected disabled>Select Your Marital Status</option>

                              @foreach($families as $family)
                              
                              <option value="Married" <?php if($family->marital_status == 'Married'){ ?> selected <?php } ?>>Married</option>

                              <option value="Separated" <?php if($family->marital_status == 'Separated'){ ?> selected <?php } ?>>Separated</option>
                              
                              <option value="Divorced" <?php if($family->marital_status == 'Divorced'){ ?> selected <?php } ?>>Divorced</option>

                              <option value="Never Married" <?php if($family->marital_status == 'Never Married'){ ?> selected <?php } ?>>Never Married</option>
                              
                              <option value="Widowed" <?php if($family->marital_status == 'Widowed'){ ?> selected <?php } ?>>Widowed</option>

                              @endforeach

                              @else

                              <option selected disabled>Select Your Marital Status</option>

                              <option value="Married">Married</option>

                              <option value="Separated">Separated</option>
                              
                              <option value="Divorced">Divorced</option>

                              <option value="Never Married">Never Married</option>
                              
                              <option value="Widowed">Widowed</option>

                              @endif
                              
                            </select>

                            
                          </div>

                        </div>

                      </div>

                                           
	                        
                    </div> 

                    <div id="children_stats"> 

                      <div class="row">
                        
                        <div class="col-md-4">

                          <label class="col-form-label">Children</label>
                          
                          <div class="col-md-6">
                          
                            <div class="form-group">
                          
                              <div class="form-check">
                          
                                <label class="form-check-label">

                                  @if($count > 0)

                                  @foreach($families as $family)
                          
                                  <input type="radio" class="form-check-input" name="children_status" id="children_status_yes" value="yes" <?php if( $family->children == 'yes' ){ ?> checked <?php } ?>>

                                  @endforeach

                                  @else

                                  <input type="radio" class="form-check-input" name="children_status" id="children_status_yes" value="yes">

                                  @endif
                                  
                                  Yes
                          
                                </label>
                          
                              </div>
                          
                              <div class="form-check">
                          
                                <label class="form-check-label">

                                  @if($count > 0)

                                  @foreach($families as $family)
                              
                                  <input type="radio" class="form-check-input" name="children_status" id="children_status_no" value="no" <?php if( $family->children == 'no' ){ ?> checked <?php }?>>

                                  @endforeach

                                  @else

                                  <input type="radio" class="form-check-input" name="children_status" id="children_status_no" value="no" checked>

                                  @endif
                              
                                  No
                              
                                </label>
                              
                              </div>
                            
                            </div>
                          
                          </div>

                        </div>

                      </div>

                    </div>

                    <div id="children_status_length">

                      <div class="row">

                        <div class="col-md-4">

                          <label class="col-form-label">How Many?</label>

                          <div class="form-group">

                            <div class="input-group">
                       
                              <div class="input-group-prepend">
                         
                                <span class="input-group-text"><i class="mdi mdi-transcribe-close text-primary"></i></span>
                         
                              </div>

                              @if($count_child > 0)

                              <select class="form-control form-control-lg" id="_children_" name="number_of_children" disabled>

                                <option selected disabled>How Many Children Do You Have?</option>
                              
                                <option value="1">1</option>
                                
                                <option value="2">2</option>
                                
                                <option value="3">3</option>
                                
                                <option value="4">4</option>

                                <option value="5">5</option>
                                
                                <option value="6">6</option>
                                
                                <option value="7">7</option>
                                
                                <option value="8">8</option>
                                
                                <option value="9">9</option>
                                
                                <option value="10">10</option>

                              </select>

                              <i class="mdi mdi-plus ml-4 mt-1" id="children_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                              @else

                              <select class="form-control form-control-lg" id="_children_" name="number_of_children">

                                <option selected disabled>How Many Children Do You Have?</option>

                                <option value="1">1</option>
                                
                                <option value="2">2</option>
                                
                                <option value="3">3</option>
                                
                                <option value="4">4</option>

                                <option value="5">5</option>
                                
                                <option value="6">6</option>
                                
                                <option value="7">7</option>
                                
                                <option value="8">8</option>
                                
                                <option value="9">9</option>
                                
                                <option value="10">10</option>

                                @endif

                              </select>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                    <div class="row">
                      
                      <div class="col-md-4">
                        
                        <div id="InputAreaForChildsName"></div>

                      </div>

                      <div class="col-md-4">
                        
                        <div id="InputAreaForChildsEmail"></div>

                      </div>

                      <div class="col-md-4">
                        
                        <div id="InputAreaForChildsPhoneNum"></div>

                      </div>

                    </div>

                    @if($count_child > 0)

                    <!-- <div class="row"> -->

                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Childs' name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Childs' email</strong></th>

                            <th style="font-size: 16px;"><strong>Childs' Phone Number</strong></th>

                            <th></th>

                            <th></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>
                  
                          <?php

                          $childrens = App\children::where('user_id', auth()->user()->id)->get();    

                          ?>

                          @foreach($childrens as $children)

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $children->fullname }}</td>
                  
                            <td style="font-size: 14px;">{{ $children->email }}</td>

                            <td style="font-size: 14px;">{{ $children->phone_number }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_child_Modal'.$children->id; ?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_child_Modal'.$children->id; ?>>Delete</button></td>
                            
                          </tr>

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif


                    <div class="dropdown-divider mt-4" style="width: 80px;"></div>               

                    <p class="card-description">

                      Siblings

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">How Many?</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-transcribe-close text-primary"></i></span>
                       
                            </div>

                            @if($count_siblings > 0)

                            <select class="form-control form-control-lg" id="siblings" name="number_of_siblings" disabled>

                              <option selected disabled>How Many Siblings Do You Have?</option>                              
                              
                              <option value="0">0</option>
                              
                              <option value="1">1</option>
                              
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>

                              <option value="5">5</option>
                              
                              <option value="6">6</option>
                              
                              <option value="7">7</option>
                              
                              <option value="8">8</option>
                              
                              <option value="9">9</option>
                              
                              <option value="10">10</option>

                            </select>

                            <i class="mdi mdi-plus ml-4 mt-1" id="siblings_plus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @else

                            <select class="form-control form-control-lg" id="siblings" name="number_of_siblings">

                              <option selected disabled>How Many Siblings Do You Have?</option>  

                              <option value="0" <?php if($families->count()>0) { if($family->number_of_siblings == 0){ ?>selected<?php } }?>>0</option>

                              <option value="1">1</option>
                                
                              <option value="2">2</option>
                              
                              <option value="3">3</option>
                              
                              <option value="4">4</option>

                              <option value="5">5</option>
                              
                              <option value="6">6</option>
                              
                              <option value="7">7</option>
                              
                              <option value="8">8</option>
                              
                              <option value="9">9</option>
                              
                              <option value="10">10</option>

                            </select>

                            <i class="mdi mdi-delete-sweep ml-4 mt-1" id="siblings_minus" style="font-size: 30px; color:rgba(0,0,0,0.4);"></i>

                            @endif

                          </div>

                        </div>

                      </div>

                      

                    </div>

                    <div class="row">
                      
                      <div class="col-md-4">

                        <div id="InputAreaForSiblingName"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForSiblingEmail"></div>

                      </div>

                      <div class="col-md-4">

                        <div id="InputAreaForSiblingPhoneNumber"></div>

                      </div>

                    </div>

                    @if($count_siblings > 0)

                    <!-- <div class="row"> -->

                    <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

                    <div class="table-responsive">

                      <table class="table table-hover">
  
                        <thead>
                  
                          <tr>
                  
                            <th style="font-size: 16px;"><strong>Siblings' name</strong></th>
                  
                            <th style="font-size: 16px;"><strong>Siblings' email</strong></th>

                            <th style="font-size: 16px;"><strong>Siblings' Phone Number</strong></th>

                            <th></th>

                            <th></th>
                  
                          </tr>
                  
                        </thead>
                  
                        <tbody>
                  
                          <?php

                          $siblings = App\siblings::where('user_id', auth()->user()->id)->get();    

                          ?>

                          @foreach($siblings as $sibling)

                          <tr>
                  
                            <td style="font-size: 14px;">{{ $sibling->fullname }}</td>
                  
                            <td style="font-size: 14px;">{{ $sibling->email }}</td>

                            <td style="font-size: 14px;">{{ $sibling->phone_number }}</td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#edit_sibling_Modal'.$sibling->id; ?>>Edit</button></td>
                            
                            <td style="font-size: 14px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_sibling_Modal'.$sibling->id; ?>>Delete</button></td>
                            
                          </tr>

                          @endforeach
                  
                        </tbody>
                  
                      </table>

                      

                    </div>

                    @endif

                    <div class="row mt-5">

                      <div class="col-md-6">

                        <button type="submit" class="btn btn-gradient-success mr-2">Save<i class=" mdi mdi-chevron-double-right ml-2"></i></button>

                      </div>

                    </div>

                    <div class="text-center">

                      @include('index-page.success_nd_error_page.success')

                      @include('index-page.success_nd_error_page.error_')

                    </div>

                  </form>

                </div>

              </div>

            </div>



        </div>



    </div>



    @include('main_layout.layout_uno.footer')


    @if($count_child > 0)

    @foreach($childrens as $children)

    @include('main_layout.modals.mq-modals.family_children_modal')

    @endforeach

    @endif


    @if($count_siblings > 0)

    @foreach($siblings as $sibling)

    @include('main_layout.modals.mq-modals.family_sibling_modal')

    @endforeach

    @endif


</div>

@if($count > 0)

@if($family->children == 'yes')

  <script type="text/javascript">document.getElementById('children_status_length').style.display = 'block';</script>

@endif

@endif

<script type="text/javascript" src="../../../js/family.js"></script>

@endsection