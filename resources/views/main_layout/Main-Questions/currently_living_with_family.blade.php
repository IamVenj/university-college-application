<?php

$get_father = App\father::where('id', $family->father_id)->first();

$get_father_count = App\father::where('id', $family->father_id)->count();

?>

<div id="father_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="text" name="father_first_name" class="form-control form-control-sm" value="{{$get_father->father_first_name}}" placeholder="Fathers' First Name"/>

          @else

          <input type="text" name="father_first_name" class="form-control form-control-sm" placeholder="Fathers' First Name"/>

          @endif
          
        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="text" name="father_middle_name" class="form-control form-control-sm" value="{{$get_father->father_middle_name}}" placeholder="Fathers' Middle Name"/>

          @else

          <input type="text" name="father_middle_name" class="form-control form-control-sm" placeholder="Fathers' Middle Name"/>

          @endif

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="text" name="father_last_name" class="form-control form-control-sm" value="{{$get_father->father_last_name}}" placeholder="Fathers' Last Name"/>

          @else

          <input type="text" name="father_last_name" class="form-control form-control-sm" placeholder="Fathers' Last Name"/>

          @endif

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="email" name="father_email" class="form-control form-control-sm" value="{{$get_father->father_email}}" placeholder="Fathers' Email"/>

          @else

          <input type="email" name="father_email" class="form-control form-control-sm" placeholder="Fathers' Email"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Fathers' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="text" name="father_phone_number" class="form-control form-control-sm" value="{{$get_father->father_phone_number}}" placeholder="Fathers' Phone Number"/>

          @else

          <input type="text" name="father_phone_number" class="form-control form-control-sm" placeholder="Fathers' Phone Number"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          @if($get_father_count > 0)

          <input type="text" name="father_address" class="form-control form-control-sm" value="{{$get_father->father_address}}" placeholder="Address"/>

          @else

          <input type="text" name="father_address" class="form-control form-control-sm" placeholder="Address"/>

          @endif

        </div>

      </div>

    </div>

  </div>

</div>

<?php

$get_mother = App\mother::where('id', $family->mother_id)->first();

$get_mother_count = App\mother::where('id', $family->mother_id)->count();

?>

<div id="mother_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="text" name="mother_first_name" class="form-control form-control-sm" value="{{$get_mother->mother_first_name}}" placeholder="Mothers' First Name"/>

          @else

          <input type="text" name="mother_first_name" class="form-control form-control-sm" placeholder="Mothers' First Name"/>

          @endif

        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="text" name="mother_middle_name" class="form-control form-control-sm" value="{{$get_mother->mother_middle_name}}" placeholder="Mothers' Middle Name"/>

          @else

          <input type="text" name="mother_middle_name" class="form-control form-control-sm" placeholder="Mothers' Middle Name"/>

          @endif

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="text" name="mother_last_name" class="form-control form-control-sm" value="{{$get_mother->mother_last_name}}" placeholder="Mothers' Last Name"/>

          @else

          <input type="text" name="mother_last_name" class="form-control form-control-sm" placeholder="Mothers' Last Name"/>

          @endif

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="email" name="mother_email" class="form-control form-control-sm" value="{{$get_mother->mother_email}}" placeholder="Mothers' Email"/>

          @else

          <input type="email" name="mother_email" class="form-control form-control-sm" placeholder="Mothers' Email"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Mothers' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="text" name="mother_phone_number" class="form-control form-control-sm" value="{{$get_mother->mother_phone_number}}" placeholder="Mothers' Phone Number"/>

          @else

          <input type="text" name="mother_phone_number" class="form-control form-control-sm" placeholder="Mothers' Phone Number"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          @if($get_mother_count > 0)

          <input type="text" name="mother_address" class="form-control form-control-sm" value="{{$get_mother->mother_address}}" placeholder="Address"/>

          @else

          <input type="text" name="mother_address" class="form-control form-control-sm" placeholder="Address"/>

          @endif

        </div>

      </div>

    </div>

  </div>

</div>

<?php

$get_guardian = App\guardian::where('id', $family->guardian_id)->first();

$get_guardian_counts = App\guardian::where('id', $family->guardian_id)->count();

?>

<div id="guardian_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="text" name="guardian_first_name" class="form-control form-control-sm" value="{{$get_guardian->guardian_first_name}}" placeholder="Guardians' First Name"/>

          @else

          <input type="text" name="guardian_first_name" class="form-control form-control-sm" placeholder="Guardians' First Name"/>

          @endif

        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="text" name="guardian_middle_name" class="form-control form-control-sm" value="{{$get_guardian->guardian_middle_name}}" placeholder="Guardians' Middle Name"/>

          @else

          <input type="text" name="guardian_middle_name" class="form-control form-control-sm" placeholder="Guardians' Middle Name"/>

          @endif

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="text" name="guardian_last_name" class="form-control form-control-sm" value="{{$get_guardian->guardian_last_name}}" placeholder="Guardians' Last Name"/>

          @else

          <input type="text" name="guardian_last_name" class="form-control form-control-sm" placeholder="Guardians' Last Name"/>

          @endif

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="email" name="guardian_email" class="form-control form-control-sm" value="{{$get_guardian->guardian_email}}" placeholder="Guardians' Email"/>

          @else

          <input type="email" name="guardian_email" class="form-control form-control-sm" placeholder="Guardians' Email"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Guardians' Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="text" name="guardian_phone_number" class="form-control form-control-sm" value="{{$get_guardian->guardian_phone_number}}" placeholder="Guardians' Phone Number"/>

          @else

          <input type="text" name="guardian_phone_number" class="form-control form-control-sm" placeholder="Guardians' Phone Number"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          @if($get_guardian_counts > 0)

          <input type="text" name="guardian_address" class="form-control form-control-sm" value="{{$get_guardian->guardian_address}}" placeholder="Address"/>

          @else

          <input type="text" name="guardian_address" class="form-control form-control-sm" placeholder="Address"/>

          @endif

        </div>

      </div>

    </div>

  </div>

</div>

<?php

$get_other = App\other::where('id', $family->other_id)->first();

$get_other_count = App\other::where('id', $family->other_id)->count();

?>

<div id="other_about">

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">First Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="text" name="other_first_name" class="form-control form-control-sm" value="{{$get_other->first_name}}" placeholder="First Name"/>

          @else

          <input type="text" name="other_first_name" class="form-control form-control-sm" placeholder="First Name"/>

          @endif

        </div>

      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Middle Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="text" name="other_middle_name" class="form-control form-control-sm" value="{{$get_other->middle_name}}" placeholder="Middle Name"/>

          @else

          <input type="text" name="other_middle_name" class="form-control form-control-sm" placeholder="Middle Name"/>

          @endif

        </div>


      </div>

    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Last Name</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-account text-success"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="text" name="other_last_name" class="form-control form-control-sm" value="{{$get_other->last_name}}" placeholder="Last Name"/>

          @else

          <input type="text" name="other_last_name" class="form-control form-control-sm" placeholder="Last Name"/>

          @endif

        </div>


      </div>

    </div>

  </div>

  <div class="row">                    

    <div class="col-md-4">
      
      <label class="col-form-label">Email</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-email text-warning"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="email" name="other_email" class="form-control form-control-sm" value="{{$get_other->email}}" placeholder="Email"/>

          @else

          <input type="email" name="other_email" class="form-control form-control-sm" placeholder="Email"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Phone Number <code>( add country code )</code></label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-cellphone-iphone text-primary"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="text" name="other_phone_number" class="form-control form-control-sm" value="{{$get_other->phone_number}}" placeholder="Phone Number"/>

          @else

          <input type="text" name="other_phone_number" class="form-control form-control-sm" placeholder="Phone Number"/>

          @endif

        </div>

      </div>
      
    </div>

    <div class="col-md-4">
      
      <label class="col-form-label">Address</label>

      <div class="form-group">

        <div class="input-group">

          <div class="input-group-prepend">

            <span class="input-group-text">

              <i class="mdi mdi-map-marker text-danger"></i>

            </span>

          </div>

          @if($get_other_count > 0)

          <input type="text" name="other_address" class="form-control form-control-sm" value="{{$get_other->address}}" placeholder="Address"/>

          @else

          <input type="text" name="other_address" class="form-control form-control-sm" placeholder="Address"/>

          @endif

        </div>

      </div>

    </div>

  </div>

</div>