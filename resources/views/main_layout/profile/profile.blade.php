@extends ('main_layout.layout_uno.index')



@section('main_question_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		  <div class="row">

            <div class="col-12 grid-margin">

              <div class="card">

                <div class="card-body">

                  <h4 class="card-title">Edit Profile</h4>

                  <form class="form-sample" action="/profile/{{auth()->user()->id}}" method="post">

                    @csrf

                    @method('PATCH')

                    <div class="dropdown-divider" style="width: 55px;"></div>

                    <p class="card-description">

                      About<i class=" mdi mdi-account-check ml-2"></i>

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">First Name</label>

                        <div class="form-group">     

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account text-success"></i></span>
                       
                            </div>                     

                            <input type="text" class="form-control form-control-sm" placeholder="first name" value="{{auth()->user()->firstname}}" name="first_name" />

                          </div>
                        
                        </div>

                      </div>

                      <div class="col-md-4">

                        <label class="col-form-label">Middle Name</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account text-success"></i></span>
                       
                            </div>
                        
                            <input type="text" class="form-control form-control-sm" placeholder="middle name" value="{{auth()->user()->middlename}}" name="middle_name" />

                          </div>

                        </div>

                      </div>

                      <div class="col-md-4">

                        <label class=" col-form-label">Last Name</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account text-success"></i></span>
                       
                            </div>

                            <input type="text" class="form-control form-control-sm" placeholder="last name" value="{{auth()->user()->lastname}}" name="last_name" />

                          </div>
                        
                        </div>

                      </div>

                    </div>

                    <div class="row">

                      <div class="col-md-3">

                        <label class="col-form-label">Email</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-email text-danger"></i></span>
                       
                            </div>

                            <input type="text" name="email" class="form-control form-control-sm" placeholder="email" value="{{auth()->user()->email}}">

                          </div>

                        </div>

                      </div>

                      <div class="col-md-3">

                        <label class="col-form-label">Gender</label>

                        <div class="form-group">

                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account-convert text-danger"></i></span>
                       
                            </div>

                            <select class="form-control form-control-lg" name="gender">

                            	<option selected disabled>Select Gender</option>

                              <option value="M" <?php if(auth()->user()->sex == 'M'){ ?> selected <?php } ?>>Male</option>

                              <option value="F" <?php if(auth()->user()->sex == 'F'){ ?> selected <?php } ?>>Female</option>

                            </select>

                          </div>

                        </div>

                      </div>

                      <div class="col-md-3">
                       
                        <label class="col-form-label">Date of Birth</label>
                       
                        <div class="form-group">
                       
                          <div class="input-group">
                       
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-calendar-plus text-primary"></i></span>
                       
                            </div>
                       
                            <input class="form-control datepicker" name="date_of_birth" placeholder="Select date" type="text" value="{{ $date }}" readonly>
                       
                          </div>
                       
                        </div>
                      
                      </div>

                      <div class="col-md-3">

                        <label class="col-form-label">Phone Number</label>

                        <div class="form-group">

                          <div class="row">

                            <div class="col-md-5">

                              <div class="input-group">
                     
                                <div class="input-group-prepend">
                           
                                  <span class="input-group-text"><i class=" mdi mdi-phone-classic text-primary"></i></span>
                           
                                </div>

                                <select name="country_code" class="form-control form-control-lg" id="exampleFormControlSelect3">
                  
                                    <option selected disabled>Choose Country Code</option>
                  
                                    <option value="+251" <?php if(auth()->user()->country_code == '+251'){?> selected <?php }?> >+251</option>
                  
                                </select>

                              </div>

                            </div>

                            <div class="col-md-7">

                              <input name="phone_number" type="text" class="form-control" placeholder="phone number" value= "{{ auth()->user()->phone_number }}" required>

                            </div>

                          </div>

                        </div>
                                        
                      </div>
	                        
                    </div>  

                    <div class="dropdown-divider mt-4" style="width: 55px;"></div>                  

                    <p class="card-description">

                      Geography<i class="mdi mdi-map-marker ml-2"></i>

                    </p>

                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">Address Line (Street Name and Number)</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-account-location text-primary"></i></span>
                       
                            </div>

                            <input type="text" class="form-control form-control-sm" name="address_line" value="{{ auth()->user()->address_line }}"/>

                          </div>

                        </div>

                      </div>                     

                      <div class="col-md-8">

                        <label class="col-form-label">City</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-city text-success"></i></span>
                       
                            </div>

                            <select class="form-control form-control-lg" name="city">

                              <option selected disabled>Choose City</option>

                              <option value="Addis Ababa" <?php if(auth()->user()->city == 'Addis Ababa'){ ?> selected <?php }?>> Addis Ababa </option>

                              <option value="DireDawa" <?php if(auth()->user()->city == 'DireDawa'){ ?> selected <?php }?>> DireDawa </option>

                              <option value="ArbaMinch" <?php if(auth()->user()->city == 'ArbaMinch'){ ?> selected <?php }?>> ArbaMinch </option>

                              <option value="Mekele" <?php if(auth()->user()->city == 'Mekele'){ ?> selected <?php }?>> Mekele </option>

                              <option value="BahirDar" <?php if(auth()->user()->city == 'BahirDar'){ ?> selected <?php }?>> BahirDar </option>

                              <option value="Gonder" <?php if(auth()->user()->city == 'Gonder'){?> selected <?php }?>> Gonder </option>

                              <option value="Jimma" <?php if(auth()->user()->city == 'Jimma'){ ?> selected <?php }?>> Jimma </option>

                              <option value="DebreMarkos" <?php if(auth()->user()->city == 'DebreMarkos'){ ?> selected <?php }?>> DebreMarkos </option>

                            </select>

                          </div>

                        </div>

                      </div>

                    </div>


                    <div class="row">

                      <div class="col-md-4">

                        <label class="col-form-label">SubCity</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class=" mdi mdi-directions text-danger"></i></span>
                       
                            </div>

                            <input type="text" class="form-control" name="subcity" value="{{ auth()->user()->subcity }}"/>

                          </div>

                        </div>

                      </div>

                      <div class="col-md-4">

                        <label class="col-form-label">Woreda</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class="mdi mdi-directions-fork text-danger"></i></span>
                       
                            </div>

                            <input type="text" class="form-control" name="woreda" value="{{ auth()->user()->woreda }}"/>

                          </div>

                        </div>

                      </div>

                      <div class="col-md-4">

                        <label class="col-form-label">House Number</label>

                        <div class="form-group">

                          <div class="input-group">
                     
                            <div class="input-group-prepend">
                       
                              <span class="input-group-text"><i class=" mdi mdi-file-tree text-primary"></i></span>
                       
                            </div>

                            <input type="text" class="form-control" name="house_number" value="{{ auth()->user()->house_number }}"/>

                          </div>

                        </div>

                      </div>
                      
                    </div>

                    <div class="row mt-5">

                      <div class="col-md-6">

                        <button type="submit" class="btn btn-gradient-danger mr-2">Update<i class=" mdi mdi-pen ml-2"></i></button>

                      </div>

                    </div>

                    <div class="text-center">

                      @include('index-page.success_nd_error_page.success')

                      @include('index-page.success_nd_error_page.error_')

                    </div>

                  </form>

                </div>

              </div>

            </div>



        </div>



    </div>



    @include('main_layout.layout_uno.footer')



</div>

<script type="text/javascript" src="../../../js/datepicker.js"></script>

@endsection