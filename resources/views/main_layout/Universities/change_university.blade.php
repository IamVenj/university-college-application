@extends ('main_layout.layout_uno.index')



@section('universities_content')

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Change University Status</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Change University, Field and academics(certificate)

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/change-university/{{auth()->user()->id}}" method="post">

	                  		@csrf

	                  		@method('PATCH')

		                    <div class="form-group">

	                          	<label for="university_id" style="font-size: 20px;">University</label>

	                          	<select name="university" class="form-control dynamic" id="university_id" data-dependent="academics_id" data-academics="academics_name" data-dbtable="academics">

	                              	<option selected disabled>Select University</option>

	                              	@foreach($university as $uni)

	                              	<option value="{{$uni->id}}">{{$uni->university_name}}</option>

	                              	@endforeach
	                              	
	                          	</select>

	                      	</div>

		                   	<div class="form-group">

	                          	<label for="academics" style="font-size: 20px;">Applying For</label>

	                          	<select name="certificate" class="form-control dynamic" id="academics_id" data-dependent="course_id" data-course="course_name" data-dbtable="courses">

	                          	</select>

	                        </div>

	                        <div class="form-group">

	                          	<label for="course" style="font-size: 20px;">Field</label>

	                          	<select name="field" class="form-control dynamic" id="course_id" data-courses="course_name_" data-dependent="admission_date">

	                          	</select>

	                        </div>

	                        <div id="admission_date"></div>

		                    <div class="row">

		                    	<div class="col-md-6">

		                    		<button type="submit" class="btn btn-gradient-info mr-2">Change</button>

		                    	</div>

		                    </div>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript" src="../../../js/change_university.js"></script>

@endsection