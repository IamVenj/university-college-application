@extends ('main_layout.layout_uno.index')



@section('universities_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create University</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create University

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-university" method="post">

	                  		@csrf

		                    <div class="form-group">

		                      	<label for="universityName" style="font-size: 20px;">Name of University</label>

		                      	<input type="text" class="form-control" id="universityName" name="university_name" placeholder="Name of University">

		                    </div>

		                   

		                    <div class="row">

		                    	<div class="col-md-6">

		                    		<button type="submit" class="btn btn-gradient-info mr-2">Add<i class="fa fa-plus ml-2"></i></button>

		                    	</div>


		                    </div>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

	@include('main_layout.Universities.edit_university')        

    </div>

    @include('main_layout.layout_uno.footer')

</div>
<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>

@endsection