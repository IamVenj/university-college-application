<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title">Created University</h4>
    
        <p class="card-description">
    
          Number of Created University: <code>{{ $universities->count() }}</code>
    
        </p>

        <div class="table-responsive">
    
          <table class="table table-hover" id="dataTables-example">
      
            <thead>
      
              <tr>
      
                <th style="font-size: 20px;">Name of University</th>
      
                <th></th>

                <th></th>
      
              </tr>
      
            </thead>
      
            <tbody>
      
              @foreach($universities as $university)

              <tr>

                @include('main_layout.modals.universityModal')
      
                <td style="font-size: 15px;">{{ $university->university_name }}</td>
      
                <td><button type="button" class="btn btn-success" data-toggle="modal" data-target=<?= '#edit_university_Modal'.$university->id; ?>>Edit</button></td>

                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target=<?= '#delete_university_Modal'.$university->id; ?>>Delete</button></td>
                
              </tr>

              @endforeach
      
            </tbody>
      
          </table>
    
        </div>

      </div>
    
    </div>

  </div>

</div>

