@extends ('main_layout.layout_uno.index')




@section ('dashboard_content')



  <!-- partial -->

  <div class="main-panel">

      <div class="content-wrapper-index">

        <div class="page-header">

          <h3 class="page-title">

            <span class="page-title-icon bg-info text-white mr-2">

              <i class="mdi mdi-crown "></i>                 

            </span>

            Dashboard

          </h3>


          <nav aria-label="breadcrumb">

            <ul class="breadcrumb">

              <li class="breadcrumb-item active" aria-current="page">

                <span></span>Overview

                <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>

              </li>

            </ul>

          </nav>

        </div>

        <div class="dropdown-divider mt-0 mb-4 w-25"></div>

        <div class="row">

          <div class="col-md-6 stretch-card grid-margin">

            <div class="card bg-gradient-danger card-img-holder text-white">

              <div class="card-body">

                <img src="../../../../image/circle.svg" class="card-img-absolute" alt="circle-image"/>

                <h4 class="font-weight-normal mb-3">Total Number of Applicants

                  <i class="mdi mdi-chart-line mdi-24px float-right"></i>

                </h4>

                @if(auth()->user()->role_id == 1)

                <?php

                $me = new App\university_membership();

                $this_applicant = App\university_membership::where('role_id', null)->groupBy('user_id')->get();

                ?>

                <h2 class="mb-5">{{$this_applicant->count()}}</h2>

                @elseif(auth()->user()->role_id == 2)

                <?php

                $me = new App\university_membership();

                $this_applicant = App\university_membership::where('university_id', $me->get_university_id_of_this())->where('role_id', null)->count();

                ?>

                <h2 class="mb-5">{{$this_applicant}}</h2>

                @endif

              </div>

            </div>

          </div>



          <div class="col-md-6 stretch-card grid-margin">

            <div class="card bg-gradient-info card-img-holder text-white">

              <div class="card-body">

                <img src="../../../../image/circle.svg" class="card-img-absolute" alt="circle-image"/>                  

                <h4 class="font-weight-normal mb-3">Total Number of Employees

                  <i class="mdi mdi-account mdi-24px float-right"></i>

                </h4>

                @if(auth()->user()->role_id == 1)

                <?php

                $me = new App\university_membership();

                $this_applicant = App\university_membership::where('role_id', 3)->count();

                ?>

                <h2 class="mb-5">{{$this_applicant}}</h2>

                @elseif(auth()->user()->role_id == 2)

                <?php

                $me = new App\university_membership();

                $this_applicant = App\university_membership::where('university_id', $me->get_university_id_of_this())->where('role_id', 3)->count();

                ?>

                <h2 class="mb-5">{{$this_applicant}}</h2>

                @endif

              </div>

            </div>

          </div>


          <!-- <div class="col-md-4 stretch-card grid-margin">

            <div class="card bg-gradient-success card-img-holder text-white">

              <div class="card-body">

                <img src="../../../../image/circle.svg" class="card-img-absolute" alt="circle-image"/>                                    

                <h4 class="font-weight-normal mb-3">Visitors Online

                  <i class="mdi mdi-diamond mdi-24px float-right"></i>

                </h4>

                <h2 class="mb-5">95,5741</h2>

                <h6 class="card-text">Increased by 5%</h6>

              </div>

            </div>

          </div> -->

        </div>



        <div class="row">

          @if(auth()->user()->role_id == 1)

          <div class="col-md-7 grid-margin stretch-card">

          @elseif(auth()->user()->role_id == 2)

          <div class="col-md-7 grid-margin stretch-card">

          @endif

            <div class="card">

              <div class="card-body">

                <div class="clearfix">

                  <h4 class="card-title float-left">Number of Applicants<i class=" mdi mdi-chart-bar ml-2"></i></h4>

                  <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div>                                     

                </div>

                <canvas id="visit-sale-chart" class="mt-4"></canvas>

              </div>

            </div>

          </div>

          @if(auth()->user()->role_id == 1)

          <div class="col-md-5 grid-margin stretch-card">

            <div class="card">

              <div class="card-body">

                <h4 class="card-title">Male & Female Applicants</h4>

                <canvas id="traffic-chart"></canvas>

                <div id="traffic-chart-legend" class="rounded-legend legend-vertical legend-bottom-left pt-4"></div>                                                      

              </div>

            </div>

          </div>

          @endif

        </div>

      </div>

    @include('main_layout.layout_uno.footer')

  </div>

  <script src="../../../../js/dashboard.js"></script>

@endsection