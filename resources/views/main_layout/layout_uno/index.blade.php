<!DOCTYPE html>

<html>

<head>


  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <title>MoSHE Admission</title>
    

  <link href="../../../font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  
  <link rel="stylesheet" href="../../../../css/vendor.bundle.base.css">

  
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="../../../font/mdi/css/materialdesignicons.min.css">



  <link rel="stylesheet" href="../../../../css/style.css">

    
  <link rel="shortcut icon" href="../../../../image/logo-22.png" />

</head>



<body>

    <div class="container-scroller">

          @include('main_layout.layout_uno.nav')

            <div class="container-fluid page-body-wrapper">

                  @include('main_layout.layout_uno.sidebar')


                  @yield('dashboard_content')

                  @yield('academics_content')

                  @yield('create_course_content')

                  @yield('universities_content')

                  @yield('create_admin_user_content')

                  @yield('create_employee_content')

                  @yield('create_role_content')

                  @yield('info_post_content')

                  @yield('admin_control_panel_content')

                  @yield('employee_control_panel_content')

                  @yield('create_add_steps_content')

                  @yield('create_step_one_content')

                  @yield('create_step_two_content')
                  
                  @yield('create_step_three_content')

                  @yield('create_step_four_content')

                  @yield('steps_content')

                  @yield('change-p_content')

                  @yield('students_content')

                  @yield('main_question_content')

                  @yield('upload_content')

            </div>

    </div>

</body>



</html>