@if(auth()->user())

<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
  
    <a class="navbar-brand brand-logo">

      <div class="row">
               
        <img src="../../../image/logo-22.png" class="ml-4 mr-0" style="width: 40px; height: 40px; position: absolute;">
        
        <div class="col-sm-9 ml-5 ml-0">

          <p class="mb-0 tracking-wide" style="font-size: 15px;">የሳይንስና ከፍተኛ ትምህርት ሚኒስቴር</p>

          <p class="mt-0 mb-0" style="font-size: 10px;">Ministry of Science and Higher Education</p>

        </div>

      </div>

    </a>
  
    <a class="navbar-brand brand-logo-mini"><img src="../../../image/logo-22.png" class="ml-2" style="width: 40px; height: 40px;"></a>
  
  </div>
  
  

  <div class="navbar-menu-wrapper d-flex align-items-stretch">

    <ul class="navbar-nav navbar-nav-right">
  
      <li class="nav-item nav-profile dropdown">
  
        <a class="nav-link " id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
  
          <div class="nav-profile-text">

            
  
            <p class="mb-1 text-black">{{ auth()->user()->firstname.' '.auth()->user()->middlename.' '.auth()->user()->lastname }}</p>

            
  
          </div>

          <i class='fa fa-angle-down ml-3 mb-1'></i>
  
        </a>
  
        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
  
          <a class="dropdown-item" href="/change-password">
  
            <i class=" mdi mdi-key-change mr-2 text-success"></i>
  
            Change Password
  
          </a>
  
          @if(is_null(auth()->user()->role_id))

          <div class="dropdown-divider2"></div>
  
          <a class="dropdown-item" href="/change-university">

            <i class="mdi mdi-redo-variant mr-2 text-danger"></i>

            Change University Status

          </a>

          @endif

          <div class="dropdown-divider2"></div>
  
          <a class="dropdown-item" href="/logout">

            <i class="mdi mdi-logout mr-2 text-primary"></i>

            Signout

          </a>
            
        </div>
  
      </li>
  
  

      <li class="nav-item d-none d-lg-block full-screen-link">
  
        <a class="nav-link">
  
          <i class="mdi mdi-fullscreen" id="fullscreen-button"></i>
  
        </a>
  
      </li>

      <li class="nav-item nav-logout d-none d-lg-block">
  
        <a class="nav-link" href="/logout">
  
          <i class="mdi mdi-power"></i>
  
        </a>
  
      </li>

    </ul>
  


    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
  
      <span class="mdi mdi-menu"></span>
  
    </button>
  
  </div>


</nav>

@endif