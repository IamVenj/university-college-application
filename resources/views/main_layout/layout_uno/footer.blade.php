<footer class="footer">
  	
  	<div class="d-sm-flex justify-content-center justify-content-sm-between">
    
    	<span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <?= date('Y'); ?> <a href="" target="_blank">Admission</a>. All rights reserved.</span>
    
    	<span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="fa fa-heart text-danger mr-2 ml-1"></i><!-- <a href="https://www.tibebtech.net/" target="_blank">TibebTech</a> --></span>
  	
  	</div>

</footer>

<script src="../../../../js/vendors/jquery/jquery.min.js"></script>

<script src="../../../../js/vendors/js/vendor.bundle.base.js"></script>

<script src="../../../../js/vendors/js/vendor.bundle.addons.js"></script>

<script src="../../../../js/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script src="../../../../js/vendors/js/vendor.bundle.addons.js"></script>

<script src="../../../../js/vendors/headroom/headroom.min.js"></script>

<script src="../../../../js/off-canvas.js"></script>

<script src="../../../../js/misc.js"></script>

<script type="text/javascript" src="../../../js/datepicker.js"></script>


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">