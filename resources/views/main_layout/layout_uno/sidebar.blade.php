@if(auth()->user())

<nav class="sidebar sidebar-offcanvas" id="sidebar">

  <ul class="nav">

    <li class="nav-item nav-profile">

      <a href="#" class="nav-link">

        <div class="nav-profile-text d-flex flex-column mt-4 mb-3">

          <div class="row">

            <span class="font-weight-bold mb-2 ml-2"> {{ auth()->user()->firstname.' '.auth()->user()->middlename.' '.auth()->user()->lastname }} </span>

            <i class="fa fa-bookmark text-success nav-profile-badge ml-3"></i>

          </div>

        </div>


        

      </a>

    </li>

    <!-- superadmin and admin -->

    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2)

      <li class="nav-item">

        <a class="nav-link" href="/dashboard">

          <span class="menu-title">Dashboard</span>

          <i class="mdi mdi-crown  menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2 || auth()->user()->role_id == 3)

      <li class="nav-item">

        <a class="nav-link" href="/profile">

          <span class="menu-title">Profile</span>

          <i class="mdi mdi-account menu-icon"></i>

        </a>

      </li>

    @endif

   @if(auth()->user()->role_id == 1)

      <li class="nav-item">

        <a class="nav-link" href="/admin-panel">

          <span class="menu-title">Admin Control Panel</span>

          <i class="mdi mdi-buffer menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 2)

      <li class="nav-item">

        <a class="nav-link" href="/employee-panel">

          <span class="menu-title">Employee Control Panel</span>

          <i class="mdi mdi-buffer menu-icon"></i>

        </a>

      </li>

    @endif



    <!-- admin -->

    @if(auth()->user()->role_id == 2)

      <li class="nav-item">

        <a class="nav-link" href="/create-academics">

          <span class="menu-title">Create Academics</span>

          <i class=" mdi mdi-book-plus menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 2)

      <li class="nav-item">

        <a class="nav-link" href="/create-course">

          <span class="menu-title">Create Fields</span>

          <i class="mdi mdi-bookmark-plus menu-icon"></i>

        </a>

      </li>

    @endif

    <!-- superadmin -->

    @if(auth()->user()->role_id == 1)

      <li class="nav-item">

        <a class="nav-link" href="/create-university">

          <span class="menu-title">Create University</span>

          <i class=" mdi mdi-plus menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 3)

      <li class="nav-item">

        <a class="nav-link" href="/students">

          <span class="menu-title">Applicants</span>

          <i class=" mdi mdi-account-multiple  menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 3)

      <li class="nav-item">

        <a class="nav-link" href="/accepted">

          <span class="menu-title">Accepted</span>

          <i class=" mdi mdi-account-check menu-icon"></i>

        </a>

      </li>

    @endif

    @if(auth()->user()->role_id == 3)

      <li class="nav-item">

        <a class="nav-link" href="/rejected">

          <span class="menu-title">Rejected</span>

          <i class="mdi mdi-account-off menu-icon"></i>

        </a>

      </li>

    @endif

    

    <!-- superadmin -->

    @if(auth()->user()->role_id == 1)

    <li class="nav-item">

      <a class="nav-link" href="/create-admin">

        <span class="menu-title">Create University Admin</span>

        <i class=" mdi mdi-account-multiple-plus  menu-icon"></i>

      </a>

    </li>

    @endif

    <!-- admin -->

    @if(auth()->user()->role_id == 2)

    <li class="nav-item">

      <a class="nav-link" href="/create-employee">

        <span class="menu-title">Create Employee</span>

        <i class=" mdi mdi-account-multiple-plus  menu-icon"></i>

      </a>

    </li>

    @endif

    <!-- admin -->

    @if(auth()->user()->role_id == 2)

    <li class="nav-item">

      <a class="nav-link" href="/post">

        <span class="menu-title">Post Information</span>

        <i class="mdi mdi-plus menu-icon"></i>

      </a>

    </li>

    @endif


    @if(auth()->user()->role_id == 2)

    <?php 

      $university_membership = App\university_membership::where('user_id', auth()->user()->id);

      $university_id = $university_membership->pluck('university_id')->get(0);

      $step_quantity = App\step_quantities::where('university_id', $university_id);

    ?>



    <li class="nav-item">

      <a class="nav-link" data-toggle="collapse" href="#admission_question" aria-expanded="false" aria-controls="steps">

        <span class="menu-title">Admission Questions</span>

        <i class="menu-arrow"></i>

      </a>

      <div class="collapse" id="admission_question">

        <ul class="nav flex-column sub-menu">

          

          @if($step_quantity->count() == 0)

          <li class="nav-item"> <a class="nav-link" href="/add-steps">Add Steps<i class="mdi mdi-plus ml-2"></i></a></li>

          @else

          <li class="nav-item"> <a class="nav-link" href="/add-steps">Edit Steps<i class="mdi mdi-edit ml-2"></i></a></li>

          @endif

          <?php 

            if($step_quantity->count() > 0)

            {

              $step_quantity_plucked = $step_quantity->pluck('step_quantity')->get(0);

          ?>

            @if($step_quantity_plucked == 1)

            <li class="nav-item"> <a class="nav-link" href="/create-step1">Step One<i class="fa fa-chart ml-2"></i></a></li>

            @endif

            @if($step_quantity_plucked == 2)
            
            <li class="nav-item"> <a class="nav-link" href="/create-step1">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/create-step2">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            @endif

            @if($step_quantity_plucked == 3)

            <li class="nav-item"> <a class="nav-link" href="/create-step1">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/create-step2">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/create-step3">Step Three<i class="fa fa-chart ml-2"></i></a></li>

            @endif

            @if($step_quantity_plucked == 4)

            <li class="nav-item"> <a class="nav-link" href="/create-step1">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/create-step2">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/create-step3">Step Three<i class="fa fa-chart ml-2"></i></a></li>
            
            <li class="nav-item"> <a class="nav-link" href="/create-step4">Step Four<i class="fa fa-chart ml-2"></i></a></li>

            @endif

          <?php } ?>

        </ul>

      </div>

    </li>

    @endif

    @if(is_null(auth()->user()->role_id))

    <li class="nav-item">

      <a class="nav-link" data-toggle="collapse" href="#main_questions" aria-expanded="false" >

        <span class="menu-title">Main Question</span>

        <i class="menu-arrow"></i>

      </a>

      <div class="collapse" id="main_questions">

        <ul class="nav flex-column sub-menu">

          <div class="row">

            <div class="col-md-9">

              <?php

              $submission = App\award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

              ?>

              @if($submission == 0)

              <li class="nav-item"> <a class="nav-link" href="/my-info">Personal Information</a></li>

              @else

              <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Personal Information</a></li>

              @endif

              

            </div>

            <div class="col-md-2" style="align-self: center;">

              @if(!is_null(auth()->user()->firstname) && !is_null(auth()->user()->middlename) && !is_null(auth()->user()->lastname) && !is_null(auth()->user()->sex) && !is_null(auth()->user()->date_of_birth) && !is_null(auth()->user()->country_code) && !is_null(auth()->user()->phone_number) && !is_null(auth()->user()->address_line) && !is_null(auth()->user()->city) && !is_null(auth()->user()->subcity) && !is_null(auth()->user()->woreda) && !is_null(auth()->user()->house_number))
              
              <input type="checkbox" name="personal_info_check" class='' disabled checked>

              @else

              <input type="checkbox" name="personal_info_check" disabled>

              @endif

            </div>

          </div>

          <div class="row">

            <div class="col-md-9">

              @if($submission == 0)

              <li class="nav-item"> <a class="nav-link" href="/education">Education</a></li>

              @else
              
              <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Education</a></li>

              @endif

            </div>

            <div class="col-md-2" style="align-self: center;">

              <?php

              $education = App\education::where('user_id', auth()->user()->id)->first();

              $prev_ed = App\previous_education::where('user_id', auth()->user()->id)->count();

              $highschool = App\highschool::where('user_id', auth()->user()->id)->count();

              $future_plans = App\future_plans::where('user_id', auth()->user()->id)->first();

              ?>

              @if(is_null($education->previous_education))
              
              <input type="checkbox" name="education_check" disabled>

              @endif
              
              @if($education->previous_education == 'yes')

                @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')

                  @if(!is_null($education->number_of_highschools) && !is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->national_exam_result) && !is_null($education->number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))

                    @if($highschool > 0 && $prev_ed > 0)

                      <input type="checkbox" name="education_check" checked disabled>

                    @else

                      <input type="checkbox" name="education_check" disabled>

                    @endif

                  @endif

                @else

                  @if(!is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))

                    @if($prev_ed > 0)

                      <input type="checkbox" name="education_check" checked disabled>

                    @else

                      <input type="checkbox" name="education_check" disabled>

                    @endif

                  @endif

                @endif

              @else

                @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')

                  @if(!is_null($education->number_of_highschools) && !is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->national_exam_result) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended) && !is_null($education->number_of_honors))

                    @if($highschool > 0)

                      <input type="checkbox" name="education_check" checked disabled>

                    @else

                      <input type="checkbox" name="education_check" disabled>

                    @endif

                  @endif

                @else

                  @if(!is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))

                    <input type="checkbox" name="education_check" checked disabled>

                  @endif

                @endif

              @endif


            </div>

          </div>

          <div class="row">

            <div class="col-md-9">

              @if($submission == 0)

              <li class="nav-item"> <a class="nav-link" href="/fam">Family</a></li>

              @else

              <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Family</a></li>

              @endif

            </div>

            <div class="col-md-2" style="align-self: center;">

              <?php

              $family = App\family::where('user_id', auth()->user()->id)->first();

              ?>

              @if(!is_null($family))

              <?php

              $father = App\father::where('id', $family->father_id)->count();

              $mother = App\mother::where('id', $family->mother_id)->count();

              $guardian = App\guardian::where('id', $family->guardian_id)->count();

              $other = App\other::where('id', $family->other_id)->count();

              ?>

                @if(!is_null($family->father_id) || !is_null($family->mother_id) || !is_null($family->guardian_id) || !is_null($family->other_id) || $family->currently_living_with == 'Alone')

                  @if($father > 0 || $mother > 0 || $guardian > 0 || $other > 0 || $family->currently_living_with == 'Alone')

                    @if(!is_null($family->marital_status) && !is_null($family->number_of_siblings) && !is_null($family->children))

                      @if($family->children == 'yes')

                        <?php

                        $children = App\children::where('user_id', auth()->user()->id)->count();

                        ?>

                        @if($children > 0)

                          <input type="checkbox" name="family_check" checked disabled>

                        @endif

                      @else

                        <input type="checkbox" name="family_check" checked disabled>

                      @endif

                    @endif

                  @else

                    <input type="checkbox" name="family_check" disabled>

                  @endif

                @endif

              @else

                <input type="checkbox" name="family_check" disabled>

              @endif
              
              

            </div>

          </div>

          <div class="row">

            <div class="col-md-9">

              @if($submission == 0)
            
              <li class="nav-item"> <a class="nav-link" href="/activities">Activities<i class="fa fa-chart ml-2"></i></a></li>

              @else

              <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Activities</a></li>

              @endif

            </div>

            <div class="col-md-2" style="align-self: center;">

              <?php

              $activity = App\activities::where('user_id', auth()->user()->id)->count();

              ?>

              @if($activity > 0)

              <input type="checkbox" name="activities_check" checked disabled>

              @else
              
              <input type="checkbox" name="activities_check" disabled>

              @endif

            </div>

          </div>

          <div class="row">

            <div class="col-md-9">

              @if($submission == 0)
            
              <li class="nav-item"> <a class="nav-link" href="/writing">Writing<i class="fa fa-chart ml-2"></i></a></li>

              @else

              <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Writing</a></li>

              @endif

            </div>

            <div class="col-md-2" style="align-self: center;">

              <?php

              $writing = App\writing::where('user_id', auth()->user()->id)->count();

              ?>

              @if($writing > 0)
              
              <input type="checkbox" name="writing_check" checked disabled>

              @else

              <input type="checkbox" name="writing_check" disabled>

              @endif

            </div>

          </div>

        </ul>

      </div>

    </li>

    @endif

    @if(is_null(auth()->user()->role_id))

    <?php 

      $step_quantity = App\step_quantities::where('university_id', auth()->user()->university_id);

    ?>

    @if($step_quantity->count() > 0)

    <li class="nav-item">

      <a class="nav-link" data-toggle="collapse" href="#questions" aria-expanded="false" aria-controls="ui-basic">

        <span class="menu-title">Institution Question</span>

        <i class="menu-arrow"></i>

      </a>

      <div class="collapse" id="questions">

        <ul class="nav flex-column sub-menu">          

          <?php 

            if($step_quantity->count() > 0)

            {

              $step_quantity_plucked = $step_quantity->pluck('step_quantity')->get(0);

          ?>

            @if($step_quantity_plucked == 1)

            @if($submission == 0)

            <li class="nav-item"> <a class="nav-link" href="/step-one">Step One<i class="fa fa-chart ml-2"></i></a></li>

            @else

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step One</a></li>

            @endif

            @endif

            @if($step_quantity_plucked == 2)
            
            @if($submission == 0)

            <li class="nav-item"> <a class="nav-link" href="/step-one">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-two">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            @else

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step One</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Two</a></li>

            @endif

            @endif

            @if($step_quantity_plucked == 3)

            @if($submission == 0)

            <li class="nav-item"> <a class="nav-link" href="/step-one">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-two">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-three">Step Three<i class="fa fa-chart ml-2"></i></a></li>

            @else

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step One</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Two</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Three</a></li>

            @endif

            @endif

            @if($step_quantity_plucked == 4)

            @if($submission == 0)

            <li class="nav-item"> <a class="nav-link" href="/step-one">Step One<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-two">Step Two<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-three">Step Three<i class="fa fa-chart ml-2"></i></a></li>

            <li class="nav-item"> <a class="nav-link" href="/step-four">Step Four<i class="fa fa-chart ml-2"></i></a></li>

            @else

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step One</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Two</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Three</a></li>

            <li class="nav-item"> <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">Step Four</a></li>

            @endif

            @endif

          <?php } ?>

        </ul>

      </div>

    </li>

    @endif

    @endif


    @if(is_null(auth()->user()->role_id))

    @if($submission == 0)

      <li class="nav-item">

        <a class="nav-link" href="/upload-doc">

          <span class="menu-title">Upload Documents</span>

          <i class="mdi mdi-upload menu-icon"></i>

        </a>

      </li>

    @else

      <li class="nav-item"> 

        <a class="nav-link" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="You Cannot access this page once your form is submitted" data-container="body" data-animation="true">

          <span class="menu-title">Upload Documents</span>

          <i class="mdi mdi-upload menu-icon"></i>

        </a>

      </li>

    @endif

    @endif

    @if(is_null(auth()->user()->role_id))

    <?php

    $submission = App\award_status::where('user_id', auth()->user()->id)->count();

    ?>

    @if($submission > 0)

    <li class="nav-item">

      <a class="nav-link" href="/institution">

        <span class="menu-title">Institutions</span>

        <i class="mdi mdi-school menu-icon"></i>

      </a>

    </li>

    @endif
    
    @endif
   

    @if(is_null(auth()->user()->role_id))

    <?php

    $submission = App\award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

    ?>

    <li class="nav-item sidebar-actions mt-3">

      <span class="nav-link">

        <div class="border-bottom">

          <h6 class="font-weight-normal mb-3 ml-4" style="color:rgba(0,0,50,0.5);">are you done?<i class=" mdi mdi-arrow-down ml-4"></i></h6>                

        </div>

        <form action="/submit" method="post">

          @csrf

          @if($submission == 0)

          <button class="btn btn-block btn-lg btn-gradient-danger mt-4">Submit All</button>

          @else

          <button class="btn btn-block btn-lg btn-gradient-danger mt-4" disabled title="Button is disabled once your form is submitted">Submit All</button>

          @endif

        </form>

      </span>

    </li>

    @endif

  </ul>

</nav>

@endif