@extends ('main_layout.layout_uno.index')



@section('create_employee_content')

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create University Employee</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create University Employee

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-employee" method="post">

	                  		@csrf

		                    <div class="form-group">

                                <label for="universityAdminEmail" style="font-size: 20px;">Email</label>

                                <input type="text" class="form-control" id="universityAdminEmail" name="email" placeholder="Email" required>

                            </div>

                            <div class="form-group">

                                <label for="universityAdminPassword" style="font-size: 20px;">Password</label>

                                <input type="password" class="form-control" id="universityAdminPassword" name="password" placeholder="Password" required>

                            </div>                   

		                    <button type="submit" class="btn btn-gradient-info mr-2" name="">Create</button>

		                    <div class="text-center">

	                        	@include('index-page.success_nd_error_page.success')

	                          	@include('index-page.success_nd_error_page.error_')

	                        </div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.user.edit_employee')

    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>
	
@endsection