<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title">Created Employees</h4>
    
        <p class="card-description">
    
          Employees: <code>@if($user!=null) {{ $user->count() }} @else 0 @endif</code>
    
        </p>

        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

        <div class="table-responsive">
    
          <table class="table table-hover" id="dataTables-example">
      
            <thead>
      
              <tr>
      
                <th style="font-size: 20px;">Email</th>
      
                <th></th>

                <th></th>
      
              </tr>
      
            </thead>
      
            <tbody>

              @if($user!=null)
      
              @foreach($user as $u)

              <?php 

              $employees = App\User::where('id', $u->user_id)->get();

              ?>

              @foreach($employees as $users)

              <tr>

                @include('main_layout.modals.employeeModal')
      
                <td style="font-size: 16px;">{{ $users->email }}</td>
      
                <td><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#edit_employee_Modal'.$users->id; ?>>Edit</button></td>

                <td><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_employee_Modal'.$users->id; ?>>Delete</button></td>
      
      
              </tr>

              @endforeach

              @endforeach

              @endif
      
            </tbody>
      
          </table>

        </div>
    
      </div>

    </div>

  </div>

</div>