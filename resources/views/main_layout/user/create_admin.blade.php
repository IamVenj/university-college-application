@extends ('main_layout.layout_uno.index')



@section('create_admin_user_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		    <div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create University Admin</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create University Admin

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-admin" method="post">

	                  		@csrf

                        <input type="hidden" name="role_id" value=2>

		                    <div class="form-group">

		                      	<label for="universityAdminEmail" style="font-size: 20px;">Email</label>

		                      	<input type="text" class="form-control" id="universityAdminEmail" name="email" placeholder="Email" required>

		                    </div>

                        <div class="form-group">

                            <label for="universityAdminPassword" style="font-size: 20px;">Password</label>

                            <input type="password" class="form-control" id="universityAdminPassword" name="password" placeholder="Password" required>

                        </div>

		                   	<div class="form-group">

                          	<label for="exampleFormControlSelect3" style="font-size: 20px;">University<i class="fa fa-plus ml-2"></i></label>

                          	<select name="university" class="form-control" id="university_id">

                              	<option selected disabled>Select University</option>

                              	@foreach($universities as $university)

                              	<option value="{{ $university->id }}">{{ $university->university_name }}</option>

                              	@endforeach

                          	</select>

                      	</div>

		                    <button type="submit" class="btn btn-gradient-info mr-2" name="">Create</button>

                        <div class="text-center">

                            @include('index-page.success_nd_error_page.success')

                            @include('index-page.success_nd_error_page.error_')

                        </div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection