<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title" style="font-size: 20px;">Uploads</h4>
    
        
    
        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

        <table class="table table-hover">
    
          <thead>
    
            <tr>
    
              <th style="font-size: 18px;">Document</th>

              <th style="font-size: 18px;">Document Type</th>
    
              <th></th>

              <th></th>
    
            </tr>
    
          </thead>
    
          <tbody>

            @if($count > 0)

            @foreach($docc as $doc)
    
            <tr>

              

              @include('main_layout.modals.uploadModal')
    
              <td style="font-size: 15px;">{{ $doc->document }}</td>

              @if($doc->document_type == 'h_transcript')

              <td style="font-size: 15px;">Highschool Transcript</td>

              @elseif($doc->document_type == 'n_e_doc')

              <td style="font-size: 15px;">National Examination Document</td>

              @elseif($doc->document_type == 'degree')

              <td style="font-size: 15px;">Degree</td>

              @endif

              <td style="font-size: 17px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_upload_Modal'.$doc->id; ?>>Delete</button></td>

              

            </tr>

            @endforeach

            @endif
    
          </tbody>
    
        </table>
    
      </div>
    </div>

  </div>

</div>