@extends ('main_layout.layout_uno.index')

@section('upload_content')


<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Upload Document</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Upload your @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate') Transcript and National Examination Document @else Grade Results @endif 

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/upload-doc" method="post" enctype="multipart/form-data">

	                  		@csrf

	                  		<div class="row">

	                            <div class="col-md-6">

				                    <div class="form-group">
		                                  
		                                <label style="font-size: 20px;">Document Type</label>
		                        
		                                <select name="document_type" class="form-control">
		            
		                                    <option selected disabled>Choose Type</option>
		                
		                                    @if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')
		                
		                                    <option Value="h_transcript">Highschool Transcript</option>
		                                    
		                                    <option Value="n_e_doc">National Examination Document</option>

		                                    @else

		                                    <option value="degree">Degree Document</option>

		                                    @endif
							                        
		                                </select>
		                                
		                            </div>

		                        </div>

		                    </div>

		                    <p class="card-description mt-4">Upload Only PDF files<i class="mdi mdi-arrow-down ml-2"></i></p>

	                  		<div class="row">

	                  			<div class="col-md-2">

			                  		<label class="file-select">

		                              <div class="select-button">

		                                <span class="p-2 pb-3" style='cursor:pointer; font-size:14.5px; font-style:bold;'><i class="mdi mdi-file-document mr-2"></i>Choose Document</span>

		                              </div>

		                              <input type="file" class="file" name="upload_document">

		                            </label>

		                        </div>

		                    	<div class="col-md-2">

		                    		<button type="submit" class="btn btn-gradient-info btn-fw mr-2" style="font-size: 18px;">Upload<i class="mdi mdi-upload ml-2"></i></button>

		                    	</div>

		                    </div>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.upload_document.edit_upload')

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection