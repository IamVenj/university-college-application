@extends ('main_layout.layout_uno.index')



@section('steps_content')

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Step Two</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Step Two Questions

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/step-two" method="post">

	                  		@csrf

	                  		@foreach($show_questions as $show_question)

	                  		<?php

	                  			$Check_and_Radio_Question_Lists = App\Check_and_Radio_Question_List::where('question_id', $show_question->id)->get();

	                  		?>

	                  		<?php

					        $answer = new App\answer_for_university_questions();

					        // $university_membership = new App\university_membership();

					        // $university_id = $university_membership->get_university_id_of_this();

					        $answers = $answer->get_answers(auth()->user()->id, 2, auth()->user()->university_id, $show_question->id);

					        ?>
	                      	
	                      	<input type="hidden" name="question_id[]" value="{{$show_question->id}}">

	                      	<input type="hidden" name="step[]" value="{{$show_question->step}}">

		                  		@if($show_question->input_type == 'textarea')

			                      	@if(!$answers->isEmpty())

			                  		<div class="form-group">

				                      	<label for="textarea_answer" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                      	@foreach($answers as $answer)

				                      	<input type="hidden" name="answer_id[]" value="{{$answer->id}}">

				                      	<{{$show_question->input_type}} class="form-control" rows="5" id="textarea_answer" name="answer[]" required >{{$answer->answer}} </{{$show_question->input_type}}>

				                      	

				                      	@endforeach

				                    </div>

				                    @else

				                    <div class="form-group">

				                      	<label for="textarea_answer" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                      	<{{$show_question->input_type}} class="form-control" rows="5" id="textarea_answer" name="answer[]" required> </{{$show_question->input_type}}>

				                    </div>

				                    @endif

		                  		@elseif($show_question->input_type == 'dropdown')

			                  		<label for="answer_select" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                    <div class="form-group mb-4">

				                    	@if(!$answers->isEmpty())

				                    	@foreach($answers as $answer)

				                    	<input type="hidden" name="answer_id[]" value="{{$answer->id}}">

				                          	<select name="answer[]" class="form-control" id="answer_select" >

				                              	<option selected disabled>Select {{$show_question->placeholder}}</option>

			                              		@foreach($Check_and_Radio_Question_Lists as $Check_and_Radio_Question_List)

			                              					                              			

				                              		<option value="{{$Check_and_Radio_Question_List->quest_box}}" <?php if($answer->answer == $Check_and_Radio_Question_List->quest_box){ ?> selected <?php } ?>>{{$Check_and_Radio_Question_List->quest_box}}</option>

				                              	

				                              	@endforeach
				                              	
				                          	</select>

				                          	@endforeach


				                      	@else

				                      		<select name="answer[]" class="form-control" id="answer_select">

				                              	<option selected disabled>Select {{$show_question->placeholder}}</option>

			                              		@foreach($Check_and_Radio_Question_Lists as $Check_and_Radio_Question_List)



				                              		<option value="{{$Check_and_Radio_Question_List->quest_box}}">{{$Check_and_Radio_Question_List->quest_box}}</option>

				                              	@endforeach
				                              	
				                          	</select>

				                      	@endif

				                    </div>

			                    @elseif($show_question->input_type == 'radio')

				                    <label for="radio_answer" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                    <div class="form-check-label mb-4">

				                    	@if(!$answers->isEmpty())

				                    	@foreach($Check_and_Radio_Question_Lists as $Check_and_Radio_Question_List)

					                    	@foreach($answers as $answer)

					                    	<input type="hidden" name="answer_id[]" value="{{$answer->id}}">

						                    	<span style="cursor:default;">

						                      	<input type="{{$show_question->input_type}}" value="{{$Check_and_Radio_Question_List->quest_box}}" class="form-control-input mt-3 ml-4" id="radio_answer" placeholder="{{$show_question->placeholder}}" name="answer[]" required <?php if($answer->answer == $Check_and_Radio_Question_List->quest_box){ ?> checked <?php } ?> >  {{$Check_and_Radio_Question_List->quest_box}}

						                      	</span>

					                      	@endforeach
				                      	
				                      	@endforeach

				                      	

				                      	@else

					                      	@foreach($Check_and_Radio_Question_Lists as $Check_and_Radio_Question_List)

						                    	<span style="cursor:default;">

						                      	<input type="{{$show_question->input_type}}" value="{{$Check_and_Radio_Question_List->quest_box}}" class="form-control-input mt-3 ml-4" id="radio_answer" placeholder="{{$show_question->placeholder}}" name="answer[]" required>  {{$Check_and_Radio_Question_List->quest_box}}

						                      	</span>

					                      	@endforeach

				                      	@endif

				                    </div>

			                    @else

				                    @if(!$answers->isEmpty())

				                    <div class="form-group">

				                      	<label for="textfield_answer" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                      	@foreach($answers as $answer)

				                      	<input type="hidden" name="answer_id[]" value="{{$answer->id}}">

				                      	<input type="{{$show_question->input_type}}" class="form-control" id="textfield_answer" name="answer[]" placeholder="{{$show_question->placeholder}}" value="{{ $answer->answer }}" required >

				                      	@endforeach

				                    </div>

				                    @else

				                    <div class="form-group">

				                      	<label for="textfield_answer" style="font-size: 20px;">{{$show_question->order.' . '.$show_question->question}}</label>

				                      	<input type="{{$show_question->input_type}}" class="form-control" id="textfield_answer" name="answer[]" placeholder="{{$show_question->placeholder}}" required>


				                    </div>

				                    @endif

			                    @endif

		                    @endforeach

		                    <button type="submit" class="btn btn-gradient-info mr-2">Save<i class="fa fa-save ml-2"></i></button>


		                    <div class="text-center">


	                          @include('index-page.success_nd_error_page.success')

	                          @include('index-page.success_nd_error_page.error_')

	                        </div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection