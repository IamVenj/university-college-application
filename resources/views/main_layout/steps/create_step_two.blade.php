@extends ('main_layout.layout_uno.index')



@section('create_step_two_content')

<style type="text/css">
	
	.how-many{
		display: none;
	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Step Two</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create Step Two Questions

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-step2" method="post">

	                  		@csrf

		                    <div class="form-group">

		                      	<label for="question" style="font-size: 20px;">Question<i class=" mdi mdi-comment-question-outline ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

		                      	<input type="text" class="form-control" id="question" name="question" placeholder="Question" required>

		                    </div>

		                    <div class="form-group">

		                      	<label for="question_placeholder" style="font-size: 20px;">Questions Placeholder<i class="mdi mdi-image-filter-center-focus-weak ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

		                      	<input type="text" class="form-control" id="question_placeholder" name="question_placeholder" placeholder="Placeholder">

		                    </div>

		                    <div class="form-group">

	                          	<label for="InputType" style="font-size: 20px;">Input Type<i class=" mdi mdi-altimeter ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

	                          	<select name="input_type" class="form-control" id="InputType">

	                              	<option selected disabled>Select Input</option>

	                              	<option value="text">TextField</option>
	                              	
	                              	<option value="textarea">TextArea</option>

	                              	<option value="dropdown">Dropdown</option>
	                              	
	                              	<option value="radio">Radio Button</option>

	                          	</select>

	                      	</div>

	                      	<div class="form-group how-many" id="how-many">

	                          	<label for="InputType" style="font-size: 20px;">How Many?</label>

	                          	<div class="row">

	                          		<div class="col-md-11">

			                          	<select class="form-control" id="numInputs">

			                              	<option selected disabled>How Many Inputs?</option>

			                              	<option value="1">1</option>
			                              	
			                              	<option value="2">2</option>

			                              	<option value="3">3</option>
			                              	
			                              	<option value="4">4</option>

			                              	<option value="5">5</option>
			                              	
			                              	<option value="6">6</option>

			                              	<option value="7">7</option>
			                              	
			                              	<option value="8">8</option>

			                          	</select>

			                        </div>

			                        <div class="col-md-1">

			                          	<i class="mdi mdi-delete-sweep" style="font-size: 40px; cursor:pointer; color:rgba(0,0,0,0.7);" id="drop_selected_input"></i>

			                         </div>

	                          	</div>

	                      	</div>

	                      	<div id="InputArea"></div>

	                      	<div class="form-group">

	                          	<label for="order_of_Q" style="font-size: 20px;">Order of The Question<i class=" mdi mdi-arrange-send-to-back ml-2"  style="color:rgba(0,0,0,0.4);"></i></label>

	                          	<select name="order" class="form-control" id="order_of_Q">

	                              	<option selected disabled>Select Order</option>

	                              	<option value="1">1</option>
	                              	
	                              	<option value="2">2</option>

	                              	<option value="3">3</option>
	                              	
	                              	<option value="4">4</option>

	                              	<option value="5">5</option>
	                              	
	                              	<option value="6">6</option>

	                              	<option value="7">7</option>
	                              	
	                              	<option value="8">8</option>

	                              	<option value="9">9</option>
	                              	
	                              	<option value="10">10</option>

	                          	</select>

	                      	</div>

	                      	<div class="form-group">

	                          	<label for="academics_id" style="font-size: 20px;">Academics<i class=" mdi mdi-book ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

	                          	<select name="academics_id" class="form-control dynamic" id="academics_id" data-dependent="course_name">

	                              	<option selected disabled>choose academics</option>

	                              	@foreach($academics as $academic)

	                              	<option value='{{$academic->id}}'>{{$academic->academics_name}}</option>
	                              	
	                              	@endforeach

	                          	</select>

	                      	</div>

	                      	<div class="form-group">

	                          	<label for="course_name" style="font-size: 20px;">Courses<i class="mdi mdi-book-open-page-variant ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

	                          	<select name="course_id" class="form-control" id="course_name">

	                              	

	                          	</select>

	                      	</div>

		                    <button type="submit" class="btn btn-gradient-info mr-2">Create Question</button>

		                    <div class="text-center">

	                          @include('index-page.success_nd_error_page.success')

	                          @include('index-page.success_nd_error_page.error_')

	                        </div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.steps.list_step_two')

    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript" src="../../../js/steps.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
	
		$('.dynamic').on('change', function(){

	      if($(this).val() != '')
	      {
	        
	        var select = $(this).attr('id');
	        
	        var value = $(this).val();
	        
	        var dependent = $(this).data('dependent');

	        var _token = $('input[name="_token').val();

	        $.ajax({
	          
	          url:"create-step2/fetch",

	          method: "POST",
	          
	          data:{select:select, value:value, _token:_token, dependent:dependent},

	          success:function(result)
	          {

	            $('#'+dependent).html(result);
	          
	          } 
	        
	        })
	      
	      }

	    });

	});

</script>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>

@endsection