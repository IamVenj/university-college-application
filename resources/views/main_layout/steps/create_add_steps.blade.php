@extends ('main_layout.layout_uno.index')



@section('create_add_steps_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

                		<?php 

			            $university_membership = App\university_membership::where('user_id', auth()->user()->id);

			            $university_id = $university_membership->pluck('university_id')->get(0);

			            $step_quantity = App\step_quantities::where('university_id', $university_id);



			          	?>

			          	@if($step_quantity->count() == 0)

	                  	<h4 class="card-title" style="font-size: 20px;">Add Steps</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Add Steps

	                  	</p>

	                  	@else

	                  	<h4 class="card-title" style="font-size: 20px;">Edit Steps</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Edit Steps

	                  	</p>

	                  	@endif

	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/add-steps" method="post">

	                  		@csrf

		                   

		                   	<div class="form-group">

	                          	<label for="Steps" style="font-size: 20px;">Steps<i class="fa fa-plus ml-2"></i></label>

	                          	<select name="add_steps" class="form-control" id="Steps">

	                              	<option selected disabled>Select Steps</option>

	                              	<option value="1">1</option>
	                              	
	                              	<option value="2">2</option>

	                              	<option value="3">3</option>
	                              	
	                              	<option value="4">4</option>

	                          	</select>

	                      	</div>

	                      	@if($step_quantity->count() == 0)

		                    <button type="submit" class="btn btn-gradient-info mr-2" >Add</button>

		                    @else

		                    <button type="submit" class="btn btn-gradient-info mr-2" >Change</button>

		                    @endif

	                        <div class="text-center">

	                            @include('index-page.success_nd_error_page.success')

	                            @include('index-page.success_nd_error_page.error_')

	                        </div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>

@endsection