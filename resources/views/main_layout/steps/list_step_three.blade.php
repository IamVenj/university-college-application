<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title">Step Three Question</h4>
    
        <p class="card-description">
    
          Number of Created Step Three Questions: <code>{{$question_model->count()}}</code>
    
        </p>

        <div class="table-responsive">
          
          <table class="table table-hover" id="dataTables-example">
      
            <thead>
      
              <tr>
      
                <th style="font-size: 20px;">Question</th>
      
                <th style="font-size: 20px;">Answer Input Type</th>

                <th style="font-size: 20px;">Order</th>

                <th style="font-size: 20px;">Academics</th>

                <th style="font-size: 20px;">PlaceHolder</th>

                <th style="font-size: 20px;">Field</th>

                <th></th>
                
                <th></th>
      
              </tr>
      
            </thead>
      
            <tbody>
        
              @foreach($question_model as $model_result)

              @include('main_layout.modals.steps-modal.stepThreeModal')

              <tr>
      
                <td>{{$model_result->question}}</td>

                <td>{{$model_result->input_type}}</td>

                <td>{{$model_result->order}}</td>
                
                <?php

                $academics = App\academics::where('id', $model_result->academics_id)->get();

                $academics_name = $academics->pluck('academics_name')->get(0);

                $course = App\Courses::where('id', $model_result->course_id)->first();

                ?>

                <td>{{$academics_name}}</td>

                <td>{{$model_result->placeholder}}</td>

                <td>{{$course->course_name}}</td>

                <td><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#edit_step_Three_Modal'.$model_result->id; ?>>Edit</button></td>

                <td><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_step_Three_Modal'.$model_result->id; ?>>Delete</button></td>
                
              </tr>

              @endforeach
      
            </tbody>
      
          </table>
        
        </div>
     
      </div>
    
    </div>

  </div>

</div>

