@extends ('main_layout.layout_uno.index')



@section('create_course_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create Field</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create Fields

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-course" method="post">

	                  		@csrf

	                  		<div class="form-group">

	                          	<label for="academics" style="font-size: 20px;">Academics / Certificate</label>

	                          	<select name="get_academics" class="form-control" id="academics">

	                              	<option selected disabled>Select Academics</option>

	                              	@foreach($academics as $academic)

	                              	<option value="{{$academic->id}}">{{$academic->academics_name}}</option>

	                              	@endforeach
	                              	
	                          	</select>

	                      	</div>

		                    <div class="form-group">

		                      	<label for="courseName" style="font-size: 20px;">Field Name</label>

		                      	<input type="text" class="form-control" id="courseName" name="course_name" placeholder="course name" required>

		                    </div>

		                   	<div class="input-daterange datepicker row align-items-center">
				            
				              	<div class="col">
				            
					                <div class="form-group">

					                	<label for="academicsName" style="font-size: 20px;">Certificate Admission Start Date</label>
					            
					                  	<div class="input-group">
					            
					                    	<div class="input-group-prepend">
					            
					                      		<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
					            
					                    	</div>
					            
					                    	<input class="form-control" placeholder="Start date" type="text" name="start_date">
					            
					                  	</div>
					            
					                </div>
				            
				              	</div>
				            
				              	<div class="col">
				            
					                <div class="form-group">

					                	<label for="academicsName" style="font-size: 20px;">Certificate Admission End Date</label>
					            
					                  	<div class="input-group">
					            
					                    	<div class="input-group-prepend">
					            
					                      		<span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
					            
					                    	</div>
					            
					                    	<input class="form-control" placeholder="End date" type="text" name="end_date">
					            
					                  	</div>
					            
					                </div>
				            
				              	</div>
				            
				            </div>
		                    

		                    <button type="submit" class="btn btn-gradient-info mr-2">Create</button>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.course.edit_course')

    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>

@endsection