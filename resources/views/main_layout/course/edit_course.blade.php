<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title" style="font-size: 20px;">Created Fields</h4>
    
        <p class="card-description">
    
          Number of Created Fields: <code>{{$course->count()}}</code>
    
        </p>
    
        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

        <div class="table-responsive">

          <table class="table table-hover" id="dataTables-example">
      
            <thead>
      
              <tr>
      
                <th style="font-size: 18px;">Field Name</th>

                <th style="font-size: 18px;">Academics Name</th>

                <th style="font-size: 18px;">Field Admission Start date</th>
                
                <th style="font-size: 18px;">Field Admission End date</th>

                <th></th>

                <th></th>
      
              </tr>
      
            </thead>
      
            <tbody>
      
              @foreach($course as $courses)

              <tr>

                @include('main_layout.modals.courseModal')
      
                <td style="font-size: 15px;">{{ $courses->course_name }}</td>


                <?php

                  $academics = App\academics::where('id', $courses->academics_id)->get();

                ?>

                @foreach($academics as $academic)

                <td style="font-size: 15px;">{{ $academic->academics_name }}</td>

                @endforeach
      
                <td style="font-size: 15px;"><?= Carbon\Carbon::parse($courses->start_date)->format('m/d/Y') ?></td>

                <td style="font-size: 15px;"><?= Carbon\Carbon::parse($courses->end_date)->format('m/d/Y') ?></td>
      
                <td style="font-size: 17px;"><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#edit_course_Modal'.$courses->id; ?>>Edit</button></td>

                <td style="font-size: 17px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_course_Modal'.$courses->id; ?>>Delete</button></td>
                
              </tr>

              @endforeach
      
            </tbody>
      
          </table>

        </div>
    
      </div>
      
    </div>

  </div>

</div>