@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      	<div class="card-body">
		    
		    		    
		        	<h4 class="card-title" style="font-size: 20px;">{{$university->university_name}}</h4>

	      			<div class="dropdown-divider mb-4" style="width:25.5%;"></div>

	      			

		          	<?php 

		            if($step_quantity->count() > 0)

		            {

		              $step_quantity_plucked = $step_quantity->pluck('step_quantity')->get(0);

		          	?>

		          	@if($step_quantity_plucked == 1)

			        <a href="/institution/{{$id}}/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>

			        @endif

			        @if($step_quantity_plucked == 2)

			        <a href="/institution/{{$id}}/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/institution/{{$id}}/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 

			        @endif

			        @if($step_quantity_plucked == 3) 

			        <a href="/institution/{{$id}}/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/institution/{{$id}}/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 
			        
			        <a href="/institution/{{$id}}/step-three/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-three</button></a> 

			        @endif

			        @if($step_quantity_plucked == 4)

			        <a href="/institution/{{$id}}/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/institution/{{$id}}/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 
			        
			        <a href="/institution/{{$id}}/step-three/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-three</button></a> 

			        <a href="/institution/{{$id}}/step-four/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-four</button></a> 

			        @endif

			        <?php } ?> 

			        <div class="row">

			        <div class="col-lg-12 mt-5">

			        <?php

	                $award = App\award_status::where('user_id', auth()->user()->id)->where('university_id', $university->id)->first();

	                ?>

	                @if(!is_null($award))

		                @if($award->award == 'accepted')

		          			<h2>Congratulations</h2>
			          
			                <div class="dropdown-divider mb-4" style="margin-right: 40%;"></div>

		          			<p class="lead">You have been accepted to the program you have applied for</p>

	          			@elseif($award->award == 'rejected')

		          			<h2>SORRY!</h2>
			          
			                <div class="dropdown-divider mb-4" style="margin-right: 40%;"></div>

		          			<p class="lead">You have been rejected for the program you have applied for</p>

	          			@endif

	                @endif

	            	</div>
	            	
	            	</div>

			        				        
			        <div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
			      	
		     	</div>


		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection