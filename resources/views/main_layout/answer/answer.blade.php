@extends ('main_layout.layout_uno.index')



@section('students_content')

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  	<div class="col-lg-12 grid-margin stretch-card">
		    
			    <div class="card">
			    
			      	<div class="card-body">

			      		<h4 class="card-title" style="font-size: 20px;">Choose University</h4>

			      		<div class="dropdown-divider mb-4" style="width:25.5%;"></div>
			    
			        	<div class="row">

			        		@foreach($university_array as $university)

			        		<div class="col-md-3">

			        			<a href="/institution/{{$university->id}}" class="inverse-btn-link"><button type="button" class="btn btn-gradient-dark btn-fw">{{$university->university_name}}</button></a> 

			        		</div>

			        		@endforeach

				        </div>
				        				        
				        <div class="text-center">

		            		@include('index-page.success_nd_error_page.success')

							@include('index-page.success_nd_error_page.error_')

						</div>
				      	
			     	</div>

			    </div>

		  	</div>

		</div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection