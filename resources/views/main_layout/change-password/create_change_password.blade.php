@extends ('main_layout.layout_uno.index')



@section('change-p_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Change Password</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Change Password

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/change-password" method="post">

	                  		@csrf

		                    <div class="form-group">

		                      	<label for="oldpassword" style="font-size: 20px;">Current Password</label>

		                      	<input type="password" class="form-control" id="oldpassword" name="current_password" placeholder="Current Password" required>

		                    </div>

							<div class="form-group">

		                      	<label for="newpassword" style="font-size: 20px;">New Password</label>

		                      	<input type="password" class="form-control" id="newpassword" name="password" placeholder="New Password" required>

		                    </div>  

		                    <div class="form-group">

		                      	<label for="password_confirmation" style="font-size: 20px;">Confirm New Password</label>

		                      	<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="New Password" required>

		                    </div>                  

		                    

		                    <button type="submit" class="btn btn-gradient-info mr-2" name="">Change Password</button>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection