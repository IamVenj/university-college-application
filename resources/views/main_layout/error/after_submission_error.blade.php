@extends ('main_layout.layout_uno.index')



@section('create_course_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row flex-grow">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body ">

			          	<div class="col-lg-12 mx-auto">

		                  	<div class="row align-items-center d-flex flex-row">
				          
				              <div class="col-lg-12 error-page-divider text-center pl-lg-4">

				              	@if(!is_null(auth()->user()))
				          
				                <?php

				                $award = App\award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->first();

				                ?>

				                @if(!is_null($award))

				                <h2>SORRY!</h2>
				          
				                <div class="dropdown-divider mb-4" style="margin-left: 40%; margin-right: 40%;"></div>

	                  			<p class="lead">You cannot access other pages except (institutions<i class="mdi mdi-school text-success ml-2 mr-2"></i>) once your form is submitted</p>

	                  			<code><i class="fa fa-info-circle mr-2"></i> You can apply to another university by clicking on change university status</code>
	                  			
	                  			@endif

	                  			@endif

	                  			<div class="text-center">

		                    		@include('index-page.success_nd_error_page.success')

									@include('index-page.success_nd_error_page.error_')

								</div>
				          
				              </div>


				          
				            </div>

				        </div>	                  	

	                </div>

                </div>

            </div>

        </div>

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection