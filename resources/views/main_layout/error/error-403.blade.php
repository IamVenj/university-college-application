<!DOCTYPE html>

<html>

<head>


  <!-- Required meta tags -->


  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <title>MoSHE Admission ERROR-403</title>
    

  <link href="../../../font/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  
  <link rel="stylesheet" href="../../../../css/vendor.bundle.base.css">


  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="../../../font/mdi/css/materialdesignicons.min.css">



  <link rel="stylesheet" href="../../../../css/style.css">
  
  
  <link rel="shortcut icon" href="../../../../image/logo-22.png" />


</head>



<body>

<div class="container-scroller">

  <div class="container-fluid page-body-wrapper full-page-wrapper">

    <div class="content-wrapper-unauthorized d-flex align-items-center text-center error-page">

      <div class="row flex-grow">

        <div class="col-lg-7 mx-auto text-white">

          <div class="row align-items-center d-flex flex-row">

            <div class="col-lg-6 text-lg-right pr-lg-4">

              <h1 class="display-1 mb-0">403</h1>

            </div>

            <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">

              <h2>WARNING!</h2>

              <h3 class="font-weight-light">Unauthorized access will be reported to the Super Admin</h3>

            </div>

          </div>

          <div class="row mt-5">

            <div class="col-12 text-center mt-xl-2">

              <?php

              $submission = App\award_status::where('user_id', auth()->user()->id)->count();

              ?>

              @if(is_null(auth()->user()->role_id))
              
                  @if($submission == 0)
                  
                      <a class="text-white font-weight-medium" href="/my-info">Back to home</a>
                  
                  @else
                  
                      <a class="text-white font-weight-medium" href="/cannot-access">Back to home</a>

                  @endif
                  
              @elseif (auth()->user()->role_id == '3') 
              
                  <a class="text-white font-weight-medium" href="/students">Back to home</a>
              
              @else
              
                  <a class="text-white font-weight-medium" href="/dashboard">Back to home</a>

              @endif

            </div>

          </div>

          <div class="row mt-5">

            <div class="col-12 mt-xl-2">

              <p class="text-white font-weight-medium text-center">Copyright &copy; 2018  All rights reserved.</p>

            </div>

          </div>

        </div>

      </div>

    </div>

    <!-- content-wrapper ends -->

  </div>

  <!-- page-body-wrapper ends -->

</div>

</body>



</html>