<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>MoSHE Admission - Complete Profile</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" href="../../../../image/logo-22.png" />
    <link href="../../../css/bootstrap.min.css" rel="stylesheet"/>
	<link href="../../../css/gsdk-bootstrap-wizard.css" rel="stylesheet"/>
	<link href="../../../css/demo.css" rel="stylesheet" />
</head>
<body>
	<div class="image-container set-full-height bg-gradient-info">
		@include('main_layout.first_Time_Login.header')
		    <div class="container">
		    	@include('main_layout.first_Time_Login.body')
		    </div>
	</div>
</body>
@include('main_layout.first_Time_Login.footer')
</html>
