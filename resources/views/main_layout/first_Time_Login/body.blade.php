<div class="row">
  
  <div class="col-sm-12 col-sm-offset-2 mt-2">

    <!--      Wizard container        -->

      <div class="wizard-container">
        
        <div class="card wizard-card" data-color="blue" id="wizardProfile"  style="border-radius:25px;">

          <form action="/info/{{auth()->user()->id}}" method="post">

            @csrf

            @method('PATCH')

          	<div class="wizard-header">

              	<h3>

              	   BUILD YOUR PROFILE 

              	</h3>

            </div>



						<div class="wizard-navigation">

							<ul>

                  <li style="text-align:center; padding:7px;"><a class="wiz-nav-link" href="#about" data-toggle="tab">About</a></li>

                  @if(empty(auth()->user()->role_id))

                  <li style="text-align:center; padding:7px;"><a class="wiz-nav-link" href="#education" data-toggle="tab">Education</a></li>

                  @endif

                  <li style="text-align:center; padding:7px;"><a class="wiz-nav-link" href="#address" data-toggle="tab">Address</a></li>

              </ul>

						</div>



            <div class="tab-content">
            
                <div class="tab-pane" id="about">
            
                    <div class="row ml-3 mr-3">                          

                      <div class="col-sm-12">

                            <div class="row">
                                      
                                <div class="col-sm-4">
                                      
                                    <div class="form-group">
                              
                                        <label>First Name <small>(required)</small></label>
                              
                                        <input name="firstname" type="text" required class="form-control" placeholder="First Name" value={{ auth()->user()->firstname }} >
                              
                                    </div>
                                      
                                </div>
                                      
                                
                                <div class="col-sm-4">
                              
                                    <div class="form-group">
                              
                                        <label>Middle Name <small>(required)</small></label>
                              
                                        <input name="middlename" type="text" required class="form-control" placeholder="Middle Name" value={{ auth()->user()->middlename }} >
                              
                                    </div>
                              
                                </div>


                                <div class="col-sm-4">

                                    <div class="form-group">
                                
                                        <label>SurName <small>(required)</small></label>
                                  
                                        <input name="lastname" type="text" required class="form-control" placeholder="Last Name" value={{ auth()->user()->lastname }} >
                                
                                    </div>

                                </div>

                                      
                            </div>


                          
                            <div class="row">

                          
                                <div class="col-sm-4">


                      
                                        <div class="form-group">

                                            <label>Phone Number <small>(required)</small></label>

                                            <div class="row">

                                              <div class="col-md-5">

                                                <select name="country_code" class="form-control" id="exampleFormControlSelect3">
                                  
                                                    <option selected disabled>Choose Country Code</option>
                                  
                                                    <option value="+251" <?php //if($row['title']=='Ato'){?> selected <?php //}?> >+251</option>
                                  
                                                </select>

                                              </div>

                                              <div class="col-md-7">

                                                <input name="phone_number" required type="text" class="form-control" placeholder="phone number" value= {{ auth()->user()->phone_number }} >

                                              </div>

                                            </div>
                                    
                                        </div>
                                    
                                        
                                </div>
                                        

                                <div class="col-sm-4">
                                
                                    <div class="form-group">
                                  
                                        <label>Gender <small>(required)</small></label>
                                
                                        <select name="sex" class="form-control" id="exampleFormControlSelect3">
                        

                                            <option selected disabled>Choose Sex</option>
                        
                                            <option value="M" <?php if(auth()->user()->sex=='M'){?> selected <?php }?> >Male</option>
                        
                                            <option Value="F" <?php if(auth()->user()->sex=='F'){?> selected <?php }?> >Female</option>
							                        
                                        </select>
                                        
                                    </div>
                                        
                                </div>


                                <div class="col-sm-4 col-sm-offset-1">
                      
                                    <div class="form-group">
                                
                                        <label>Email</label>
                                
                                        <input name="email" type="email" required class="form-control" placeholder="me@example.com" value= {{ auth()->user()->email }} >
                                
                                    </div>
                                
                                </div>

                                
                            </div>

                            <div class="row">
                              

                              <div class="col-md-4">
                       
                                <label class="col-form-label">Date of Birth</label>
                               
                                <div class="form-group">
                                                              
                                  <input name="date_of_birth" class="form-control datepicker" placeholder="Select date" type="text" value="{{ $date }}" readonly>
                                                             
                                </div>
                              
                              </div>

                              @if(is_null(auth()->user()->role_id))

                              <div class="col-md-8">
                       
                                <label class="col-form-label">Certificate</label>
                               
                                <div class="form-group">
                                                              
                                  <select class="form-control" name="certificate_to_see_if_chosen">

                                    <option selected disabled>Choose Degree</option>
                                    
                                    <option value="Associate">Associate's(AA, AS)</option>
                                    
                                    <option value="Bachelor">Bachelor's(BA, BS)</option>
                                    
                                    <option value="Master">Master's(MA, MS)</option>
                                    
                                    <option value="Buisness">Buisness(MBA, MAcc)</option>
                                    
                                    <option value="Law">Law(JD, LLM)</option>
                                    
                                    <option value="Medicine">Medicine(MD, DO, DVM, DOS)</option>
                                    
                                    <option value="Doctorate">Doctorate(PhD, EdD, etc)</option>

                                    <option value="Undecided">Undecided</option>

                                  </select>
                                                             
                                </div>
                              
                              </div>

                              @endif

                            </div>

                            <div class="text-center mb-3">

                            @include('index-page.success_nd_error_page.success')

                            @include('index-page.success_nd_error_page.error_')

                          </div>
                                                                      
                      </div>
                              
                    </div>
                  
                  </div>
                            
                  <div class="tab-pane" id="education">
                                              
                    <div class="row">

                      <div class="col-sm-6">

                        <div class="form-group">

                          <label for="university_id">Add University<i class="fa fa-plus ml-2"></i></label>

                          <select name="university" class="form-control dynamic" id="university_id" data-dependent="academics_id" data-academics="academics_name" data-dbtable="academics">

                              <option selected disabled>Select University</option>

                              @foreach($universities as $university)

                              <option value="{{ $university->id }}">{{ $university->university_name }}</option>

                              @endforeach

                          </select>

                        </div>

                      </div>

                      <div class="col-sm-6">

                        <div class="form-group">

                          <label for="academics">Applying For:</label>

                          <select name="academics_id" class="form-control dynamic" id="academics_id" data-dependent="course_id" data-course="course_name" data-dbtable="courses">

                          </select>

                        </div>

                      </div>

                      <div class="col-sm-12">

                        <div class="form-group">

                          <label for="course">Course:</label>

                          <select name="course_id" class="form-control dynamic" id="course_id" data-courses="course_name_" data-dependent="admission_date">

                          </select>

                        </div>

                      </div>


                          
                    </div>

                    <div class="row">

                      <div class="col-sm-6">
                        
                        <div id="admission_date"></div>

                      </div>

                    </div>

                  </div>

                  

                  <div class="tab-pane" id="address">

                      <div class="row ml-2">

                          <div class="col-sm-6 col-sm-offset-1">

                            <div class="form-group">

                                <label>Address Line (Street Name and Numbers)</label><br>

                                <input class="form-control" placeholder="Address Line" type="text" name="address_line" value="{{ auth()->user()->address_line }}" required>

									          </div>

                          </div>


                          <div class="col-sm-6 col-sm-offset-1">

                            <div class="form-group">

                                <label>City</label>

                                <select name="city" class="form-control">

                                    <option selected disabled>Choose City</option>

                                    <option value="Addis Ababa" <?php if(auth()->user()->city == 'Addis Ababa'){ ?> selected <?php }?>> Addis Ababa </option>

                                    <option value="DireDawa" <?php if(auth()->user()->city == 'DireDawa'){ ?> selected <?php }?>> DireDawa </option>

                                    <option value="ArbaMinch" <?php if(auth()->user()->city == 'ArbaMinch'){ ?> selected <?php }?>> ArbaMinch </option>

                                    <option value="Mekele" <?php if(auth()->user()->city == 'Mekele'){ ?> selected <?php }?>> Mekele </option>

                                    <option value="BahirDar" <?php if(auth()->user()->city == 'BahirDar'){ ?> selected <?php }?>> BahirDar </option>

                                    <option value="Gonder" <?php if(auth()->user()->city == 'Gonder'){?> selected <?php }?>> Gonder </option>

                                    <option value="Jimma" <?php if(auth()->user()->city == 'Jimma'){ ?> selected <?php }?>> Jimma </option>

                                    <option value="DebreMarkos" <?php if(auth()->user()->city == 'DebreMarkos'){ ?> selected <?php }?>> DebreMarkos </option>

                                </select>

                            </div>

                          </div>

                      </div>

                      <div class="row ml-2">

                          <div class="col-sm-4 col-sm-offset-1">

                            <div class="form-group">

                                <label>Sub City</label><br>

                                <input name="subcity" type="text" class="form-control" placeholder="Sub City" required value="{{ auth()->user()->subcity }}" required>

                            </div>

                          </div>


                          <div class="col-sm-4 col-sm-offset-1">

                            <div class="form-group">

                                <label>House No.</label>

                                <input name="house_number" type="text" required class="form-control" placeholder="House No." value= "{{ auth()->user()->house_number }}" required>

                            </div>

                          </div>

                          <div class="col-sm-4 col-sm-offset-1">

                            <div class="form-group">

                                <label>Woreda</label>

                                <input name="woreda" type="text" required class="form-control" placeholder="Woreda" value="{{ auth()->user()->woreda }}" required>

                            </div>

                          </div>

                      </div>

                  </div>

						</div>

            <div class="wizard-footer height-wizard" style="margin-top:-40px;">

                <div class="row ml-2 mt-0">


                    <div class="pull-right">

                        <button type='button' style="border-radius:25px;" class='btn btn-next btn-info btn-wd btn-sm mr-2 ml-3 mb-4' name='next'>Next</button>

                        <button type='submit' style="border-radius:25px;" class='btn btn-finish btn-success btn-wd btn-sm ml-3 mb-4' name='finish'/>Finish</button>

                    </div>



                    <div class="pull-right">

                        <button type='button' style="border-radius:25px;" class='btn btn-previous btn-default btn-wd btn-sm ml-2' name='previous'/>Previous</button>

                    </div>


                </div>

                <div class="clearfix"></div>

            </div>

          </form>

        </div>

      </div> <!-- wizard container -->

  </div>

</div><!-- end row -->