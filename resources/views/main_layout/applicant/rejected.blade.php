@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">Rejected Applicants</h4>
		    
		        
		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

			        <div class="table-responsive">

				        <table class="table table-hover" id="dataTables-example">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;">First Name</th>
				              
				              <th style="font-size: 16px;">Middle Name</th>
				              
				              <th style="font-size: 16px;">Last Name</th>
				              
				              <th style="font-size: 16px;">Email</th>
				              
				              <th style="font-size: 16px;">Phone Number</th>
				              
				              <th style="font-size: 16px;">Gender</th>
				              
				              <th style="font-size: 16px;">Address Line</th>
				              
				              <th style="font-size: 16px;">Cummulative GPA</th>
				              
				              <th style="font-size: 16px;">National Examination Result</th>

				              <th style="font-size: 16px;">Applying For</th>
				              
				              <th style="font-size: 16px;">Field</th>
				    
				              <th></th>				              
				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@foreach($users as $user)

							<?php

							$award_status = new App\award_status();

							$submission = $award_status->get_users($user->user_id, $university_id, 'rejected');

							?>

							@foreach($submission as $submit)

							<?php

							$panel = App\User::where('id', $submit->user_id)->get();

							?>


				          	@if($panel->count() > 0)
				    
				            @foreach($panel as $user)

				            <tr>

				    
				              <td style="font-size: 15px;">{{ $user->firstname }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->middlename }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->lastname }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->email }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->country_code.''.$user->phone_number }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->sex }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->address_line }}</td>

				              <?php

				              $education = App\education::where('user_id', $user->id)->first();

				              ?>
				              
				              <td style="font-size: 15px;">{{ $education->cgpa }}</td>
				              
				              <td style="font-size: 15px;">{{ $education->national_exam_result }}</td>

				              <?php

				              $academics = App\academics::find($education->academics_id);

				              $course = App\Courses::find($education->course_id);

				              ?>
				              
				              <td style="font-size: 15px;">{{ $academics->academics_name }}</td>
				              
				              <td style="font-size: 15px;">{{ $course->course_name }}</td>
				              
				              <td style="font-size: 15px;">
				           
				              	<a href="/students/unreject/{{$user->id}}" method="post">
				           				              	
				              		<button class="btn btn-gradient-danger" type="button">Unreject</button>
				              	
				              	</a>
				          	
				          	  </td>
				    
				             
				            </tr>

				            @endforeach

				            @endif

				            @endforeach

				            @endforeach
				    
				          </tbody>
				    
				        </table>

				        <div class="text-center">

		            		@include('index-page.success_nd_error_page.success')

							@include('index-page.success_nd_error_page.error_')

						</div>
			    
			      	</div>
		      
		      	</div>

		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true

      	});

    });

</script>

@endsection