<div class="modal fade" id=<?= 'delete_course_Modal'.$courses->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Delete <i class="fa fa-trash ml-2"></i></h4>

            </div>

            <form class="login-form" action="/create-course/{{ $courses->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $courses->course_name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-gradient-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_course_Modal'.$courses->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Edit<i class="mdi mdi-pen ml-2"></i></h4>

            </div>

            <form class="login-form" action="/create-course/{{ $courses->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="academics" style="font-size: 20px;">Academics / Certificate</label>

                        <select name="academics_update" class="form-control" id="academics">

                            <option selected disabled>Select Academics</option>

                            @foreach($academics as $academic)

                            <option value="{{$academic->id}}" <?php if($academic->id == $courses->academics_id){ ?> selected <?php } ?>>{{$academic->academics_name}}</option>

                            @endforeach
                            
                        </select>

                    </div>

                    <div class="form-group">

                        <label for="courseName" style="font-size: 20px;">Field Name</label>

                        <input type="text" class="form-control" id="courseName" name="course_name_update" value="{{ $courses->course_name }}" placeholder="course name" required>

                    </div>

                    <div class="input-daterange datepicker">
                                                
                        <div class="form-group">

                            <label for="academicsName" style="font-size: 20px;">Field Admission Start Date</label>
                    
                            <div class="input-group">
                    
                                <div class="input-group-prepend">
                    
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                    
                                </div>
                    
                                <input class="form-control" placeholder="Start date" type="text" name="start_date_update" value="<?= Carbon\Carbon::parse($courses->start_date)->format('m/d/Y') ?>">
                    
                            </div>
                    
                        </div>

                        <div class="form-group">

                            <label for="academicsName" style="font-size: 20px;">Field Admission End Date</label>
                    
                            <div class="input-group">
                    
                                <div class="input-group-prepend">
                    
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                    
                                </div>
                    
                                <input class="form-control" placeholder="End date" type="text" name="end_date_update" value="<?= Carbon\Carbon::parse($courses->end_date)->format('m/d/Y') ?>">
                    
                            </div>
                    
                        </div>
                                        
                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-gradient-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>