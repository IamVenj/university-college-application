<div class="modal fade" id=<?= 'delete_university_Modal'.$university->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/create-university/{{ $university->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $university->university_name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_university_Modal'.$university->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/create-university/{{ $university->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="universityName" style="font-size: 20px;">Name of University</label>

                        <input type="text" class="form-control" id="universityName" name="university_name" value="{{ $university->university_name }}"  placeholder="Email">

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>