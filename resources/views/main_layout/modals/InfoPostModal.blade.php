<div class="modal fade" id=<?= 'delete_info_Modal'.$post->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/post/{{ $post->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $post->title }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-gradient-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_info_Modal'.$post->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Edit<i class="mdi mdi-pen ml-2"></i></h4>

            </div>

            <form class="login-form" action="/post/{{ $post->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="post_Title" style="font-size: 20px;">Course Name / Admission Notice Title</label>

                        <input type="text" class="form-control" id="post_Title" name="title_update" placeholder="Title" value="{{$post->title}}" required>

                    </div>

                    <div class="form-group">
                                          
                        <label style="font-size: 20px;">Information Type</label>
                
                        <select name="info_type_update" class="form-control">
    
                            <option selected disabled>Choose Type</option>
        
                            <option value="admission_notice" <?php if($post->info_type == 'admission_notice'){ ?>selected<?php } ?>>Admission Notice</option>
        
                            <option Value="available_courses" <?php if($post->info_type == 'available_courses'){ ?>selected<?php } ?>>Available Courses</option>
                                    
                        </select>
                        
                    </div>

                    <div class="form-group">

                        <label for="post_description" style="font-size: 20px;">Description</label>

                        <textarea type="text" class="form-control" id="post_description" rows='5' name="post_description_update" placeholder="Description" required>{{$post->description}}</textarea>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>