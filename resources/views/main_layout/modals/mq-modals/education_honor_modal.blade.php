<div class="modal fade" id=<?= 'edit_honor_Modal'.$this_honor->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/education/honor/{{ $this_honor->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Honor Name</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-google-circles-communities text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="honor_name_update" value="{{ $this_honor->name }}" placeholder="Honor Name" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Grade Level</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-account-circle text-danger"></i>

                                </span>

                            </div>

                            <select class="form-control form-control-lg" name="honor_grade_level_update">

                                <option selected disabled>Choose Grade Level</option>

                                <option value="9" <?php if($this_honor->grade_level == '9'){ ?> selected <?php } ?>>9</option>

                                <option value="10" <?php if($this_honor->grade_level == '10'){ ?>selected<?php } ?>>10</option>

                                <option value="11" <?php if($this_honor->grade_level == '11'){ ?>selected<?php } ?>>11</option>

                                <option value="12" <?php if($this_honor->grade_level == '12'){ ?>selected<?php } ?>>12</option>

                                <option value="Associates" <?php if($this_honor->grade_level == 'Associates'){ ?>selected<?php } ?>>Associates(AA, AS)</option>

                                <option value="Bachelors" <?php if($this_honor->grade_level == 'Bachelors'){ ?>selected<?php } ?>>Bachelors(BA, BS)</option>

                                <option value="Masters" <?php if($this_honor->grade_level == 'Masters'){ ?>selected<?php } ?>>Masters(MA, MS)</option>

                                <option value="Business" <?php if($this_honor->grade_level == 'Business'){ ?>selected<?php } ?>>Business(MBA, MAcc)</option>

                                <option value="Law" <?php if($this_honor->grade_level == 'Law'){ ?>selected<?php } ?>>Law(JD, LLM)</option>

                                <option value="Medicine" <?php if($this_honor->grade_level == 'Medicine'){ ?>selected<?php } ?>>Medicine(MD, DO, DVM, DDS)</option>

                                <option value="Doctorate" <?php if($this_honor->grade_level == 'Doctorate'){ ?>selected<?php } ?>>Doctorate(PhD, EdD)</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Level of Recognition</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-account-circle text-danger"></i>

                                </span>

                            </div>

                            <select class="form-control form-control-lg" name="honor_level_of_recognition_update">

                                <option selected disabled>Choose Level of Recognition</option>

                                <option value="School" <?php if($this_honor->level_of_recognition == 'School'){ ?>selected<?php } ?>>School</option>

                                <option value="State/Regional" <?php if($this_honor->level_of_recognition == 'State/Regional'){ ?>selected<?php } ?>>State/Regional</option>

                                <option value="National" <?php if($this_honor->level_of_recognition == 'National'){ ?>selected<?php } ?>>National</option>

                                <option value="International" <?php if($this_honor->level_of_recognition == 'International'){ ?>selected<?php } ?>>International</option>

                            </select>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_honor_Modal'.$this_honor->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/education/honor/{{ $this_honor->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $this_honor->name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>