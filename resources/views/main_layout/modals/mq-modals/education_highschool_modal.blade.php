<div class="modal fade" id=<?= 'edit_highschool_Modal'.$hs->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/education/highschool/{{ $hs->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Highschools' Name</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-school text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="name_of_highschool_update" value="{{ $hs->name }}" placeholder="Name of Your HighSchool" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Highschools' Email</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-email text-warning"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="highschool_email_update" value="{{ $hs->email }}" placeholder="Email" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Highschools' Address</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-map-marker text-danger"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="highschool_address_update" value="{{ $hs->address }}" placeholder="Email" required/>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_highschool_Modal'.$hs->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/education/highschool/{{ $hs->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $hs->name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>
