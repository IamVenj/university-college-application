<div class="modal fade" id=<?= 'edit_child_Modal'.$children->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/fam/child/{{ $children->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Childs' Fullname</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-school text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="child_name_update" value="{{ $children->fullname }}" placeholder="Childs' Fullname" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Childs' Email</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-email text-warning"></i>

                                </span>

                            </div>

                            <input type="email" class="form-control form-control-sm" name="child_email_update" value="{{ $children->email }}" placeholder="Email"/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Childs' Phone Number</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-map-marker text-danger"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="child_phone_update" value="{{ $children->phone_number }}" placeholder="Phone Number"/>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_child_Modal'.$children->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/fam/child/{{ $children->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $children->fullname }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>
