<div class="modal fade" id=<?= 'edit_activity_Modal'.$activity->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/activities/{{ $activity->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Activity Name</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-school text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="activity_name_update" value="{{ $activity->activity_name }}" placeholder="Activity Name" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Please Describe This Activity</label>

                        <textarea name="activity_description_update" rows="8" class="form-control" required>{{ $activity->description }}</textarea>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Hours Spent Per Week</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-map-marker text-danger"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="activity_hour_update" value="{{ $activity->hours_spent_per_week }}" placeholder="Hours Spent Per Week" required/>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_activity_Modal'.$activity->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/activities/{{ $activity->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $activity->activity_name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>
