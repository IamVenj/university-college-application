<div class="modal fade" id=<?= 'edit_sibling_Modal'.$sibling->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/fam/sibling/{{ $sibling->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Siblings' Fullname</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-account text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="sibling_name_update" value="{{ $sibling->fullname }}" placeholder="Siblings' Fullname" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Siblings' Email</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-email text-warning"></i>

                                </span>

                            </div>

                            <input type="email" class="form-control form-control-sm" name="sibling_email_update" value="{{ $sibling->email }}" placeholder="Email"/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Siblings' Phone Number</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-phone text-danger"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="sibling_phone_update" value="{{ $sibling->phone_number }}" placeholder="Phone Number"/>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_sibling_Modal'.$sibling->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/fam/sibling/{{ $sibling->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $sibling->fullname }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>
