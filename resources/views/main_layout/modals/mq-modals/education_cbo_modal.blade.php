<div class="modal fade" id=<?= 'edit_cbo_Modal'.$_cbo_->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/education/cbo/{{ $_cbo_->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label class="col-form-label">Organizations' Name</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-google-circles-communities text-success"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="cbo_name_update" value="{{ $_cbo_->cbo_name }}" placeholder="Organizations' Name" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Counselor/Mentor Prefix</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-account-circle text-danger"></i>

                                </span>

                            </div>

                            <select class="form-control form-control-lg" name="counselor_prefix_update">

                                <option selected disabled>Choose Prefix</option>

                                <option value="Mr" <?php if($_cbo_->counselor_prefix == 'Mr'){ ?> selected <?php } ?>>Mr</option>

                                <option value="Dr" <?php if($_cbo_->counselor_prefix == 'Dr'){ ?> selected <?php } ?> >Dr</option>

                                <option value="Professor" <?php if($_cbo_->counselor_prefix == 'Professor'){ ?> selected <?php } ?>>Professor</option>

                            </select>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Counselors' Fullname</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-account text-danger"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="counselor_fullname_update" value="{{ $_cbo_->fullname }}" placeholder="Counselors' Fullname" required/>

                        </div>

                    </div>

                    <div class="form-group">

                        <label class="col-form-label">Counselors' Email</label>

                        <div class="input-group">

                            <div class="input-group-prepend">

                                <span class="input-group-text">

                                    <i class="mdi mdi-email text-warning"></i>

                                </span>

                            </div>

                            <input type="text" class="form-control form-control-sm" name="counselor_email_update" value="{{ $_cbo_->email }}" placeholder="Counselors' Email" required/>

                        </div>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'delete_cbo_Modal'.$_cbo_->id; ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/education/cbo/{{ $_cbo_->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $_cbo_->cbo_name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            </form>

        </div>

    </div>

</div>