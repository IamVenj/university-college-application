<div class="modal fade" id=<?= 'delete_step_one_Modal'.$model_result->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Delete <i class="fa fa-trash ml-2"></i></h4>

            </div>

            <form class="login-form" action="/create-step1/{{ $model_result->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_step_one_Modal'.$model_result->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Edit <i class="fa fa-edit ml-2"></i></h4>

            </div>

            <form class="login-form" action="/create-step1/{{ $model_result->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="question" style="font-size: 20px;">Question<i class=" mdi mdi-comment-question-outline ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

                        <input type="text" class="form-control" id="question" name="edit_question" value="{{$model_result->question}}" placeholder="Question" required>

                    </div>

                    <div class="form-group">

                        <label for="question_placeholder" style="font-size: 20px;">Questions Placeholder<i class="mdi mdi-image-filter-center-focus-weak ml-2" style="color:rgba(0,0,0,0.4);"></i></label>

                        <input type="text" class="form-control" id="question_placeholder" name="question_placeholder" value="{{$model_result->placeholder}}" placeholder="Placeholder" required>

                    </div>


                    <div class="form-group">

                        <label for="order_of_Q" style="font-size: 20px;">Order of The Question<i class=" mdi mdi-arrange-send-to-back ml-2"  style="color:rgba(0,0,0,0.4);"></i></label>

                        <select name="order" class="form-control" id="order_of_Q">

                            <option selected disabled>Select Order</option>

                            <option value="1" <?php if($model_result->order == 1){ ?> selected <?php } ?>>1</option>
                            
                            <option value="2" <?php if($model_result->order == 2){ ?> selected <?php } ?>>2</option>

                            <option value="3" <?php if($model_result->order == 3){ ?> selected <?php } ?>>3</option>
                            
                            <option value="4" <?php if($model_result->order == 4){ ?> selected <?php } ?>>4</option>

                            <option value="5" <?php if($model_result->order == 5){ ?> selected <?php } ?>>5</option>
                            
                            <option value="6" <?php if($model_result->order == 6){ ?> selected <?php } ?>>6</option>

                            <option value="7" <?php if($model_result->order == 7){ ?> selected <?php } ?>>7</option>
                            
                            <option value="8" <?php if($model_result->order == 8){ ?> selected <?php } ?>>8</option>

                            <option value="9" <?php if($model_result->order == 9){ ?> selected <?php } ?>>9</option>
                            
                            <option value="10" <?php if($model_result->order == 10){ ?> selected <?php } ?>>10</option>

                        </select>

                    </div>

                    

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>