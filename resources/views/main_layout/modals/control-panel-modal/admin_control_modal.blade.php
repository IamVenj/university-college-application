<div class="modal fade" id=<?= 'delete_controlled_admin_Modal'.$user->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Delete <i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/admin-panel/{{ $user->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to delete {{ $user->email }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>


<div class="modal fade" id=<?= 'deactivate_controlled_admin_Modal'.$user->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Deactivate<i class="fa fa-user ml-2"></i></h4>

            </div>

            <form class="login-form" action="/admin-panel/deactivate/{{ $user->id }}" method="post">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <p class="text-center lead">Are you sure you want to deactivate {{ $user->email }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Deactivate</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'activate_controlled_admin_Modal'.$user->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Activate <i class="fa fa-user ml-2"></i></h4>

            </div>

            <form class="login-form" action="/admin-panel/activate/{{ $user->id }}" method="post">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <p class="text-center lead">Do you want to Activate {{ $user->email }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">Activate</button>

                </div>

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'reset_controlled_admin_password_Modal'.$user->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Reset <i class="mdi mdi-key-remove ml-2"></i></h4>

            </div>

            <form class="login-form" action="/admin-panel/reset/{{ $user->id }}" method="post">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="password_reset" style="font-size: 20px;">New Password</label>

                        <input type="password" class="form-control" id="password_reset" name="password_reset" placeholder="Write New Password..." required>

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-info" style="width:100%;">Reset Password</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'change_university_of_controlled_admin_Modal'.$user->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Edit <i class="fa fa-edit ml-2"></i></h4>

            </div>

            <form class="login-form" action="/admin-panel/change-university/{{ $user->id }}" method="post">

                @csrf

                @method('PATCH')

                <div class="modal-body">

                    <div class="form-group">

                        <label for="change_university" style="font-size: 20px;">Change University</label>

                        <select name="change_university" class="form-control" id="change_university">

                            <option selected disabled>Choose University</option>

                            @foreach($universities as $university)

                            <?php

                            $university_name__Queryz = App\university_membership::where('user_id', $user->id)->first(); 

                            ?>
        
                            <option value="{{ $university->id }}" <?php if($university->id == $university_name__Queryz->university_id){ ?>selected<?php } ?>>{{ $university->university_name }}</option>

                            @endforeach
                                            
                        </select>


                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-info" style="width:100%;">Change</button>

                </div>

            

            </form>

        </div>

    </div>

</div>