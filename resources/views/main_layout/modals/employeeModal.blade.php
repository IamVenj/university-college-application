<div class="modal fade" id=<?= 'delete_employee_Modal'.$users->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/create-employee/{{ $users->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <input type="hidden" name="delete_employee_id" value={{ $users->id }}>

                    <p class="text-center lead">Are you sure you want to delete {{ $users->email }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-gradient-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_employee_Modal'.$users->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;">Edit<i class="mdi mdi-pen ml-2"></i></h4>

            </div>

            <form class="login-form" action="/create-employee/{{ $users->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="universityAdminEmail" style="font-size: 20px;">Email</label>

                        <input type="text" class="form-control" id="universityAdminEmail" name="email" value='{{$users->email}}' placeholder="Email">

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>