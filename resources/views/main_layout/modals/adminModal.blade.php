<div class="modal fade" id=<?= 'delete_academics_Modal'.$academic->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-trash"></i></h4>

            </div>

            <form class="login-form" action="/create-academics/{{ $academic->id }}" method="post">

                @csrf

                @method('DELETE')

                <div class="modal-body">

                    <input type="hidden" name="delete_academics_id" value={{ $academic->id }}>

                    <p class="text-center lead">Are you sure you want to delete {{ $academic->academics_name }} ?</p>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>

                </div>

            

            </form>

        </div>

    </div>

</div>

<div class="modal fade" id=<?= 'edit_academics_Modal'.$academic->id;?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header justify-content-center">

                <h4 class="title title-up" style="margin-bottom:20px;"><i class="fa fa-edit"></i></h4>

            </div>

            <form class="login-form" action="/create-academics/{{ $academic->id }}" method="post">

                @method('PATCH')

                @csrf

                <div class="modal-body">

                    <div class="form-group">

                        <label for="academicsName" style="font-size: 20px;">Academics Name</label>

                        <input type="text" class="form-control" id="academicsName" value="{{$academic->academics_name}}" name="academics_name" placeholder="academics name">

                    </div>

                </div>

                <div class="modal-footer" style="margin-top:30px;">

                    <button type="submit" class="btn btn-success" style="width:100%;">save</button>

                </div>

            

            </form>

        </div>

    </div>

</div>