@extends ('main_layout.layout_uno.index')



@section('admin_control_panel_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      	<div class="card-body">
		    
			        <h4 class="card-title" style="font-size: 20px;">Admin Control Panel</h4>
			    
			        <p class="card-description">
			    
			          Number of Registered University Admins: <code>@if($users!=null) {{ $users->count() }} @else 0 @endif</code>
			    
			        </p>
		    
		        	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>

			        <div class="table-responsive">

				        <table class="table table-hover" id="dataTables-example">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;">First Name</th>
				              
				              <th style="font-size: 16px;">Middle Name</th>
				              
				              <th style="font-size: 16px;">Last Name</th>
				              
				              <th style="font-size: 16px;">Email</th>
				              
				              <th style="font-size: 16px;">Phone Number</th>
				              
				              <th style="font-size: 16px;">Gender</th>
				              
				              <th style="font-size: 16px;">Address Line</th>
				             
				              <th style="font-size: 16px;">City</th>
				              
				              <th style="font-size: 16px;">SubCity</th>
				              
				              <th style="font-size: 16px;">House Number</th>
				              
				              <th style="font-size: 16px;">Woreda</th>

				              <th style="font-size: 16px;">University</th>
				    
				              <th></th>

				              <th></th>

				              <th></th>
				              
				              <th></th>
				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($users!=null)
				    
				            @foreach($users as $user)

				            @include('main_layout.modals.control-panel-modal.admin_control_modal')

				            <tr>

				    
				              <td style="font-size: 13px;">{{ $user->firstname }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->middlename }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->lastname }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->email }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->phone_number }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->sex }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->address_line }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->city }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->subcity }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->house_number }}</td>
				              
				              <td style="font-size: 13px;">{{ $user->woreda }}</td>

				              <?php

				              	$university_name__Queryz = App\university_membership::where('user_id', $user->id)->get(); 

				              	foreach ($university_name__Queryz as $university_name__Query) {

				              		$university_namez = App\universities::where('id', $university_name__Query->university_id)->get();

				              	}


				              ?>

				              @foreach($university_namez as $university_name)

				              <td style="font-size: 13px;">{{ $university_name->university_name }}</td>

				              @endforeach


				              <td style="font-size: 13px;"><button type="button" class="btn btn-primary"  data-toggle="modal" data-target=<?= '#change_university_of_controlled_admin_Modal'.$user->id; ?>>Change University</button></td>
				    
				              <td style="font-size: 13px;"><button type="button" class="btn btn-info"  data-toggle="modal" data-target=<?= '#reset_controlled_admin_password_Modal'.$user->id; ?>>Reset Password</button></td>

				              <?php if($user->activation_status == 1){ ?>

				              <td style="font-size: 13px;"><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#deactivate_controlled_admin_Modal'.$user->id; ?>>Deactivate</button></td>

				          	  <?php }else{ ?>

		          	  		  <td style="font-size: 13px;"><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#activate_controlled_admin_Modal'.$user->id; ?>>Activate</button></td>

				          	  <?php } ?>

				              <td style="font-size: 13px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_controlled_admin_Modal'.$user->id; ?>>Delete</button></td>
				              
				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				        <div class="text-center">

		            		@include('index-page.success_nd_error_page.success')

							@include('index-page.success_nd_error_page.error_')

						</div>
			    
			      	</div>
		      
		      	</div>

		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>

<script type="text/javascript">
	
    $(document).ready(function() {

  		$('#dataTables-example').DataTable({

        	responsive: true


      	});

    });

</script>

@endsection