@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

		        @foreach($show_question as $sh)

		        <code>{{$sh->order.' . '.$sh->question}}</code>

		        <?php

		        $answer = new App\answer_for_university_questions();

		        $university_membership = new App\university_membership();

		        $university_id = $university_membership->get_university_id_of_this();

		        $answers = $answer->get_answers($user->id, 4, $university_id, $sh->id);

		        ?>

		        @foreach($answers as $answer)

		        <p class="lead ml-4 mt-2">Answer : {{$answer->answer}}</p>

		        @endforeach

		        @endforeach

		        <div class="text-center">

            		@include('index-page.success_nd_error_page.success')

					@include('index-page.success_nd_error_page.error_')

				</div>
	    
	      	</div>
	      

		  </div>

		</div>
		

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection