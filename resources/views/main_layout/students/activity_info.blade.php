@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

		        	<label class="card-description mt-5 mb-1 ml-3" style="font-size:20px;">
			        	
			        	Activities

			        </label>

			        <div class="table-responsive">

				        <div class="dropdown-divider mt-0 ml-3" style="width:15%;"></div>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;"><strong>Activity Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Description</strong></th>

				              <th style="font-size: 16px;"><strong>Hours Spent Per Week</strong></th>
				              				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($activities!=null)

				          	@foreach($activities as $activity)
				    
				            <tr>


					    		<td style="font-size: 15px;">{{ $activity->activity_name }}</td>
				              
				              	<td style="font-size: 15px;">{{ $activity->description }}</td>

				              	<td style="font-size: 15px;">{{ $activity->hours_spent_per_week }}</td>
				            

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

			        <div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
		    
		      	</div>
		      
		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection