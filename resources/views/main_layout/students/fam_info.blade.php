@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

			        

		        	<label class="card-description mt-5 mb-1 ml-3" style="font-size:20px;">
			        	
			        	Family

			        </label>

				    <div class="table-responsive">

				        <div class="dropdown-divider mt-0 ml-3" style="width:15%;"></div>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;"><strong>Currently Living With</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Martial Status</strong></th>

				              <th style="font-size: 16px;"><strong>Do You have Children?</strong></th>

				              @if($fam!=null)

				              @if($fam->children == 'yes')

				              <th style="font-size: 16px;"><strong>Number of Children</strong></th>

				              @endif

				              @endif

				              <th style="font-size: 16px;"><strong>Number of Siblings</strong></th>
				              				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($education!=null && $fam!=null)
				    
				            <tr>

					    		<?php

					    		$certificate = App\academics::find($education->academics_id);

					    		$course = App\Courses::find($education->course_id);

					    		?>

					    		<td style="font-size: 15px;">{{ $fam->currently_living_with }}</td>
				              
				              	<td style="font-size: 15px;">{{ $fam->marital_status }}</td>

				              	<td style="font-size: 15px;">{{ $fam->children }}</td>

				              	@if($fam->children == 'yes')
					              
			            	  	<td style="font-size: 15px;">{{ $fam->number_of_children }}</td>

			            	  	@endif
					              
				              	<td style="font-size: 15px;">{{ $fam->number_of_siblings }}</td>
				            

				            </tr>



				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @if($fam!=null)

				    <div class="table-responsive">

				        @if(!is_null($fam->father_id))

				        <code class="ml-2">Fathers' Info</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Fathers' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  

				              <th style="font-size: 16px;"><strong>Address</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($fam!=null)

				          	<?php

				          	$father = App\father::find($fam->father_id);

				          	?>
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $father->father_first_name.' '.$father->father_middle_name.' '.$father->father_last_name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $father->father_email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $father->father_phone_number }}</td>

				              	<td style="font-size: 15px;">{{ $father->father_address }}</td>
				            
				              	

				            </tr>

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    <div class="table-responsive">

				        @if(!is_null($fam->mother_id))

				        <code class="ml-2">Mothers' Info</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Mothers' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  

				              <th style="font-size: 16px;"><strong>Address</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($fam!=null)

				          	<?php

				          	$mother = App\mother::find($fam->mother_id);

				          	?>
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $mother->mother_first_name.' '.$mother->mother_middle_name.' '.$mother->mother_last_name }}</td>
				              					             
			            	  	<td style="font-size: 15px;">{{ $mother->mother_email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $mother->mother_phone_number }}</td>

				              	<td style="font-size: 15px;">{{ $mother->mother_address }}</td>
				            
				              	

				            </tr>

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    <div class="table-responsive">

				        @if(!is_null($fam->guardian_id))

				        <code class="ml-2">Guardians' Info</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Guardians' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  

				              <th style="font-size: 16px;"><strong>Address</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($fam!=null)

				          	<?php

				          	$guardian = App\guardian::find($fam->guardian_id);

				          	?>
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $guardian->guardian_first_name.' '.$guardian->guardian_middle_name.' '.$guardian->guardian_last_name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $guardian->guardian_email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $guardian->guardian_phone_number }}</td>

				              	<td style="font-size: 15px;">{{ $guardian->guardian_address }}</td>
				            
				              	

				            </tr>

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    <div class="table-responsive">

				        @if(!is_null($fam->other_id))

				        <code class="ml-2">Others' Info</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Others' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  

				              <th style="font-size: 16px;"><strong>Address</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($fam!=null)

				          	<?php

				          	$other = App\other::find($fam->other_id);

				          	?>
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $other->other_first_name.' '.$other->other_middle_name.' '.$other->other_last_name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $other->other_email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $other->other_phone_number }}</td>

				              	<td style="font-size: 15px;">{{ $other->other_address }}</td>
				            
				              	

				            </tr>

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    <div class="table-responsive">

				        @if($fam->children == 'yes')

				        <code class="ml-2">Children</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>FullName</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($children!=null)

				          	@foreach($children as $child)

				            <tr>
				            	

				            	<td style="font-size: 15px;">{{ $child->fullname }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $child->email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $child->phone_number }}</td>
				            

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    <div class="table-responsive">

				        @if($fam->number_of_siblings != 0)

				        <code class="ml-2">Siblings</code>

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>FullName</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>	  
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($siblings!=null)

				          	@foreach($siblings as $sibling)

				            <tr>


				            	<td style="font-size: 15px;">{{ $sibling->fullname }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $sibling->email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $sibling->phone_number }}</td>
				            

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				        @endif

				    </div>

				    @endif

			        <div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
		    
		      	</div>
	      
	      	</div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection