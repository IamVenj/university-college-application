@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

		        	<div class="table-responsive">

			        	<table class="table table-hover mb-4">
					    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Disciplinary Violation</strong></th>

				              <th style="font-size: 16px;"><strong>Conviction</strong></th>
				              				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($writings!=null)
				    
				            <tr>

				              
				              	<td style="font-size: 15px;">{{ $writings->disciplinary_violation }}</td>

				              	<td style="font-size: 15px;">{{ $writings->conviction }}</td>
				            

				            </tr>

				            @endif
				    
				          </tbody>
				    
				        </table>

			        </div>

		        	<label class="card-description mt-3 mb-1 ml-3" style="font-size:20px;">
			        	
			        	Writing

			        </label>

			        <div class="dropdown-divider mt-0 ml-3 mb-3" style="width:15%;"></div>

			        <code class="ml-2 ">Essay</code>

			        @if($writings!=null)

			        <p class="lead ml-3 mb-5 mt-2">{{ $writings->essay }}</p>

			        @endif

			        @if($writings!=null)

			        @if($writings->disciplinary_violation == 'yes')

			        <code class="ml-2 ">Disciplinary Violation Discription</code>

			        <p class="lead ml-3 mb-5 mt-2">{{ $writings->disciplinary_violation_description }}</p>

			        @endif

			        @if($writings->conviction == 'yes')

			        <code class="ml-2 ">Conviction Discription</code>

			        <p class="lead ml-3 mb-5 mt-2 mb-5">{{ $writings->conviction_description }}</p>

			        @endif

			        @endif

			        <div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
		    
		      	</div>
	      
	      	</div>


		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection