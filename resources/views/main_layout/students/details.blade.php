@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<style type="text/css">
	
	.inverse-btn-link:hover{

		color: #e8e8e8;

	}

</style>

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

		        	<div class="table-responsive">

				        <table class="table table-hover mb-5">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;"><strong>First Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Middle Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Last Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Phone Number</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Sex</strong></th>
				              				              
				              <th style="font-size: 16px;"><strong>City</strong></th>

				              <th style="font-size: 16px;"><strong>Adress Line</strong></th>
				              
				              <th style="font-size: 16px;"><strong>SubCity</strong></th>
				              
				              <th style="font-size: 16px;"><strong>House Number</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Woreda</strong></th>
				    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($user!=null)
				    

				            <tr>

				    
				              <td style="font-size: 15px;">{{ $user->firstname }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->middlename }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->lastname }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->email }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->phone_number }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->sex }}</td>

				              <td style="font-size: 15px;">{{ $user->city }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->address_line }}</td>
				             				              
				              <td style="font-size: 15px;">{{ $user->subcity }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->house_number }}</td>
				              
				              <td style="font-size: 15px;">{{ $user->woreda }}</td>

				              
				              
				            </tr>


				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

			        <a href="/students/education-details/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-info btn-fw">Education</button></a>

			        <a href="/students/fam-info/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-primary btn-fw">Family</button></a>

			        <a href="/students/activity/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-danger btn-fw">Activity</button></a>

			        <a href="/students/writing/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-success btn-fw">Writing</button></a>      

		         	<?php 

		            $university_membership = App\university_membership::where('user_id', auth()->user()->id);

		            $university_id = $university_membership->pluck('university_id')->get(0);

		            $step_quantity = App\step_quantities::where('university_id', $university_id);



		          	?>

		          

		          	<?php 

		            if($step_quantity->count() > 0)

		            {

		              $step_quantity_plucked = $step_quantity->pluck('step_quantity')->get(0);

		          	?>

		          	@if($step_quantity_plucked == 1)

			        <a href="/students/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>

			        @endif

			        @if($step_quantity_plucked == 2)

			        <a href="/students/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/students/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 

			        @endif

			        @if($step_quantity_plucked == 3) 

			        <a href="/students/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/students/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 
			        
			        <a href="/students/step-three/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-three</button></a> 

			        @endif

			        @if($step_quantity_plucked == 4)

			        <a href="/students/step-one/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-one</button></a>
			        
			        <a href="/students/step-two/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-two</button></a> 
			        
			        <a href="/students/step-three/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-three</button></a> 

			        <a href="/students/step-four/{{$user->id}}" class="inverse-btn-link ml-2 mr-4" ><button type="button" class="btn btn-gradient-dark btn-fw">Step-four</button></a> 

			        @endif

			        <?php } ?> 

			        

			        <div class="row mt-5">

			        	@foreach($doc as $d)

		      		
			      		<div class="col-md-3 ml-2">

			      			
		        			<form action="/students/doc/{{$d->document}}">


				      			@if($d->document_type == 'h_transcript')

					            <p class="card-description">Highschool Transcript</p>

				              	@elseif($d->document_type == 'n_e_doc')

				             	<p class="card-description">National Examination Document</p>

				             	@elseif($d->document_type == 'degree')

					            <p class="card-description">Degree Certificate</p>

				              	@endif
				      				
				      			<button type="submit" class="btn btn-dark btn-fw w-100">Download</button>

				      		</form>

				      		

			      		</div>

			      		@endforeach
			      		
			      	</div>
			        
			      	

			      	
			        

			        <div class="row mt-5">

			        	<div class="col-md-6">

			        		<form action="/students/accept/{{$user->id}}" method="post">

			        			@csrf
		      							      				
				      			<button type="submit" class="btn btn-success" style="width: 100%; font-size: 20px;">Accept</button>

			      			</form>

			      		</div>

			      		<div class="col-md-6">

			      			<form action="/students/reject/{{$user->id}}" method="post">

			      				@csrf
				      				
				      			<button type="submit" class="btn btn-danger" style="width: 100%; font-size: 20px;">Reject</button>

				      		</form>

				      	</div>

			      	</div>

			        				        
			        <div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
			      	
		     	</div>


		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection