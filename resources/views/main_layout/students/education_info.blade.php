@extends ('main_layout.layout_uno.index')



@section('students_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

		  <div class="col-lg-12 grid-margin stretch-card">
		    
		    <div class="card">
		    
		      <div class="card-body">
		    
		        <h4 class="card-title" style="font-size: 20px;">{{$user->firstname.' '.$user->middlename.' '.$user->lastname}}</h4>
		    		    
		        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

		        <label class="card-description mt-5 mb-1 ml-3" style="font-size:20px;">
		        	
		        	Education

		        </label>

		        <div class="dropdown-divider mt-3 ml-3" style="width:15%;"></div>

			        <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    
				              <th style="font-size: 16px;"><strong>Applying For</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Field</strong></th>

				              <th style="font-size: 16px;"><strong>Have You Obtained A Diploma/Degree Certificate?<code> / previous education</code></strong></th>
				              
				              <th style="font-size: 16px;"><strong>Cummulative GPA</strong></th>
				              
				              <th style="font-size: 16px;"><strong>National Examination Result</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($education!=null)
				    
				            <tr>

					    		<?php

					    		$certificate = App\academics::find($education->academics_id);

					    		$course = App\Courses::find($education->course_id);

					    		?>

					    		<td style="font-size: 15px;">{{ $certificate->academics_name }}</td>
				              
				              	<td style="font-size: 15px;">{{ $course->course_name }}</td>

				              	<td style="font-size: 15px;">{{ $education->previous_education }}</td>
					              
			            	  	<td style="font-size: 15px;">{{ $education->cgpa }}</td>
					              
				              	<td style="font-size: 15px;">{{ $education->national_exam_result }}</td>
				            

				            </tr>



				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @if($previous_education!=null)

			        <code class="ml-2">Previous Education</code>

				    <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    
				              
				              
				              <th style="font-size: 16px;"><strong>University</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Certificate Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Date of Obtaining Certificate</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($previous_education!=null)
				    
				            <tr>

					    		<?php

					    		$university = App\universities::find($previous_education->university_id);

					    		$course = App\Courses::find($education->course_id);

					    		?>

					    		

				            	<td style="font-size: 15px;">{{ $university->university_name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $previous_education->certificate_name }}</td>
					              
				              	<td style="font-size: 15px;">{{ $previous_education->date_obtain }}</td>
				            

				            </tr>



				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @endif

				    @if(!$highschool->isEmpty())

			        <code class="ml-2">Highschool</code>

			        <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Highschools' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Email</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Address</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($highschool!=null)

				          	@foreach($highschool as $hs)
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $hs->name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $hs->email }}</td>
					              
				              	<td style="font-size: 15px;">{{ $hs->address }}</td>
				            
				              	

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @endif

				    @if(!$cbo->isEmpty())				    

			        <code class="ml-2">Community-Based Organization</code>

			        <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Organizations' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Counselors' Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Counselors' Email</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($cbo!=null)

				          	@foreach($cbo as $_cbo_)
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $_cbo_->cbo_name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $_cbo_->counselor_prefix.' '.$_cbo_->fullname }}</td>
					              
				              	<td style="font-size: 15px;">{{ $_cbo_->email }}</td>
				            
				              	

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @endif

				    @if(!$honor->isEmpty())

			        <code class="ml-2">Honors</code>

			        <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Honor Name</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Grade Level</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Level of Recognition</strong></th>	    
				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($honor!=null)

				          	@foreach($honor as $honors)
				    
				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $honors->name }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $honors->grade_level }}</td>
					              
				              	<td style="font-size: 15px;">{{ $honors->level_of_recognition }}</td>
				            
				              	

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @endif

				    @if(!$future_plans->isEmpty())

			        <code class="ml-2">Future Plans</code>

			        <div class="table-responsive">

				        <table class="table table-hover mb-4">
				    
				          <thead>
				    
				            <tr>
				    				              
				              <th style="font-size: 16px;"><strong>Description</strong></th>
				              
				              <th style="font-size: 16px;"><strong>Highest Degree Intended</strong></th>
				              				            				    
				            </tr>
				    
				          </thead>
				    
				          <tbody>

				          	@if($future_plans!=null)

				          	@foreach($future_plans as $future_plan)

				            <tr>

				            	

				            	<td style="font-size: 15px;">{{ $future_plan->description }}</td>
				              					              
			            	  	<td style="font-size: 15px;">{{ $future_plan->highest_degree_intended }}</td>
					              			            
				              	

				            </tr>

				            @endforeach

				            @endif
				    
				          </tbody>
				    
				        </table>

				    </div>

				    @endif

	        		<div class="text-center">

	            		@include('index-page.success_nd_error_page.success')

						@include('index-page.success_nd_error_page.error_')

					</div>
		    
		      	</div>
		      
		    </div>

		  </div>

		</div>


    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection