<div class="row">

  <div class="col-lg-12 grid-margin stretch-card">
    
    <div class="card">
    
      <div class="card-body">
    
        <h4 class="card-title" style="font-size: 20px;">Created Academics</h4>
    
        <p class="card-description">
    
          Number of Created Academics: <code>{{$academics->count()}}</code>
    
        </p>
    
        <div class="dropdown-divider mb-4" style="width:25.5%;"></div>

          <div class="table-responsive">

            <table class="table table-hover">
        
              <thead>
        
                <tr>
        
                  <th style="font-size: 18px;">Academics Name</th>
        
                  <th></th>

                  <th></th>
        
                </tr>
        
              </thead>
        
              <tbody>
        
                @foreach($academics as $academic)

                <tr>

                  @include('main_layout.modals.academicsModal')
        
                  <td style="font-size: 15px;">{{ $academic->academics_name }}</td>
        
                  <td style="font-size: 17px;"><button type="button" class="btn btn-success"  data-toggle="modal" data-target=<?= '#edit_academics_Modal'.$academic->id; ?>>Edit</button></td>

                  <td style="font-size: 17px;"><button type="button" class="btn btn-danger"  data-toggle="modal" data-target=<?= '#delete_academics_Modal'.$academic->id; ?>>Delete</button></td>
                  
                </tr>

                @endforeach
        
              </tbody>
        
            </table>
      
        </div>
      
      </div>

    </div>

  </div>

</div>