@extends ('main_layout.layout_uno.index')



@section('academics_content')

<!-- partial -->

<div class="main-panel">        

    <div class="content-wrapper-index">

		<div class="row">

            <div class="col-md-12 grid-margin stretch-card">

              	<div class="card">

                	<div class="card-body">

	                  	<h4 class="card-title" style="font-size: 20px;">Create Academics</h4>

	                  	<p class="card-description" style="font-size:15px; margin-bottom: 0%;">

	                    	Create Academics(eg. bachelor's degree, masters degree)

	                  	</p>



	                  	<div class="dropdown-divider mb-4" style="width:25.5%;"></div>



	                  	<form class="forms-sample" action="/create-academics" method="post">

	                  		@csrf

		                    <div class="form-group">

		                      	<label for="academicsName" style="font-size: 20px;">Academics Name</label>

		                      	<input type="text" class="form-control" id="academicsName" name="academics_name" placeholder="academics name" required>

		                    </div>

		                    <button type="submit" class="btn btn-gradient-info mr-2" name="">Create</button>

		                    <div class="text-center">

	                    		@include('index-page.success_nd_error_page.success')

								@include('index-page.success_nd_error_page.error_')

							</div>

	                  	</form>

	                </div>

                </div>

            </div>

        </div>

        @include('main_layout.academics.edit_academics')

    </div>

    @include('main_layout.layout_uno.footer')

</div>


@endsection