@extends('welcome')

@section('notice_course_content')


<section class="section section-components mt-5">

    <div class="container">

        <h3 class="h4 text mb-4">Admission Notice and Available Courses</h4>

          	<div class="row justify-content-center">

            	<div class="col-lg-12">

              	<!-- Tabs with icons -->

            	<div class="dropdown-divider mb-2" style="width: 43%;"></div>

	            <div class="mb-2">

	            	
	                <small class="text" style="font-size: 18px;">{{$university->university_name}}</small>


	            </div>

	            <div class="dropdown-divider mb-4" style="width: 43%;"></div>

              	<div class="nav-wrapper">

                	<ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">

                  		<li class="nav-item">

                    		<a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true" style="font-size: 20px;"><i class="fa fa-info-circle mr-2"></i>Admission Notice</a>

                  		</li>

                  		<li class="nav-item">

                    		<a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false" style="font-size: 20px;"><i class="fa fa-bell mr-2"></i>Available Couses</a>

                  		</li>

                  

                	</ul>

              	</div>

              	<div class="card shadow">

                	<div class="card-body">

                		

                  		<div class="tab-content" id="myTabContent">

                  			

                    		<div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">

                    			@foreach($Information_Posts_notice as $Information_Post)

                    			<p class="lead">{{$Information_Post->title}}</p>

                    			<p class="description">{{$Information_Post->description}}</p>

                    			@endforeach

                    		</div>

                    		

                    		<div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">

                          <div class="table-responsive">

                      			<table class="table table-striped">
    
        						          <thead>
        						    
        						            <tr>
        						    
        						              <th>Course Name</th>
        						    
        						              <th>Description</th>
        						    
        						            </tr>
        						    
        						          </thead>
        						    
        						          <tbody>
        						    
        						            @foreach($Information_Posts_course as $Information_Post)

        						            <tr>
        						    
        						              <td>{{ $Information_Post->title }}</td>
        						    
        						              <td>{{ $Information_Post->description }}</td>
        						              
        						            </tr>

        						            @endforeach
        						    
        						          </tbody>
        						    
        						        </table>

                          </div>

                    		</div>

                  		</div>

                	</div>

              	</div>

            </div>

        </div>

    </div>

</section>

@endsection