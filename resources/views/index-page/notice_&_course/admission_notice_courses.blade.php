@extends ('welcome')


@section('notice_course_content')

<section class="section" style="margin-top: 80px;">

  <div class="container">

    <div class="row row-grid align-items-center">

      @foreach($universities as $university)

      <div class="col-md-6">

        

        <div class="card shadow shadow-lg--hover mt-5">

          <div class="card-body">

              <div class="pl-4">

              	

                <h5 class="title text-success">{{$university->university_name}}</h5>

                <p>Admission Notice and Available Courses</p>

                <a href="/universities/{{$university->id}}" class="text-warning">View More</a>

                

              </div>


          </div>

        </div>

        

    </div>

    @endforeach

  </div>

</section>



@endsection