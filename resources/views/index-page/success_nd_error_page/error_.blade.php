<style type="text/css">
		
	ul{
		list-style-type: none;
	}

</style>

@if(count($errors))

    <div class="form-group mt-4">

    	<div class="alert alert-danger pb-0">

    		<ul>

	      		@foreach($errors->all() as $error)

	      			<li class="text-center mb-0" style="font-family: Nunito; font-size: 16px;">{{ $error }}</li>

	      		@endforeach

      		</ul>


      	</div>

    </div>

@endif