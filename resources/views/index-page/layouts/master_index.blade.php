<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>MoSHE Admission</title>
  
  <link rel="stylesheet" href="../../../css/style.css">


  <link href="../../../font/font-awesome/css/font-awesome.min.css" rel="stylesheet">


  <link rel="stylesheet" href="../../../css/materialdesignicons.min.css">
  
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">


  <link rel="stylesheet" href="../../../css/vendor.bundle.base.css">

  <link rel="stylesheet" type="text/css" href="../../../css/my_style.css">


  <link rel="shortcut icon" href="../../../../image/logo-22.png" />
  
</head>

	<body>

		@include('index-page.welcome-layouts.welcome-header')

		<div class="container-scroller">

		    <div class="container-fluid page-body-wrapper full-page-wrapper">

		      	<div class="content-wrapper d-flex auth row ">

	      			@yield('login_content')


	      			@yield('signup_content')
		      			
			    </div>
		    
		    </div>

	  	</div>

	</body>

	@include ('index-page.layouts.master_footer')

</html>