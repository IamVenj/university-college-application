
<style type="text/css">

.navbar-transparent{

  background: linear-gradient(to right, #90caf9, #047edf 99%);

}

</style>

 <header class="header-global">
 
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent headroom">
 
      <div class="container">
 
        <a class="navbar-brand" href="/">
 
          <div class="row">

            <div class="col-md-2">
               
               <img src="../../../image/logo-22.png" style="width: 50px; height: 50px;">

            </div>
            
            <div class="col-md-8">

              <p class="mb-0 tracking-wide" style="font-size: 20px; letter-spacing: 1px;">የሳይንስና ከፍተኛ ትምህርት ሚኒስቴር</p>

              <p class="mt-0 mb-0" style="font-size: 10px; letter-spacing: 1px;">Ministry of Science and Higher Education</p>

            </div>

          </div>

           
        </a>
 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
 
          <span class="navbar-toggler-icon"></span>
 
        </button>
 
        <div class="navbar-collapse collapse" id="navbar_global">
 
          <ul class="navbar-nav align-items-lg-center ml-lg-auto">
  
            <li class="nav-item">
 
              <a href="/" class="nav-link login-nav">
 
                <i class="fa fa-home" style="text-transform: uppercase;"></i>
 
                <span>Home</span>
 
              </a>
 
            </li>

            <li class="nav-item">
 
              <a href="/login" class="nav-link login-nav">
 
                <i class="fa fa-sign-in" style="text-transform: uppercase;"></i>
 
                <span>Login</span>
 
              </a>
 
            </li>

            <li class="nav-item">
 
              <a href="/signup" class="nav-link signup-nav">
 
                <i class="fa fa-user-plus"></i>
 
                <span>Sign Up</span>
 
              </a>
 
            </li>
 
          </ul>
 
        </div>
 
      </div>
 
    </nav>
 
  </header>