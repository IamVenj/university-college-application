 
  <script src="../../../js/vendors/jquery/jquery.min.js"></script>
 
  <script src="../../../js/vendors/popper/popper.min.js"></script>
 
  <script src="../../../js/vendors/bootstrap/bootstrap.min.js"></script>
 
  <script src="../../../js/vendors/headroom/headroom.min.js"></script>
 
  <!-- Optional JS -->
 
  <script src="../../../js/vendors/onscreen/onscreen.min.js"></script>
 
  <script src="../../../js/vendors/nouislider/js/nouislider.min.js"></script>
 
  <script src="../../../js/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
 
  <!-- Argon JS -->
 
  <script src="../../../js/argon.js?v=1.0.1"></script>