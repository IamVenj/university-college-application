@extends ('index-page.layouts.master_index')


@section ('login_content')


	<style>

		.form-control{
			border-color: rgba(0,0,0,0.3); 
			border-top: none; 
			border-left: none; 
			border-right: none;
			color: #000;
			font-size: 16px !important;
			padding: 0px;
			padding-top: 5px;
			padding-left: 2px;
		}

	</style>    

	<div class="row w-100 ml-auto mr-auto mb-auto mt-auto">
		
	  	<div class="col-lg-4 mx-auto">
	    
	    	<div class="auth-form-light text-left p-5">


	    
	    	    <h2 style="font-family: Nunito;">Reset</h2>
	    	    	    
	        	<h6 class="font-weight-light" style="font-family: Nunito;">Reset Your Password</h6>

	    
	          	<form class="pt-3" action="/reset-password" method="POST">
	        
	        		@csrf

	        		<input type="hidden" name="token" value="{{ $token }}">

	        		<div class="form-group">
	        
	                  	<input type="email" style="font-family: Nunito;" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Email" name="email" required>
	        
	                </div>

	                <div class="form-group">
	        
	                  	<input type="password" style="font-family: Nunito;" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Password" name="password" required>
	        
	                </div>

	                <div class="form-group">
	        
	                  	<input type="password" style="font-family: Nunito;" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Confirm Password" name="password_confirmation" required>
	        
	                </div>

	        
	                <div class="mt-3">
	        
	                  	<button type="submit" style="font-family: Nunito;" class="btn btn-block btn-gradient-info btn-lg font-weight-medium auth-form-btn">RESET</button>
	        
	                </div>

	        	</form>

	        	<div class="text-center">

	        		@include('index-page.success_nd_error_page.success')

					@include('index-page.success_nd_error_page.error_')

				</div>

	    	</div>

	    </div>

	</div>

@endsection

