
@extends ('index-page.layouts.master_index')



@section ('signup_content')

	<style>

		.form-control{
			border-color: rgba(0,0,0,0.3); 
			border-top: none; 
			border-left: none; 
			border-right: none;
			color: #000;
			font-size: 16px !important;
			padding: 0px;
			padding-top: 5px;
			padding-left: 2px;
		}

	</style>                
         	

	<div class="row w-100 mt-auto mb-auto ml-auto mr-auto">

		<div class="col-md-6 order-lg-2 ml-lg-auto">

	        <div class="position-relative pl-md-5">

	          <img src="../../../image/signup.svg" width="400" height="500" class="img-center img-fluid">

	        </div>

        </div>

        <div class="col-lg-4 mx-auto">

            <div class="auth-form-light text-left p-5">

              	<div class="wizard-card" data-color="blue" id="wizardProfile"  style="border-radius:25px;">
              	
              	
              		<h2 style="font-family: Nunito;">Sign Up</h2>
              	
              		<h6 class="font-weight-light mb-3"  style="font-family: Nunito; font-size:15px;">Signing up is easy. It only takes a few steps</h6>
	        
              	
	              	<form class="pt-3" method="post" action="/signup">

	              		{{ csrf_field() }}
	                	
		            
		                <div class="form-group">
		            
		                  	<input type="email" style="font-family: Nunito;" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Email" name="email" required>
		            
		                </div>


		     			<div class="form-group">
		            
		                 	<input type="password" style="font-family: Nunito;" class="form-control form-control-sm" id="password" placeholder="Password" name="password" required>
		            
		                </div>


						<div class="form-group mb-3">
		            
		                 	<input type="password" style="font-family: Nunito;" class="form-control form-control-sm" id="password_confirmation" placeholder="Confirm Password" name="password_confirmation" required>
		            
		                </div>

		                <div class="form-group mb-3">
		           

		            
		                <div class="mt-2">
		            
		                  	<button class="btn btn-block btn-gradient-info btn-lg font-weight-medium auth-form-btn" type="submit" style="font-family: Nunito;">SIGN UP</button>
		            
		                </div>


		            
		                <p class="text-center mt-4 font-weight-light" style="font-style: bold; font-size:20px; font-family: Nunito;">
		            
		                  	Already have an account? <a href="/login" class="text-info">Login</a>
		            
		                </p>

		                <div class="text-center">

			                @include('index-page.success_nd_error_page.success')

							@include('index-page.success_nd_error_page.error_')

						</div>
	              	
	              	</form>

              	</div>

            </div>
        
        </div>
   	
   	</div>

@endsection