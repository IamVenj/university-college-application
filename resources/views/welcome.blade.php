<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <meta name="author" content="Creative Tim">

  <title>MoSHE Admission - Welcome</title>


  <link rel="shortcut icon" href="../../../../image/logo-22.png" />



  <link href="../../../font/font-awesome/css/font-awesome.min.css" rel="stylesheet">

  <link type="text/css" href="../../../css/argon.css?v=1.0.1" rel="stylesheet">
  
</head>



<body>

  @include('index-page.welcome-layouts.welcome-header') 

  <main>

    @yield('welcome-content')

    @yield('notice_course_content')

  </main>

  @include('index-page.welcome-layouts.welcome-footer')

</body>

</html>