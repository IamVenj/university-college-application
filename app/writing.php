<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class writing extends Model
{
    protected $fillable = ['user_id', 'essay', 'disciplinary_violation', 'disciplinary_violation_description', 'conviction', 'conviction_description'];

    protected $hidden = ['user_id'];

    public function check_if_data_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }

    public function get_data($id)
    {
    	return $this->where('user_id', $id)->first();
    }

    public function get_writing()
    {
    	return $this->where('user_id', auth()->user()->id)->get();
    }

    public function count()
    {
    	return $this->where('user_id', auth()->user()->id)->count();
    }
}
