<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\VerifyEmail;

use App\university_membership;

use Carbon\Carbon;

use App\award_status;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','token', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function role()
    {
        return $this->belongsTo('App\role', 'role_id');
    }

    public function verified()

    {

        return $this->token === null;

    }

    public function isConnected()
    {
        $connected = @fsockopen("www.google.com", 80);

        if($connected)
        {
            return true;
        }
        else{
            return false;
        }
    }

    public function changeDateFormat($date)
    {

        return Carbon::parse($date)->format('m/d/Y');

    }

    public function get_users_for_panel($university_id, $role_id)
    {
        $university_memberships = university_membership::where('university_id', $university_id)->where('role_id', $role_id)->get();

        return $university_memberships;
        
    }

    public function get_employee_users_for_panel($university_id, $role_id)
    {
        $university_memberships = university_membership::where('university_id', $university_id)->where('role_id', $role_id)->get();

        foreach ($university_memberships as $key => $value) {
            
            $this_user = $this->where('id', $university_memberships[$key]->user_id)->get();

            return $this_user;

        }

        
        
    }

    public function get_users_for_judgment($university_id, $id)
    {
        $award_status = new award_status();

        $submission = $award_status->get_users($id, $university_id);

        foreach ($submission as $submit) {
        
            $this_user = $this->where('id', $submit->user_id)->get();

            return $this_user;

        }

        
    }

    public function sendVerificationEmail(){

        $this->notify(new VerifyEmail($this));

    }


}
