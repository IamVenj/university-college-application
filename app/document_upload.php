<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class document_upload extends Model
{

	protected $fillable = ['document_type', 'document', 'user_id'];

	protected $hidden = ['user_id'];

    public function check_if_document_exists($id, $doc)
    {

    	return $this->where('user_id', $id)->where('document', $doc)->count();

    }

    public function check_if_document_type_exists($id, $doc)
    {

        return $this->where('user_id', $id)->where('document_type', $doc)->count();

    }

    public function check_if_user_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }

    public function getDoc($id)
    {
    	return $this->where('user_id', $id)->get();
    }
}
