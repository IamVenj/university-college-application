<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\academics;

use App\university_membership;

use App\universities;

use App\Questions;

use Carbon\Carbon;

class academicsController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function create()

    {
        if(auth()->user()->role_id == 2)
        {

            $Questions = new Questions();

            $academics_f = new academics();

            $university_id = $Questions->get_university_id();

            $academics = $academics_f->get_academics_that_university_registered($university_id);

        	return view('main_layout.academics.create_academics', compact('academics'));
        }
        else
        {
            return view('main_layout.error.error-403');
        }

    }


    public function store()

    {

        $university_membership = new university_membership();

        $academics = new academics();


        $this->validate(request(), [

            'academics_name' => ['required', 'max:255', 'string']

        ]);

        $check_if_academics_has_been_used = $academics->check_if_academics_has_been_used(request('academics_name'), $university_membership->get_university_id_of_this());

        if($check_if_academics_has_been_used == 0)
        {


            academics::create([

                'academics_name' => request('academics_name'),

                'university_id' => $university_membership->get_university_id_of_this(),

                'user_id' => auth()->user()->id

            ]);


            return redirect()->back()->with('success', 'Academics Successfully Created');

        }
        else
        {
            return redirect()->back()->withErrors('Academics Name has already been taken!');
        }


    }

    public function destroy($id)
    {

        $delete_academics = academics::find($id);

        $delete_academics->delete();

        return redirect()->back()->with('success', 'Academics is Deleted Successfully!');

    }

    public function update($id)
    {

        $academics_update = academics::find($id);

        $academics_update->academics_name = request('academics_name');
        
        $academics_update->save();

        return redirect()->back()->with('success', 'Academics Updated Successfully');

    }


}
