<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\User;

use App\university_membership;

class CreateAdminUserController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function create()
    {

        if(auth()->user()->role_id == 1)
        {
            $uni_membership = new university_membership();

        	$universities = universities::all();

            $university_memberships = $uni_membership->user_membership();



        	return view('main_layout.user.create_admin', compact('universities', 'university_memberships'));
        }
        else
        {

            return view('main_layout.error.error-403');

        }

    }

    public function store()
    {

    	$uni_membership = new university_membership();

    	$this->validate(request(), [
    		
    		'email' => ['required', 'email', 'max:255', 'unique:users'],

    		'password' => ['required', 'min:6'],

    		'university' => ['required']
    	
    	]);

        if($uni_membership->check_if_university_has_an_admin(request('university')) == 0)

        {

        	User::create([

        		'role_id' => request('role_id'),

        		'email' => request('email'),

        		'password' => bcrypt(request('password'))

        	]);

        	if($uni_membership->check_created_user(request('email')) > 0){

        		university_membership::create([

        			'user_id' => $uni_membership->get_created_user(request('email'))->first()->id,

        			'university_id' => $uni_membership->get_university(request('university'))->first()->id,

        			'role_id' => 2

        		]);

        	}

        }else{

            return redirect()->back()->withErrors('University already has an admin!');

        }

    	return redirect()->back()->with('success', 'Admin Account is Created Successfully!');

    }
}
