<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Hash;

class ChangePasswordController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }
    
    public function create()
    {

    	return view('main_layout.change-password.create_change_password');

    }

    public function update()
    {

        $this->validate(request(),[

            'password' => ['required', 'confirmed', 'min:6']

        ]);


    	$user = User::where('id', auth()->user()->id)->first();


    	$current_password = request('current_password');

    	$new_password = request('password');


    	if(Hash::check($current_password, $user->password))
    	
    	{

    		$user->password = bcrypt($new_password);

    		$user->save();

    		return redirect()->back()->with('success', 'You have Changed Your Password');

    	}

    	else

    	{

    		return redirect()->back()->withErrors('Your Old Password Is Not Correct!');
    	
    	}

    }
    
}
