<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\university_membership;

use App\User;

use App\education;

use App\award_status;

use App\answer_for_university_questions;

use App\universities_applied;

use App\Courses;

class UniversityMembershipController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function show()
    {
    	$university = universities::all();

    	return view('main_layout.Universities.change_university', compact('university'));
    }

    public function update($id)
    {

        $this->validate(request(), [

            'certificate' => 'required',

            'field' => 'required',

            'university' => 'required'

        ]);

        $check_if_you_have_applied_before = universities_applied::where('university_id', intval(request('university')))->where('user_id', $id)->where('course_id', request('field'))->count();


        $date_check = Courses::where('id', request('field'))->first();


        $now_timestamp = strtotime(date('Y-m-d'));

        if ($now_timestamp <= strtotime($date_check->start_date) || $now_timestamp >= strtotime($date_check->end_date)) {
            
            if ($now_timestamp <= strtotime($date_check->start_date))
            {
                return redirect()->back()->withErrors('The Admission date for this field has not begun yet!');
            }

            elseif($now_timestamp >= strtotime($date_check->end_date))

            {
                return redirect()->back()->withErrors('The Admission date for this field has passed!');
            }

        }
        else
        {

            if($check_if_you_have_applied_before == 0)
            {


                // university membership

                $check_if_member_exists = university_membership::where('university_id', request('university'))->where('user_id', $id)->count();

                if($check_if_member_exists == 0)
                {

                    university_membership::create([

                        'user_id' => $id,

                        'university_id' => request('university'),

                        'academics_id' => request('certificate'),

                        'course_id' => request('field')

                    ]);

                }
                else
                {

                    $university = university_membership::where('university_id', request('university'))->where('user_id', $id)->first();

                    $university->university_id = request('university');

                    $university->academics_id = request('certificate');

                    $university->course_id = request('field');

                    $university->save();

                }

                // universities applied

                if($check_if_you_have_applied_before == 0)
                {

                    universities_applied::create([

                        'user_id' => $id,

                        'university_id' => request('university'),

                        'course_id' => request('field')

                    ]);

                } 

                else
                {

                    $university = universities_applied::where('university_id', request('university'))->where('user_id', $id)->where('course_id', request('course_id'))->first();

                    $university->university_id = request('university');

                    $university->course_id = request('field');

                    $university->save();

                }


                // update user

                $user = User::find($id);

                $user->academics_id = request('certificate');

                $user->university_id = request('university');

                $user->save();


                // update education - course and academics

                $education = education::where('user_id', $id)->first();

                $education->course_id = request('field');

                $education->academics_id = request('certificate');

                $education->save();


            	$get_uni = universities::find(request('university'));

            	return redirect()->back()->with('success', 'You have Successfully changed to '.$get_uni->university_name);

            }
            else
            {
                return redirect()->back()->withErrors('You cannot apply to the same university with the same field, you have applied here before!');
            }

        }

    }

}
