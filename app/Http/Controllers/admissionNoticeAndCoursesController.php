<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\Courses;

use App\Information_Posts;

class admissionNoticeAndCoursesController extends Controller
{

	public function __construct()
	{

		$this->middleware('guest');

	}

    public function index()
    {

    	$universities = universities::all();

    	return view('index-page.notice_&_course.admission_notice_courses', compact('universities'));

    }

    public function show($id)
    {

        $Information_Posts_notice = Information_Posts::where('university_id', $id)->where('info_type', 'admission_notice')->get();
    	
        $Information_Posts_course = Information_Posts::where('university_id', $id)->where('info_type', 'available_courses')->get();

        $university = universities::find($id);


    	return view('index-page.notice_&_course.show_university', compact('Information_Posts_notice', 'university', 'Information_Posts_course'));

    }
}
