<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\academics;

use App\Questions;

use App\Courses;

use Carbon\Carbon;

class CourseController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function create()
    {
        if(auth()->user()->role_id == 2)
        {

        	$Questions = new Questions();

            $academics_f = new academics();

            $courses = new Courses();

            $university_id = $Questions->get_university_id();

            $academics = $academics_f->get_academics_that_university_registered($university_id);

            $course = $courses->courses($university_id);

        	return view('main_layout.course.create_course', compact('academics', 'course'));

        }
        else
        {

            return view('main_layout.error.error-403');

        }
    
    }

    public function store()
    {


    	$Questions = new Questions();


        $university_id = $Questions->get_university_id();


    	$this->validate(request(),[

    		'get_academics' => 'required',

    		'course_name' => 'required'

    	]);

    	Courses::create([

    		'academics_id' => request('get_academics'),

    		'course_name' => request('course_name'),

    		'university_id' => $university_id,

            'start_date' => Carbon::createFromFormat('m/d/Y', request('start_date'))->format('Y-m-d'),

            'end_date' => Carbon::createFromFormat('m/d/Y', request('end_date'))->format('Y-m-d')

    	]);

    	return redirect()->back()->with('success', 'Course is added successfully');


    }

    public function update($id)
    {

        $update_course = Courses::find($id);

        $update_course->academics_id = request('academics_update');

        $update_course->course_name = request('course_name_update');

        $update_course->start_date = Carbon::createFromFormat('m/d/Y', request('start_date_update'))->format('Y-m-d');
        
        $update_course->end_date = Carbon::createFromFormat('m/d/Y', request('end_date_update'))->format('Y-m-d');

        $update_course->save();

        return redirect()->back()->with('success', 'Course '.request('course_name_update').' is updated Successfully');

    }

    public function destroy($id)
    {


        $destroy_course = Courses::find($id);

        $destroy_course->delete();

        return redirect()->back()->with('success', 'Course is deleted Successfully');

    }
}
