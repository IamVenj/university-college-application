<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\award_status;

use App\university_membership;

use App\education;

use App\previous_education;

use App\highschool;

use App\future_plans;

use App\family;

use App\activities;

use App\writing;

use App\father;

use App\mother;

use App\guardian;

use App\other;

use App\children;

use App\document_upload;

use App\universities_applied;

use App\answer_for_university_questions;

use App\step_quantities;

use App\Questions;

use App\Courses;

class SubmissionController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function store()
    {

        $university_membership = new university_membership();

        $university_id = $university_membership->get_university_id_of_this();


        if(!is_null(auth()->user()->firstname) && !is_null(auth()->user()->middlename) && !is_null(auth()->user()->lastname) && !is_null(auth()->user()->sex) && !is_null(auth()->user()->date_of_birth) && !is_null(auth()->user()->country_code) && !is_null(auth()->user()->phone_number) && !is_null(auth()->user()->address_line) && !is_null(auth()->user()->city) && !is_null(auth()->user()->subcity) && !is_null(auth()->user()->woreda) && !is_null(auth()->user()->house_number))
        {



            $education = education::where('user_id', auth()->user()->id)->first();

            $prev_ed = previous_education::where('user_id', auth()->user()->id)->count();

            $highschool = highschool::where('user_id', auth()->user()->id)->count();

            $future_plans = future_plans::where('user_id', auth()->user()->id)->first();

            $family = family::where('user_id', auth()->user()->id)->first();

            $activity = activities::where('user_id', auth()->user()->id)->count();

            $writing = writing::where('user_id', auth()->user()->id)->count();

            $doc = new document_upload();

            $answer = new answer_for_university_questions();

            $question = new Questions();

            $step_quantity = step_quantities::where('university_id', $university_id)->first();

            $question_count_1 = $question->get_question_count($education->academics_id, 1, auth()->user()->university_id, $education->course_id);
            
            $question_count_2 = $question->get_question_count($education->academics_id, 2, auth()->user()->university_id, $education->course_id);
            
            $question_count_3 = $question->get_question_count($education->academics_id, 3, auth()->user()->university_id, $education->course_id);
            
            $question_count_4 = $question->get_question_count($education->academics_id, 4, auth()->user()->university_id, $education->course_id);



            $date_check = Courses::where('id', $education->course_id)->first();

            $now_timestamp = strtotime(date('Y-m-d'));

            if ($now_timestamp <= strtotime($date_check->start_date) || $now_timestamp >= strtotime($date_check->end_date)) {

                if ($now_timestamp <= strtotime($date_check->start_date))
                {
                    return redirect()->back()->withErrors('The Admission date for this field has not begun yet!');
                }

                elseif($now_timestamp >= strtotime($date_check->end_date))

                {
                    return redirect()->back()->withErrors('The Admission date for this field has passed!');
                }

            }

            else

            {

                if(!is_null($education))
                {

                    if(is_null($education->previous_education))
                    {

                        return redirect('/education')->withErrors('You have not finished your form yet!');

                    }
                    else
                    {

                        if($education->previous_education == 'yes')
                        {

                            if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')
                            {

                                if(!is_null($education->number_of_highschools) && !is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->national_exam_result) && !is_null(number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))
                                {

                                    if($highschool > 0 && $prev_ed > 0)
                                    {

                                        if(!is_null($family))
                                        {

                                            $father = father::where('id', $family->father_id)->count();

                                            $mother = mother::where('id', $family->mother_id)->count();

                                            $guardian = guardian::where('id', $family->guardian_id)->count();

                                            $other = other::where('id', $family->other_id)->count();

                                            if(!is_null($family->father_id) || !is_null($family->mother_id) || !is_null($family->guardian_id) || !is_null($family->other_id) || $family->currently_living_with == 'Alone')
                                            {

                                                if($father > 0 || $mother > 0 || $guardian > 0 || $other > 0 || $family->currently_living_with == 'Alone')
                                                {

                                                    if(!is_null($family->marital_status) && !is_null($family->number_of_siblings) && !is_null($family->children))
                                                    {

                                                        if($family->children == 'yes')
                                                        {

                                                            $children = children::where('user_id', auth()->user()->id)->count();

                                                            if($children > 0)
                                                            {

                                                                if($activity > 0)
                                                                {

                                                                    if($writing > 0)
                                                                    {

                                                                        if(!is_null($step_quantity))
                                                                        {

                                                                            $num = $step_quantity->step_quantity;

                                                                            if($num == 1)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);



                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            }

                                                                            if($num == 2)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }
                                                                                    else
                                                                                    {

                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            
                                                                            }

                                                                            if($num == 3)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            if($num == 4)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {
                                                                                            if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                            {

                                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                                {

                                                                                                    award_status::create([

                                                                                                        'user_id' => auth()->user()->id,

                                                                                                        'university_id' => auth()->user()->university_id,

                                                                                                        'submission' => 1

                                                                                                    ]);

                                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                                }

                                                                                            }

                                                                                            else
                                                                                            {
                                                                                                return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                            {

                                                                                award_status::create([

                                                                                    'user_id' => auth()->user()->id,

                                                                                    'university_id' => auth()->user()->university_id,

                                                                                    'submission' => 1

                                                                                ]);

                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                            }
                                                                            else
                                                                            {
                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                            }
                                                                            
                                                                        }

                                                                    }
                                                                    else
                                                                    {

                                                                        return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                                }                                                        

                                                            }

                                                        }
                                                        else
                                                        {

                                                            if($activity > 0)
                                                            {

                                                                if($writing > 0)
                                                                {

                                                                    if(!is_null($step_quantity))
                                                                    {

                                                                        $num = $step_quantity->step_quantity;

                                                                        if($num == 1)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {

                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                {

                                                                                    award_status::create([

                                                                                        'user_id' => auth()->user()->id,

                                                                                        'university_id' => auth()->user()->university_id,

                                                                                        'submission' => 1

                                                                                    ]);

                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                }
                                                                                else
                                                                                {
                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        }

                                                                        if($num == 2)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }
                                                                                else
                                                                                {

                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        
                                                                        }

                                                                        if($num == 3)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        if($num == 4)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                        {

                                                                            award_status::create([

                                                                                'user_id' => auth()->user()->id,

                                                                                'university_id' => auth()->user()->university_id,

                                                                                'submission' => 1

                                                                            ]);

                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                        }
                                                                        else
                                                                        {
                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                        }
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                }

                                                            }
                                                            else
                                                            {

                                                                return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                            }

                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                
                                                    return redirect('/fam')->withErrors('You have not finished your form yet!');

                                                }

                                            }

                                        }

                                    }
                                    else
                                    {

                                        return redirect('/education')->withErrors('You have not finished your form yet!');

                                    }

                                }

                            }
                            else
                            {

                                if(!is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))
                                {

                                    if($prev_ed > 0)
                                    {

                                        if(!is_null($family))
                                        {

                                            $father = father::where('id', $family->father_id)->count();

                                            $mother = mother::where('id', $family->mother_id)->count();

                                            $guardian = guardian::where('id', $family->guardian_id)->count();

                                            $other = other::where('id', $family->other_id)->count();

                                            if(!is_null($family->father_id) || !is_null($family->mother_id) || !is_null($family->guardian_id) || !is_null($family->other_id) || $family->currently_living_with == 'Alone')
                                            {

                                                if($father > 0 || $mother > 0 || $guardian > 0 || $other > 0 || $family->currently_living_with == 'Alone')
                                                {

                                                    if(!is_null($family->marital_status) && !is_null($family->number_of_siblings) && !is_null($family->children))
                                                    {

                                                        if($family->children == 'yes')
                                                        {

                                                            $children = children::where('user_id', auth()->user()->id)->count();

                                                            if($children > 0)
                                                            {

                                                                if($activity > 0)
                                                                {

                                                                    if($writing > 0)
                                                                    {

                                                                        if(!is_null($step_quantity))
                                                                        {

                                                                            $num = $step_quantity->step_quantity;

                                                                            if($num == 1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            }

                                                                            if($num == 2)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {

                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            
                                                                            }

                                                                            if($num == 3)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            if($num == 4)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {
                                                                                            if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                            {

                                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                                {

                                                                                                    award_status::create([

                                                                                                        'user_id' => auth()->user()->id,

                                                                                                        'university_id' => auth()->user()->university_id,

                                                                                                        'submission' => 1

                                                                                                    ]);

                                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                                }

                                                                                            }

                                                                                            else
                                                                                            {
                                                                                                return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                            {

                                                                                award_status::create([

                                                                                    'user_id' => auth()->user()->id,

                                                                                    'university_id' => auth()->user()->university_id,

                                                                                    'submission' => 1

                                                                                ]);

                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                            }
                                                                            else
                                                                            {
                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                            }
                                                                            
                                                                        }

                                                                    }
                                                                    else
                                                                    {

                                                                        return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                                }                                                        

                                                            }

                                                        }
                                                        else
                                                        {

                                                            if($activity > 0)
                                                            {

                                                                if($writing > 0)
                                                                {

                                                                    if(!is_null($step_quantity))
                                                                    {

                                                                        $num = $step_quantity->step_quantity;

                                                                        if($num == 1)
                                                                        {
                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {

                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                {

                                                                                    award_status::create([

                                                                                        'user_id' => auth()->user()->id,

                                                                                        'university_id' => auth()->user()->university_id,

                                                                                        'submission' => 1

                                                                                    ]);

                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                }
                                                                                else
                                                                                {
                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        }

                                                                        if($num == 2)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        
                                                                        }

                                                                        if($num == 3)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        if($num == 4)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                        {

                                                                            award_status::create([

                                                                                'user_id' => auth()->user()->id,

                                                                                'university_id' => auth()->user()->university_id,

                                                                                'submission' => 1

                                                                            ]);

                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                        }
                                                                        else
                                                                        {
                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                        }
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                }

                                                            }
                                                            else
                                                            {

                                                                return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                            }

                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                
                                                    return redirect('/fam')->withErrors('You have not finished your form yet!');

                                                }

                                            }

                                        }

                                    }
                                    else
                                    {

                                        return redirect('/education')->withErrors('You have not finished your form yet!');

                                    }

                                }

                            }

                        }
                        else
                        {

                            if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')
                            {

                                if(!is_null($education->number_of_highschools) && !is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->national_exam_result) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended) && !is_null($education->number_of_honors))
                                {

                                    if($highschool > 0)
                                    {

                                        if(!is_null($family))
                                        {

                                            $father = father::where('id', $family->father_id)->count();

                                            $mother = mother::where('id', $family->mother_id)->count();

                                            $guardian = guardian::where('id', $family->guardian_id)->count();

                                            $other = other::where('id', $family->other_id)->count();

                                            if(!is_null($family->father_id) || !is_null($family->mother_id) || !is_null($family->guardian_id) || !is_null($family->other_id) || $family->currently_living_with == 'Alone')
                                            {

                                                if($father > 0 || $mother > 0 || $guardian > 0 || $other > 0 || $family->currently_living_with == 'Alone')
                                                {

                                                    if(!is_null($family->marital_status) && !is_null($family->number_of_siblings) && !is_null($family->children))
                                                    {

                                                        if($family->children == 'yes')
                                                        {

                                                            $children = children::where('user_id', auth()->user()->id)->count();

                                                            if($children > 0)
                                                            {

                                                                if($activity > 0)
                                                                {

                                                                    if($writing > 0)
                                                                    {

                                                                        if(!is_null($step_quantity))
                                                                        {

                                                                            $num = $step_quantity->step_quantity;

                                                                            if($num == 1)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            }

                                                                            if($num == 2)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }
                                                                                    else
                                                                                    {

                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                                }
                                                                            
                                                                            }

                                                                            if($num == 3)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            if($num == 4)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                        {
                                                                                            if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                            {

                                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                                {

                                                                                                    award_status::create([

                                                                                                        'user_id' => auth()->user()->id,

                                                                                                        'university_id' => auth()->user()->university_id,

                                                                                                        'submission' => 1

                                                                                                    ]);

                                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                                }

                                                                                            }

                                                                                            else
                                                                                            {
                                                                                                return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                        }
                                                                        else
                                                                        {
                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                            {

                                                                                award_status::create([

                                                                                    'user_id' => auth()->user()->id,

                                                                                    'university_id' => auth()->user()->university_id,

                                                                                    'submission' => 1

                                                                                ]);

                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                            }
                                                                            else
                                                                            {
                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                            }
                                                                            
                                                                        }

                                                                    }
                                                                    else
                                                                    {

                                                                        return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                                }                                                        

                                                            }

                                                        }
                                                        else
                                                        {

                                                            if($activity > 0)
                                                            {

                                                                if($writing > 0)
                                                                {

                                                                    if(!is_null($step_quantity))
                                                                    {

                                                                        $num = $step_quantity->step_quantity;

                                                                        if($num == 1)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {

                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                {

                                                                                    award_status::create([

                                                                                        'user_id' => auth()->user()->id,

                                                                                        'university_id' => auth()->user()->university_id,

                                                                                        'submission' => 1

                                                                                    ]);

                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                }
                                                                                else
                                                                                {
                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        }

                                                                        if($num == 2)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {

                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }
                                                                                else
                                                                                {

                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        
                                                                        }

                                                                        if($num == 3)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        if($num == 4)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'h_transcript') > 0 && $doc->check_if_document_type_exists(auth()->user()->id, 'n_e_doc') > 0)
                                                                        {

                                                                            award_status::create([

                                                                                'user_id' => auth()->user()->id,

                                                                                'university_id' => auth()->user()->university_id,

                                                                                'submission' => 1

                                                                            ]);

                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                        }
                                                                        else
                                                                        {
                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                        }
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                }

                                                            }
                                                            else
                                                            {

                                                                return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                            }

                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                
                                                    return redirect('/fam')->withErrors('You have not finished your form yet!');

                                                }

                                            }

                                        }

                                    }
                                    else
                                    {

                                        return redirect('/education')->withErrors('You have not finished your form yet!');

                                    }

                                }

                            }
                            else
                            {

                                if(!is_null($education->number_of_cbo) && !is_null($education->cgpa) && !is_null($education->number_of_honors) && !is_null($future_plans->description) && !is_null($future_plans->highest_degree_intended))
                                {

                                    if(!is_null($family))
                                    {

                                        $father = father::where('id', $family->father_id)->count();

                                        $mother = mother::where('id', $family->mother_id)->count();

                                        $guardian = guardian::where('id', $family->guardian_id)->count();

                                        $other = other::where('id', $family->other_id)->count();

                                        if(!is_null($family->father_id) || !is_null($family->mother_id) || !is_null($family->guardian_id) || !is_null($family->other_id) || $family->currently_living_with == 'Alone')
                                        {

                                            if($father > 0 || $mother > 0 || $guardian > 0 || $other > 0 || $family->currently_living_with == 'Alone')
                                            {

                                                if(!is_null($family->marital_status) && !is_null($family->number_of_siblings) && !is_null($family->children))
                                                {

                                                    if($family->children == 'yes')
                                                    {

                                                        $children = children::where('user_id', auth()->user()->id)->count();

                                                        if($children > 0)
                                                        {

                                                            if($activity > 0)
                                                            {

                                                                if($writing > 0)
                                                                {

                                                                    if(!is_null($step_quantity))
                                                                    {

                                                                        $num = $step_quantity->step_quantity;

                                                                        if($num == 1)
                                                                        {
                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {

                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                {

                                                                                    award_status::create([

                                                                                        'user_id' => auth()->user()->id,

                                                                                        'university_id' => auth()->user()->university_id,

                                                                                        'submission' => 1

                                                                                    ]);

                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                }
                                                                                else
                                                                                {
                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        }

                                                                        if($num == 2)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {

                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                            }
                                                                        
                                                                        }

                                                                        if($num == 3)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        if($num == 4)
                                                                        {

                                                                            if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                    {
                                                                                        if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                        {

                                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                            {

                                                                                                award_status::create([

                                                                                                    'user_id' => auth()->user()->id,

                                                                                                    'university_id' => auth()->user()->university_id,

                                                                                                    'submission' => 1

                                                                                                ]);

                                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                            }

                                                                                        }

                                                                                        else
                                                                                        {
                                                                                            return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                        {

                                                                            award_status::create([

                                                                                'user_id' => auth()->user()->id,

                                                                                'university_id' => auth()->user()->university_id,

                                                                                'submission' => 1

                                                                            ]);

                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                        }
                                                                        else
                                                                        {
                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                        }
                                                                        
                                                                    }

                                                                }
                                                                else
                                                                {

                                                                    return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                                }

                                                            }
                                                            else
                                                            {

                                                                return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                            }                                                        

                                                        }

                                                    }
                                                    else
                                                    {

                                                        if($activity > 0)
                                                        {

                                                            if($writing > 0)
                                                            {

                                                                if(!is_null($step_quantity))
                                                                {

                                                                    $num = $step_quantity->step_quantity;

                                                                    if($num == 1)
                                                                    {
                                                                        if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                        {

                                                                            if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                            {

                                                                                award_status::create([

                                                                                    'user_id' => auth()->user()->id,

                                                                                    'university_id' => auth()->user()->university_id,

                                                                                    'submission' => 1

                                                                                ]);

                                                                                return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                            }
                                                                            else
                                                                            {
                                                                                return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                            }

                                                                        }

                                                                        else
                                                                        {

                                                                            return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                        }
                                                                    }

                                                                    if($num == 2)
                                                                    {

                                                                        if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                        {
                                                                            if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                            {

                                                                                if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                {

                                                                                    award_status::create([

                                                                                        'user_id' => auth()->user()->id,

                                                                                        'university_id' => auth()->user()->university_id,

                                                                                        'submission' => 1

                                                                                    ]);

                                                                                    return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                }
                                                                                else
                                                                                {
                                                                                    return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                }

                                                                            }

                                                                            else
                                                                            {

                                                                                return redirect('/step-two')->withErrors('You have not completed your forms yet');

                                                                            }

                                                                        }

                                                                        else
                                                                        {

                                                                            return redirect('/step-one')->withErrors('You have not completed your forms yet');

                                                                        }
                                                                    
                                                                    }

                                                                    if($num == 3)
                                                                    {

                                                                        if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                        {
                                                                            if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                {

                                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                    {

                                                                                        award_status::create([

                                                                                            'user_id' => auth()->user()->id,

                                                                                            'university_id' => auth()->user()->university_id,

                                                                                            'submission' => 1

                                                                                        ]);

                                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        else
                                                                        {
                                                                            return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                        }

                                                                    }

                                                                    if($num == 4)
                                                                    {

                                                                        if($answer->get_answers_count(auth()->user()->id, 1, auth()->user()->university_id) == $question_count_1)
                                                                        {
                                                                            if($answer->get_answers_count(auth()->user()->id, 2, auth()->user()->university_id) == $question_count_2)
                                                                            {
                                                                                if($answer->get_answers_count(auth()->user()->id, 3, auth()->user()->university_id) == $question_count_3)
                                                                                {
                                                                                    if($answer->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == $question_count_4)
                                                                                    {

                                                                                        if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                                        {

                                                                                            award_status::create([

                                                                                                'user_id' => auth()->user()->id,

                                                                                                'university_id' => auth()->user()->university_id,

                                                                                                'submission' => 1

                                                                                            ]);

                                                                                            return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                                        }

                                                                                    }

                                                                                    else
                                                                                    {
                                                                                        return redirect('/step-four')->withErrors('You have not completed your forms yet');
                                                                                    }

                                                                                }

                                                                                else
                                                                                {
                                                                                    return redirect('/step-three')->withErrors('You have not completed your forms yet');
                                                                                }

                                                                            }

                                                                            else
                                                                            {
                                                                                return redirect('/step-two')->withErrors('You have not completed your forms yet');
                                                                            }

                                                                        }

                                                                        else
                                                                        {
                                                                            return redirect('/step-one')->withErrors('You have not completed your forms yet');
                                                                        }

                                                                    }

                                                                }
                                                                else
                                                                {
                                                                    if($doc->check_if_document_type_exists(auth()->user()->id, 'degree') > 0)
                                                                    {

                                                                        award_status::create([

                                                                            'user_id' => auth()->user()->id,

                                                                            'university_id' => auth()->user()->university_id,

                                                                            'submission' => 1

                                                                        ]);

                                                                        return redirect('/cannot-access')->with('success', 'Your form is successfully submitted');

                                                                    }
                                                                    else
                                                                    {
                                                                        return redirect('/upload-doc')->withErrors('Please upload your PDF Documents');
                                                                    }
                                                                    
                                                                }

                                                            }
                                                            else
                                                            {

                                                                return redirect('/writing')->withErrors('You have not finished your form yet!');

                                                            }

                                                        }
                                                        else
                                                        {

                                                            return redirect('/activities')->withErrors('You have not finished your form yet!');

                                                        }

                                                    }

                                                }
                                            }
                                            else
                                            {
                                            
                                                return redirect('/fam')->withErrors('You have not finished your form yet!');

                                            }

                                        }

                                    }

                                }
                                else
                                {

                                    return redirect('/education')->withErrors('You have not finished your form yet!');

                                }

                            }

                        }

                    }

                }
                
            }

        }

        else
            
        {

            return redirect('/my-info')->withErrors('You have not finished your form yet!');

        }

    }

}
