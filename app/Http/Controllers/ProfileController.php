<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use Carbon\Carbon;

class ProfileController extends Controller
{
    public function index()
    {

    	$user = new User();

    	$date = $user->changeDateFormat(auth()->user()->date_of_birth);

    	return view('main_layout.profile.profile', compact('date'));

    }

    public function update($id)
    {
    	$this->validate(request(),[

    		'first_name' => 'required',

    		'middle_name' => 'required',

    		'last_name' => 'required',

    		'gender' => 'required',

    		'date_of_birth' => 'required',

    		'country_code' => 'required',

    		'phone_number' => 'required',

    		'address_line' => 'required',

    		'email' => ['required', 'email'],

    		'city' => 'required',

    		'subcity' => 'required',

    		'woreda' => 'required',

    		'house_number' => 'required'

    	]);


    	$user = User::find($id);

    	$user->firstname = request('first_name');

    	$user->middlename = request('middle_name');

    	$user->lastname = request('last_name');

    	$user->sex = request('gender');

    	$user->date_of_birth = Carbon::createFromFormat('m/d/Y', request('date_of_birth'))->format('Y-m-d');

    	$user->country_code = request('country_code');

    	$user->phone_number = request('phone_number');

    	$user->address_line = request('address_line');

    	$user->city = request('city');

    	$user->subcity = request('subcity');

    	$user->woreda = request('woreda');

    	$user->house_number = request('house_number');

    	$user->email = request('email');

    	$user->save();

    	return redirect()->back()->with('success', 'Profile is Updated Successfully');
    }
}
