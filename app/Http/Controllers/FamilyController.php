<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\family;

use App\father;

use App\mother;

use App\guardian;

use App\other;

use App\children;

use App\siblings;

use DB;

use App\award_status;

class FamilyController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {
    	$submission = award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

        if($submission == 0)
        {

	    	$family = new family();

	    	$children = new children();

	    	$siblings =  new siblings();

	    	$families = $family->get_family();

	    	$count = $family->check_if_data_exists();

	    	$count_child = $children->check_if_data_exists();

	    	$count_siblings = $siblings->check_if_data_exists();

	    	return view('main_layout.Main-Questions.family', compact('families', 'count', 'count_child', 'count_siblings'));
	    }
	    else
	    {
	    	return redirect('/cannot-access');
	    }
    }


    public function store()
    {

    	$family = new family();

    	$this->validate(request(), [

    		'family_parent' => 'required',

    		'marital_status' => 'required',

    		'children_status' =>'required'

    	]);

    	############### Father ###################

    	if(request('family_parent') == 'Father')
    	{

    		$this->validate(request(), [

    			'father_first_name' => 'required',
    		
	    		'father_middle_name' => 'required',
	    		
	    		'father_last_name' => 'required',
	    		
	    		'father_email' => 'required',
	    		
	    		'father_phone_number' => 'required',
	    		
	    		'father_address' => 'required',

    		]);

    		if($family->check_if_father_exists() == 0){

	    		father::create([

	    			'father_first_name' => request('father_first_name'),
	    			
	    			'father_middle_name' => request('father_middle_name'),
	    			
	    			'father_last_name' => request('father_last_name'),
	    			
	    			'father_email' => request('father_email'),
	    			
	    			'father_phone_number' => request('father_phone_number'),
	    			
	    			'father_address' => request('father_address'),

	    		]);

    			$new_father = father::where('father_first_name', request('father_first_name'))->where('father_email', request('father_email'))->where('father_address', request('father_address'))->first();

    		}
    		else
    		{

    			$get_data = $family->get_data();

    			$get_father = father::where('id', $get_data->father_id)->first();

    			$get_father->father_first_name = request('father_first_name');

    			$get_father->father_middle_name = request('father_middle_name');
    			
    			$get_father->father_last_name = request('father_last_name');
    			
    			$get_father->father_email = request('father_email');
    			
    			$get_father->father_phone_number = request('father_phone_number');
    			
    			$get_father->father_address = request('father_address');

    			$get_father->save();

    		}

    	}

    	################# Mother ########################

    	elseif(request('family_parent') == 'Mother')
    	{

    		$this->validate(request(),[

    			'mother_first_name' => 'required',
	    		
	    		'mother_middle_name' => 'required',
	    		
	    		'mother_last_name' => 'required',
	    		
	    		'mother_email' => 'required',
	    		
	    		'mother_phone_number' => 'required',
	    		
	    		'mother_address' => 'required',

    		]);

    		if($family->check_if_mother_exists() == 0){

	    		mother::create([

	    			'mother_first_name' => request('mother_first_name'),
	    			
	    			'mother_middle_name' => request('mother_middle_name'),
	    			
	    			'mother_last_name' => request('mother_last_name'),
	    			
	    			'mother_email' => request('mother_email'),
	    			
	    			'mother_phone_number' => request('mother_phone_number'),
	    			
	    			'mother_address' => request('mother_address'),

	    		]);

	    		$new_mother = mother::where('mother_first_name', request('mother_first_name'))->where('mother_email', request('mother_email'))->where('mother_address', request('mother_address'))->first();

	    	}
	    	else
	    	{

	    		$get_data = $family->get_data();

    			$get_mother = mother::where('id', $get_data->mother_id)->first();

    			$get_mother->mother_first_name = request('mother_first_name');

    			$get_mother->mother_middle_name = request('mother_middle_name');
    			
    			$get_mother->mother_last_name = request('mother_last_name');
    			
    			$get_mother->mother_email = request('mother_email');
    			
    			$get_mother->mother_phone_number = request('mother_phone_number');
    			
    			$get_mother->mother_address = request('mother_address');

    			$get_mother->save();

	    	}


    	}

    	################# Both ###################

    	elseif(request('family_parent') == 'Both')

    	{

    		$this->validate(request(), [

    			'father_first_name' => 'required',
    		
	    		'father_middle_name' => 'required',
	    		
	    		'father_last_name' => 'required',
	    		
	    		'father_email' => 'required',
	    		
	    		'father_phone_number' => 'required',
	    		
	    		'father_address' => 'required',

	    		'mother_first_name' => 'required',
	    		
	    		'mother_middle_name' => 'required',
	    		
	    		'mother_last_name' => 'required',
	    		
	    		'mother_email' => 'required',
	    		
	    		'mother_phone_number' => 'required',
	    		
	    		'mother_address' => 'required',

    		]);

    		if($family->check_if_father_exists() == 0 && $family->check_if_mother_exists() == 0){

	    		father::create([

	    			'father_first_name' => request('father_first_name'),
	    			
	    			'father_middle_name' => request('father_middle_name'),
	    			
	    			'father_last_name' => request('father_last_name'),
	    			
	    			'father_email' => request('father_email'),
	    			
	    			'father_phone_number' => request('father_phone_number'),
	    			
	    			'father_address' => request('father_address')

	    		]);

	    		mother::create([

	    			'mother_first_name' => request('mother_first_name'),
	    			
	    			'mother_middle_name' => request('mother_middle_name'),
	    			
	    			'mother_last_name' => request('mother_last_name'),
	    			
	    			'mother_email' => request('mother_email'),
	    			
	    			'mother_phone_number' => request('mother_phone_number'),
	    			
	    			'mother_address' => request('mother_address')

	    		]);

	    		$new_father = father::where('father_first_name', request('father_first_name'))->where('father_email', request('father_email'))->where('father_address', request('father_address'))->first();

	    		$new_mother = mother::where('mother_first_name', request('mother_first_name'))->where('mother_email', request('mother_email'))->where('mother_address', request('mother_address'))->first();

	    	}
	    	else
	    	{

	    		$get_data = $family->get_data();

    			$get_father = father::where('id', $get_data->father_id)->first();

    			$get_father->father_first_name = request('father_first_name');

    			$get_father->father_middle_name = request('father_middle_name');
    			
    			$get_father->father_last_name = request('father_last_name');
    			
    			$get_father->father_email = request('father_email');
    			
    			$get_father->father_phone_number = request('father_phone_number');
    			
    			$get_father->father_address = request('father_address');

    			$get_father->save();

    			$get_mother = mother::where('id', $get_data->mother_id)->first();

    			$get_mother->mother_first_name = request('mother_first_name');

    			$get_mother->mother_middle_name = request('mother_middle_name');
    			
    			$get_mother->mother_last_name = request('mother_last_name');
    			
    			$get_mother->mother_email = request('mother_email');
    			
    			$get_mother->mother_phone_number = request('mother_phone_number');
    			
    			$get_mother->mother_address = request('mother_address');

    			$get_mother->save();

	    	}

    	}

    	############### Guardian ###################

    	elseif(request('family_parent') == 'Guardian')
    	{

    		$this->validate(request(), [

    			'guardian_first_name' => 'required',
    		
	    		'guardian_middle_name' => 'required',
	    		
	    		'guardian_last_name' => 'required',
	    		
	    		'guardian_email' => 'required',
	    		
	    		'guardian_phone_number' => 'required',
	    		
	    		'guardian_address' => 'required',

    		]);

    		if($family->check_if_guardian_exists() == 0){

	    		guardian::create([

	    			'guardian_first_name' => request('guardian_first_name'),
	    			
	    			'guardian_middle_name' => request('guardian_middle_name'),
	    			
	    			'guardian_last_name' => request('guardian_last_name'),
	    			
	    			'guardian_email' => request('guardian_email'),
	    			
	    			'guardian_phone_number' => request('guardian_phone_number'),
	    			
	    			'guardian_address' => request('guardian_address')

	    		]);

	    		$new_guardian = guardian::where('guardian_first_name', request('guardian_first_name'))->where('guardian_email', request('guardian_email'))->where('guardian_address', request('guardian_address'))->first();

	    	}
	    	else
	    	{

	    		$get_data = $family->get_data();

    			$get_guardian = guardian::where('id', $get_data->guardian_id)->first();

    			$get_guardian->guardian_first_name = request('guardian_first_name');

    			$get_guardian->guardian_middle_name = request('guardian_middle_name');
    			
    			$get_guardian->guardian_last_name = request('guardian_last_name');
    			
    			$get_guardian->guardian_email = request('guardian_email');
    			
    			$get_guardian->guardian_phone_number = request('guardian_phone_number');
    			
    			$get_guardian->guardian_address = request('guardian_address');

    			$get_guardian->save();

	    	}

    	}

    	############## Other ####################

    	elseif(request('family_parent') == 'Other')
    	{

    		$this->validate(request(), [

    			'other_first_name' => 'required',
    		
	    		'other_middle_name' => 'required',
	    		
	    		'other_last_name' => 'required',
	    		
	    		'other_email' => 'required',
	    		
	    		'other_phone_number' => 'required',
	    		
	    		'other_address' => 'required',

    		]);

    		if($family->check_if_other_exists() == 0){

	    		other::create([

	    			'first_name' => request('other_first_name'),
	    			
	    			'middle_name' => request('other_middle_name'),
	    			
	    			'last_name' => request('other_last_name'),
	    			
	    			'email' => request('other_email'),
	    			
	    			'phone_number' => request('other_phone_number'),
	    			
	    			'address' => request('other_address')

	    		]);

	    		$new_other = other::where('first_name', request('other_first_name'))->where('email', request('other_email'))->where('address', request('other_address'))->first();

	    	}
	    	else
	    	{

	    		$get_data = $family->get_data();

    			$get_other = other::where('id', $get_data->other_id)->first();

    			$get_other->first_name = request('other_first_name');

    			$get_other->middle_name = request('other_middle_name');
    			
    			$get_other->last_name = request('other_last_name');
    			
    			$get_other->email = request('other_email');
    			
    			$get_other->phone_number = request('other_phone_number');
    			
    			$get_other->address = request('other_address');

    			$get_other->save();

	    	}

    	}
    	   	    	
    	################################################### Children ############################################

    	$children = new children();

    	if(request('children_status') == 'yes' && !empty(request('child_name')))
    	{
    		$this->validate(request(), [

    			'child_name' => 'required',

    		]);

    		$child_name = request('child_name');

			$child_email = request('child_email');

    		$child_phone = request('child_phone');


    		if($children->check_if_data_exists() == 0)
    		{

    			$this->validate(request(), [

    				'number_of_children' => 'required',

    			]);
	    		

	    		foreach ($child_name as $key => $value) {
	    			
		    		children::create([

		    			'user_id' => auth()->user()->id,

		    			'fullname' => $child_name[$key],

		    			'email' => $child_email[$key],

		    			'phone_number' => $child_phone[$key]

		    		]); 

		    	}

	    	}
	    	else
	    	{

	    		children::create([

	    			'user_id' => auth()->user()->id,

	    			'fullname' => $child_name,

	    			'email' => $child_email,

	    			'phone_number' => $child_phone

	    		]);

	    	}

    	}

    	####################################### Siblings ###################################

    	$sibling = new siblings();

    	if(!empty(request('sibling_name')))
    	{

    		$this->validate(request(), [

    			'sibling_name' => 'required',

	    		'sibling_email' => 'required',

	    		'sibling_phone_number' => 'required'

    		]);

    		$sibling_name = request('sibling_name');

    		$sibling_email = request('sibling_email');

    		$sibling_phone_number = request('sibling_phone_number');

    		if($sibling->check_if_data_exists() == 0)

			{

				$this->validate(request(), ['number_of_siblings' => 'required']);

	    		foreach ($sibling_name as $key => $value) {

	    			siblings::create([

	    				'user_id' => auth()->user()->id,

		    			'fullname' => $sibling_name[$key],

		    			'email' => $sibling_email[$key],

		    			'phone_number' => $sibling_phone_number[$key]

	    			]);

	    		}

	    	}

	    	else
	    	{

	    		siblings::create([

    				'user_id' => auth()->user()->id,

	    			'fullname' => $sibling_name,

	    			'email' => $sibling_email,

	    			'phone_number' => $sibling_phone_number

    			]);

	    	}

    	}


    	if($family->check_if_data_exists() == 0){

    		if(request('family_parent') == 'Father')
    		{

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'father_id' => $new_father->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}elseif(request('family_parent') == 'Mother'){

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'mother_id' => $new_mother->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}elseif(request('family_parent') == 'Both'){

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'father_id' => $new_father->id,

		    		'mother_id' => $new_mother->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}elseif(request('family_parent') == 'Guardian'){

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'guardian_id' => $new_guardian->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}elseif(request('family_parent') == 'Other'){

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'other_id' => $new_other->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}else{

    			family::create([

		    		'user_id' => auth()->user()->id,

		    		'currently_living_with' => request('family_parent'),

		    		'marital_status' => request('marital_status'),

		    		'children' => request('children_status'),

		    		'number_of_children' => request('number_of_children'),

		    		'number_of_siblings' => request('number_of_siblings')

		    	]);

    		}

	    }

	    else
	    
	    {

	    	$update_fam = $family->get_data();

	    	$update_fam->currently_living_with = request('family_parent');

	    	$update_fam->marital_status = request('marital_status');

	    	$update_fam->children = request('children_status');

	    	$update_fam->save();

	    }

	    return redirect()->back()->with('success', 'Family Saved Successfully');

    }

    public function update_child($id)
    {

    	$update_child = children::find($id);

    	$update_child->fullname = request('child_name_update');

    	$update_child->email = request('child_email_update');
    	
    	$update_child->phone_number = request('child_phone_update');

    	$update_child->save();

    	return redirect()->back()->with('success', request('child_name_update').' is updated Successfully');

    }

    public function destroy_child($id)
    {

    	$destroy_kid = children::find($id);

    	$destroy_kid->delete();

    	return redirect()->back()->with('success', 'Kid is deleted Successfully!');

    }

    public function update_sibling($id)
    {

    	$update_sibling = siblings::find($id);

    	$update_sibling->fullname = request('sibling_name_update');

    	$update_sibling->email = request('sibling_email_update');
    	
    	$update_sibling->phone_number = request('sibling_phone_update');

    	$update_sibling->save();

    	return redirect()->back()->with('success', request('sibling_name_update').' is updated Successfully');

    }

    public function destroy_sibling($id)
    {

    	$destroy_sibling = siblings::find($id);

    	$destroy_sibling->delete();

    	return redirect()->back()->with('success', 'Sibling is deleted Successfully!');

    }



}
