<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\step_quantities;

use App\university_membership;

class StepQuantityController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }


    public function index()
    {

    	if(auth()->user()->role_id != 2)
    	{
    		return view('/');
    	}
    	else
    	{
    		return view('main_layout.steps.create_add_steps');
    	}
    }

    public function store()
    {

        $university_membership = new step_quantities();

        $this->validate(request(),['add_steps'=>['required']]);



        $university = university_membership::where('user_id', auth()->user()->id);

        $university_id = $university->pluck('university_id')->get(0);

        $step_quantity = step_quantities::where('university_id', $university_id);

        if($step_quantity->count() == 0)
        {

            step_quantities::create([
            
                'step_quantity' => request('add_steps'),
            
                'university_id' => $university_membership->get_university_id()
            
            ]);

            return redirect()->back()->with('success', 'Step is Added Successfully');
            
        }

        else

        {

            $update_step = step_quantities::where('university_id', $university_id)->first();

            $update_step->step_quantity = request('add_steps');

            $update_step->save();

            return redirect()->back()->with('success', 'Step is updated successfully');

        }
    
    }
}
