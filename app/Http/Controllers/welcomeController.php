<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class welcomeController extends Controller
{

 	public function __construct(){

 		$this->middleware('guest', ['except'=>'index']);

 	}

 	public function index(){


 		return view('index-page.welcome-layouts.home');

 	}


}
