<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\award_status;

class loginController extends Controller
{


	protected $redirectAfterLogout = '/login';

	protected $redirectIfLoginSuccessful = '/dashboard';


	public function __construct(){

        $this->middleware('guest')->except('destroy');
	
	}


    public function index()
    {

		return view('index-page.login.login');
    
    }


    public function destroy()

    {

    	auth()->logout();

    	return redirect($this->redirectAfterLogout);
    
    }


    public function store()

    {
        

    	if(!auth()->attempt(request(['email', 'password']))){

    		return back()->withErrors([

    			'message' => 'Please Check Your credentials and Try again.'

    		]);
    	
    	}else{

            if(is_null(auth()->user()->token) && is_null(auth()->user()->firstname) && auth()->user()->activation_status == 1 || is_null(auth()->user()->token) && is_null(auth()->user()->middlename) && auth()->user()->activation_status == 1 || is_null(auth()->user()->token) && is_null(auth()->user()->lastname) && auth()->user()->activation_status == 1 || is_null(auth()->user()->token) && is_null(auth()->user()->phone_number) && auth()->user()->activation_status == 1){


                return redirect('/info');


            }elseif(!is_null(auth()->user()->token)){
                
                auth()->logout();

                return redirect('/login')->withErrors(['message' => 'Account is not verified!']);

            }elseif(auth()->user()->activation_status == 0){

                auth()->logout();

                return redirect('/login')->withErrors(['message' => 'Your Account is Deactivated!']);

            }else{

                $submission = award_status::where('user_id', auth()->user()->id)->count();

                if(is_null(auth()->user()->role_id))
                {
                    if($submission == 0)
                    {
                        return redirect('/my-info');
                    }
                    else
                    {
                        return redirect('/cannot-access');
                    }
                }
                elseif (auth()->user()->role_id == '3') 
                {
                    return redirect('/students');
                }
                else
                {
                    return redirect('/dashboard');
                }

            }

        }

    }

}
