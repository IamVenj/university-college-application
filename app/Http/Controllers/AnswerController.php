<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\step_quantities;

use App\university_membership;

use App\universities;

use App\Questions;

use App\education;

use App\answer_for_university_questions;

class AnswerController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

    	$university_membership = university_membership::where('user_id', auth()->user()->id)->get();

    	$university_array = array();

    	foreach ($university_membership as $member) {

    		$universities = universities::find($member->university_id);

    		array_push($university_array, $universities);

    	}

    	return view('main_layout.answer.answer', compact('university_array'));
    
    }

    public function show($id)
    {

    	$step_quantity = step_quantities::where('university_id', $id);

    	$user = User::find(auth()->user()->id);

        $university = universities::find($id);

    	return view('main_layout.answer.university', compact('step_quantity', 'user', 'id', 'university'));

    }

    public function show_step_one($university_id, $id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $answer = new answer_for_university_questions();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 1, $university_id, $member->course_id);

        return view('main_layout.answer.step_one_info', compact('user', 'show_question', 'university_id'));

    }

    public function show_step_two($university_id, $id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $answer = new answer_for_university_questions();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 2, $university_id, $member->course_id);

        return view('main_layout.answer.step_two_info', compact('user', 'show_question', 'university_id'));

    }

    public function show_step_three($university_id, $id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $answer = new answer_for_university_questions();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 3, $university_id, $member->course_id);

        return view('main_layout.answer.step_three_info', compact('user', 'show_question', 'university_id'));

    }


    public function show_step_four($university_id, $id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $answer = new answer_for_university_questions();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 4, $university_id, $member->course_id);

        return view('main_layout.answer.step_four_info', compact('user', 'show_question', 'university_id'));

    }
}
