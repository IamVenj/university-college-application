<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\universities;

use App\university_membership;

class AdminPanelController extends Controller
{

	public function __construct()
	{

		$this->middleware('auth');

	}

    public function index()
    {

        if(auth()->user()->role_id == 1)
        {

            $university_membership = new university_membership();

            $university = new universities();

        	$users = User::where('role_id', 2)->get();

            $user = new User();

        	$universities = universities::all();

        	if(auth()->user()->role_id != 1)
    		{

    			return redirect('/');

    		}
    		else
    		{

    			return view('main_layout.control-panel.admin_control', compact('users', 'universities'));

    		}
        }
        else
        {
            return view('main_layout.error.error-403');
        }
    }

    public function destroy($id)
   	{
    	
        $delete_this_user = User::find($id);

        $delete_this_user->delete();

        return redirect()->back()->with('success', 'User is Succussfully Deleted!');

    }

    public function deactivate($id)
    {

        $deactivate_this_user = User::find($id);

        $deactivate_this_user->activation_status = 0;

        $deactivate_this_user->save();

        return redirect()->back()->with('success', 'User is Succussfully Deactivated!');

    }

    public function password_reset($id)
    {

        $this->validate(request(),[

            'password_reset' => ['required', 'min:6']

        ]);


        $change_password_of_this_user = User::find($id);

        $change_password_of_this_user->password = bcrypt(request('password_reset'));

        $change_password_of_this_user->save();

        return redirect()->back()->with('success', 'Password reset Succussful');


    }

    public function activate($id)
    {

        $activate_this_user = User::find($id);

        $activate_this_user->activation_status = 1;

        $activate_this_user->save();

        return redirect()->back()->with('success', 'User is Succussfully Activated!');        

    }

    public function change_university($id)
    {

    	$this->validate(request(),['change_university'=>'required']);

        $uni_membership = new university_membership();

        if($uni_membership->check_if_university_has_an_admin(request('change_university')) == 0)

        {

        	$change_university_of_this_user = university_membership::where('user_id', $id)->first();

        	$change_university_of_this_user->university_id = request('change_university');

        	$change_university_of_this_user->save();

        }

        else

        {

            return redirect()->back()->withErrors('University already has an admin!');
        
        }

    	return redirect()->back()->with('success', 'University is Succussfully Changed!');

    }

 
}
