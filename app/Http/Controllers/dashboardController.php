<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\User;

use App\university_membership;

class dashboardController extends Controller
{


    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

        if(is_null(auth()->user()->role_id)){

            return redirect('/my-info');

        }

        elseif (auth()->user()->role_id == 3) 

        {
        
            return redirect('/students');
        
        }

        else

        {

            return view('main_layout.dashboard.dashboard');

        }

    }


    function getAllMonths()
    {

        $month_array = array();

        if(auth()->user()->role_id == 1){

            $universities = university_membership::where('role_id', null)->orderBy('created_at', 'ASC')->pluck('created_at');

            $universities_date = json_decode($universities);

        }elseif(auth()->user()->role_id == 2)
        {

            $member = new university_membership();

            $university_id = $member->get_university_id_of_this();

            $universities = university_membership::where('role_id', null)->where('university_id', $university_id)->orderBy('created_at', 'ASC')->pluck('created_at');

            $universities_date = json_decode($universities);            

        }

        if(!empty($universities_date)){

            foreach ($universities_date as $unformatted_date) {
                
                $date = new \DateTime($unformatted_date->date);

                $month_no = $date->format('m');

                $month_name = $date->format('M');

                $month_array[$month_no] = $month_name;

            }

        }

        return $month_array;

    }

    function getApplicantsCountPerMonth($month)
    {

        if(auth()->user()->role_id == 1){

            $appliers = university_membership::whereMonth('created_at', $month)->where('role_id', null)->groupBy('user_id')->get();

            $monthly_appliers = $appliers->count();

        }
        elseif(auth()->user()->role_id == 2)
        {

            $member = new university_membership();

            $university_id = $member->get_university_id_of_this();

            $monthly_appliers = university_membership::whereMonth('created_at', $month)->where('university_id', $university_id)->where('role_id', null)->count();         

        }

        
        return $monthly_appliers;

    }

    function getApplicantsDataPerMonth()
    {

        $monthly_appliers_array = array();

        $month_name_array = array();

        $month_array = $this->getAllMonths();

        if(!empty($month_array))
        {

            foreach ($month_array as $month_no => $month_name) {
                
                $monthly_appliers = $this->getApplicantsCountPerMonth($month_no);

                array_push($monthly_appliers_array, $monthly_appliers);

                array_push($month_name_array, $month_name);

            }

        }

        $max_no = max($monthly_appliers_array);

        $max = round(($max_no + 10/2) / 10) * 10;

        $monthly_applicant_data_array = [

            'months' => $month_name_array,

            'applicants' => $monthly_appliers_array,

            'max' => $max

        ];

        return $monthly_applicant_data_array;

    }

    function getGender()
    {

        if(auth()->user()->role_id == 1)
        {

            $maf = User::where('role_id', null)->where('sex', !empty(''))->groupBy('sex')->pluck('sex');

            $gender = json_decode($maf);

        }
        // elseif(auth()->user()->role_id == 2)
        // {

        //     $member = new university_membership();

        //     $university_id = $member->get_university_id_of_this();

        //     $university_membership = university_membership::where('role_id', null)->where('university_id', $university_id)->groupBy('user_id')->pluck('user_id');

        //     $gender = array();

        //     foreach ($university_membership as $members) {
               
        //         $maf = User::where('id', $members)->where('sex', !empty(''))->groupBy('sex')->pluck('sex');

        //         foreach ($maf as $g) {

        //             array_push($gender, $g);

        //         }

        //     }
        
        // }

        return $gender;

    }


    function MaleAndFemalePercentage($gender)
    {

        if(auth()->user()->role_id == 1)
        {

            $male_female = User::where('role_id', null)->where('sex', $gender)->count();

        }

        // elseif(auth()->user()->role_id == 2)
        // {

        //     $member = new university_membership();

        //     $university_id = $member->get_university_id_of_this();

        //     $university_membership = university_membership::where('role_id', null)->where('university_id', $university_id)->pluck('user_id');

        //     foreach ($university_membership as $members) {
               
        //         $male_female = User::where('id', $members)->where('sex', $gender)->count();

        //     }  

        // }

        $male_female_precentage = $male_female/100;

        return $male_female_precentage;

    }

    function MaleAndFemaleApplicantsComparison()
    {

        $gender = $this->getGender();

        $male_female_array = array();

        $male_female_precentage_array = array();
        
        if(!empty($gender))
        {

            foreach ($gender as $g) {
                
                $male_female_precentage = $this->MaleAndFemalePercentage($g);

                array_push($male_female_array, $g);

                array_push($male_female_precentage_array, $male_female_precentage);

            }

        }

        // dd($male_female_array);

        $gender_array = [

            'gender' =>  $male_female_array,

            'percentage' => $male_female_precentage_array

        ];

        return $gender_array;

    }


}
