<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\academics;

use App\university_membership;

use App\Questions;

use App\Check_and_Radio_Question_List;

use App\Courses;

class Create_Step_1_Controller extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function create()
    {

        if(auth()->user()->role_id == 2)
        {

        	$university = new Questions();

        	$university_id = $university->get_university_id();

        	$academics = academics::where('university_id', $university_id)->get();


        	$question_model = Questions::where('step', 1)->where('university_id', $university_id)->get();

        	return view('main_layout.steps.create_step_one', compact('academics', 'question_model'));

        }
        else
        {

            return view('main_layout.error.error-403');

        }

    }

    public function fetch(Request $request)
    {
        $select = $request->get('select');
        
        $value = $request->get('value');
        
        $dependent = $request->get('dependent');


        $data = Courses::where($select, $value)->groupBy($dependent)->get();
        
        $output = '<option selected disabled>Choose Course</option>';
        
        foreach ($data as $row) {
        
            $output .= '<option value="'.$row->id.'"">'.$row->$dependent.'</option>';
        
        }
        
        
        return $output;
    }

    public function store()
    {

    	$question = new Questions();

        $Check_and_Radio_Question_List_m = new Check_and_Radio_Question_List();

    	$university_id = $question->get_university_id();

    	$this->validate(request(),[
    	
    		'question' => 'required|max:255',
    	
    		'input_type' => 'required',
    	
    		'order' => 'required',
    	
    		'academics_id' => 'required',

            'course_id' => 'required'
    	
    	]);

        if(request('input_type') == 'dropdown' || request('input_type') == 'radio')
        {

            $answer_inputs = request('answer_inputs');



            $check_if_order_has_been_used = $question->check_order_usage(request('order'), request('academics_id'), 1, request('course_id'));

            $check_if_question_has_been_used = $question->check_question_usage(request('question'), request('academics_id'), 1, request('course_id'));
        

            if($check_if_question_has_been_used == 0)
            {

                if($check_if_order_has_been_used == 0)
                {
                    Questions::create([

                        'question' => request('question'),

                        'university_id' => $university_id,

                        'academics_id' => request('academics_id'),

                        'input_type' => request('input_type'),

                        'step' => 1,

                        'placeholder' => request('question_placeholder'),

                        'order' => request('order'),

                        'course_id' => request('course_id')

                    ]);

                    $Question_id = $Check_and_Radio_Question_List_m->get_Question_id(request('question'), $university_id, 1, request('order'));


                    foreach ($answer_inputs as $answer_input) {
                    
                        Check_and_Radio_Question_List::create([

                            'question_id' => $Question_id,

                            'input_type' => request('input_type'),

                            'quest_box' => $answer_input

                        ]);
                        
                    }

                    
                    return redirect()->back()->with('success', 'Question '.request('order').' is Successfully Created');
                }
                else
                {
                    return redirect()->back()->withErrors('Order '.request('order').' has already been Taken!');
                }

            }
            else
            {
                return redirect()->back()->withErrors('Question has already been Taken!');
            }

        }

    	else

        {

            $check_if_order_has_been_used = $question->check_order_usage(request('order'), request('academics_id'), 1, request('course_id'));

            $check_if_question_has_been_used = $question->check_question_usage(request('question'), request('academics_id'), 1, request('course_id'));



            if($check_if_question_has_been_used == 0)
            {

                if($check_if_order_has_been_used == 0)
                {
                    Questions::create([

                        'question' => request('question'),

                        'university_id' => $university_id,

                        'academics_id' => request('academics_id'),

                        'input_type' => request('input_type'),

                        'step' => 1,

                        'placeholder' => request('question_placeholder'),

                        'order' => request('order'),

                        'course_id' => request('course_id')

                    ]);

                    return redirect()->back()->with('success', 'Question '.request('order').' is Successfully Created');
                }
                else
                {
                    return redirect()->back()->withErrors('Order '.request('order').' has already been Taken!');
                }

            }
            else
            {
                return redirect()->back()->withErrors('Question has already been Taken!');
            }


        }

    	

    }

    public function update($id)
    {

    	$step_one_questions = Questions::find($id);

    	$step_one_questions->question = request('edit_question');

    	$step_one_questions->order = request('order');
    	
        $step_one_questions->placeholder = request('question_placeholder');

    	$step_one_questions->save();

    	return redirect()->back()->with('success', 'Question '.request('order').' is Updated Successfully');

    }

    public function destroy($id)
    {
    	$step_one_questions = Questions::find($id);

    	$step_one_questions->delete();

    	return redirect()->back()->with('success', 'Question is Deleted Successfully!');
    }
}
