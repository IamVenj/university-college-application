<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;

use App\User;


class VerifyController extends Controller
{

    public function __construct()
    {

    	$this->middleware('auth')->except('verify');

    }

	public function verify($token)
	{

        // dd('verified');

		User::where('token', $token)->firstOrFail()->update(['token'=>null, 'email_verified_at'=>now()]);

        return redirect('/login')->with("success", "You're account is Verified!");


	}

}
