<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\university_membership;

use App\award_status;

use App\Notifications\RejectionEmail;

class RejectedController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index()
    {

    	$university_membership = new university_membership();

    	$user = new User();

        $university_id = $university_membership->get_university_id_of_this();

        $users = $user->get_users_for_panel($university_id, null);


    	return view('main_layout.applicant.rejected', compact('users', 'university_id'));

    }

    public function reject($id)
    {
        $user_m = new User();

        $check_for_connection = $user_m->isConnected();

        if($check_for_connection == true)
        {
            $university_membership = new university_membership();

            $university_id = $university_membership->get_university_id_of_this();

        	$update_status = award_status::where('user_id', $id)->where('university_id', $university_id)->first();

        	$update_status->award = 'rejected';

        	$update_status->save();

            $user = User::findOrFail($id);

            $user->notify(new RejectionEmail($user));

        	return redirect('/rejected')->with('success', 'Applicant is rejected');

        }
        else
        {
            return redirect()->back()->withErrors('Please Check Your Internet Connection!');
        }

    }

    public function unreject($id)
    {
        $university_membership = new university_membership();

        $university_id = $university_membership->get_university_id_of_this();

    	$update_status = award_status::where('user_id', $id)->where('university_id', $university_id)->first();

    	$update_status->award = null;

    	$update_status->save();

    	return redirect()->back()->with('success', 'Applicant is Unrejected');

    }

}
