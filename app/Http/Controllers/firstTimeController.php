<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\education;
use App\university_membership;
use Carbon\Carbon;
use App\universities_applied;
use App\Courses;
use Illuminate\Support\Facades\DB;

class firstTimeController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (is_null(auth()->user()->firstname) && is_null(auth()->user()->middlename) && is_null(auth()->user()->lastname)) {
            $user = new User();
            $date = $user->changeDateFormat(auth()->user()->date_of_birth);
            $universities = DB::table('universities')->groupBy('university_name')->get();
            return view('main_layout/first_Time_Login/first_time_login', compact('universities', 'date'));
        } else {
            if (is_null(auth()->user()->role_id)) {
                return redirect('/my-info');
            } elseif (auth()->user()->role_id == '3') {
                return redirect('/students');
            } else {
                return redirect('/dashboard');
            }
        }
    }

    public function fetch(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $table = $request->get('dbtable');
        $academics = $request->get('academics');
        $course = $request->get('course');
        $courses = $request->get('courses');

        if ($academics == 'academics_name') {
            $data = DB::table($table)->where($select, $value)->groupBy($academics)->get();
            $output = '<option selected disabled>Select Academics</option>';
            foreach ($data as $row) {
                $output .= '<option value="' . $row->id . '"">' . $row->$academics . '</option>';
            }
        } elseif ($courses == 'course_name_') {
            $data = Courses::where('id', $value)->first();
            $now_timestamp = strtotime(date('Y-m-d'));
            if ($now_timestamp <= strtotime($data->start_date) || $now_timestamp >= strtotime($data->end_date)) {
                $output = '<p class="lead ad" style="color:red;">The admission date has Passed!</p>';
            }
        } else {
            $data = DB::table($table)->where($select, $value)->groupBy($course)->get();
            $output = '<option selected disabled>Select Field</option>';
            foreach ($data as $row) {
                $output .= '<option value="' . $row->id . '"">' . $row->$course . '</option>';
            }
        }
        return $output;
    }

    public function edit($id)
    {
        $user_id = User::find($id);
        return view('main_layout/first_Time_Login/first_time_login', compact('user_id'));
    }

    public function update($id)
    {
        if (isset($_POST['finish'])) {
            if (is_null(auth()->user()->role_id)) {
                $date_check = Courses::where('id', request('course_id'))->first();
                $now_timestamp = strtotime(date('Y-m-d'));
                if ($now_timestamp <= strtotime($date_check->start_date) || $now_timestamp >= strtotime($date_check->end_date)) {
                    if ($now_timestamp <= strtotime($date_check->start_date)) {
                        return redirect()->back()->withErrors('The Admission date for this field has not begun yet!');
                    } elseif ($now_timestamp >= strtotime($date_check->end_date)) {
                        return redirect()->back()->withErrors('The Admission date for this field has passed!');
                    }
                } else {
                    $update_user_information = User::find($id);
                    $update_user_information->firstname = request('firstname');
                    $update_user_information->middlename = request('middlename');
                    $update_user_information->lastname = request('lastname');
                    $update_user_information->email = request('email');
                    $update_user_information->date_of_birth = Carbon::createFromFormat('m/d/Y', request('date_of_birth'))->format('Y-m-d');
                    $update_user_information->country_code = request('country_code');
                    $update_user_information->phone_number = request('phone_number');
                    $update_user_information->address_line = request('address_line');
                    $update_user_information->city = request('city');
                    $update_user_information->subcity = request('subcity');
                    $update_user_information->woreda = request('woreda');
                    $update_user_information->house_number = request('house_number');
                    $update_user_information->sex = request('sex');
                    $update_user_information->academics_id = request('academics_id');
                    $update_user_information->certificate = request('certificate_to_see_if_chosen');
                    $update_user_information->university_id = request('university');
                    $update_user_information->save();
                    education::create([
                        'user_id' => auth()->user()->id,
                        'academics_id' => request('academics_id'),
                        'course_id' => request('course_id')
                    ]);
                    university_membership::create([
                        'user_id' => auth()->user()->id,
                        'university_id' => request('university'),
                        'academics_id' => request('academics_id'),
                        'course_id' => request('course_id')
                    ]);
                    universities_applied::create([
                        'user_id' => auth()->user()->id,
                        'university_id' => request('university'),
                        'course_id' => request('course_id')
                    ]);
                }
            } else {
                $update_user_information = User::find($id);
                $update_user_information->firstname = request('firstname');
                $update_user_information->middlename = request('middlename');
                $update_user_information->lastname = request('lastname');
                $update_user_information->email = request('email');
                $update_user_information->date_of_birth = Carbon::createFromFormat('m/d/Y', request('date_of_birth'))->format('Y-m-d');
                $update_user_information->country_code = request('country_code');
                $update_user_information->phone_number = request('phone_number');
                $update_user_information->address_line = request('address_line');
                $update_user_information->city = request('city');
                $update_user_information->subcity = request('subcity');
                $update_user_information->woreda = request('woreda');
                $update_user_information->house_number = request('house_number');
                $update_user_information->sex = request('sex');

                $update_user_information->save();
            }
            if (is_null(auth()->user()->role_id)) {
                return redirect('/my-info');
            } elseif (auth()->user()->role_id == '3') {
                return redirect('/students');
            } else {
                return redirect('/dashboard');
            }
        }
    }
}
