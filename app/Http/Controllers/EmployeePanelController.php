<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\university_membership;

class EmployeePanelController extends Controller
{
    public function __construct()
    {

    	$this->middleware('auth');

    }

    public function index()
    {

        $university_membership = new university_membership();

        $user = new User();

        $university_id = $university_membership->get_university_id_of_this();

        $users = $user->get_users_for_panel($university_id, 3);


    	if(auth()->user()->role_id != 2)
    	{

    		return view('main_layout.error.error-403');

    	}else{

	    	return view('main_layout.control-panel.employee_control', compact('users'));

    	}


    }

   	public function destroy($id)
   	{
    	
        $delete_this_user = User::find($id);

        $delete_this_user->delete();

        return redirect()->back()->with('success', 'User is Succussfully Deleted!');

    }

    public function deactivate($id)
    {

        $deactivate_this_user = User::find($id);

        $deactivate_this_user->activation_status = 0;

        $deactivate_this_user->save();

        return redirect()->back()->with('success', 'User is Succussfully Deactivated!');

    }

    public function password_reset($id)
    {

        $this->validate(request(),[

            'password_reset' => ['required', 'min:6']

        ]);


        $change_password_of_this_user = User::find($id);

        $change_password_of_this_user->password = bcrypt(request('password_reset'));

        $change_password_of_this_user->save();

        return redirect()->back()->with('success', 'Password reset Succussful');


    }

    public function activate($id)
    {

        $activate_this_user = User::find($id);

        $activate_this_user->activation_status = 1;

        $activate_this_user->save();

        return redirect()->back()->with('success', 'User is Succussfully Activated!');        

    }


}
