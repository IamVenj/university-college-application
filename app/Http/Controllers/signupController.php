<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Support\Facades\Hash;

use App\universities;


class signupController extends Controller
{


    public function __construct()

    {

        $this->middleware('guest');

    }

    public function index()

    {

    	return view('index-page.register.signup');
    
    }


    public function store()
    
    {

        $user_m = new User();

        $check_for_connection = $user_m->isConnected();


    	$this->validate(request(), [

    		'email' => ['required', 'email', 'max:255', 'unique:users'],

    		'password' => ['required', 'confirmed', 'min:6']

    	]);



    	$hashed_password = bcrypt(request('password'));

    	$email = request('email');


        if($check_for_connection == true){

        	$user = User::create([

                'email' => $email,

                'password' => $hashed_password,

                'token' => str_random(25)

            ]);

            $user->sendVerificationEmail();


            return redirect('/login')->with('success', 'Email has Been Sent Please Verify Your Email Account!');

        }else{

            return redirect('/signup')->withErrors('Please Check Your Internet Connection!');

        }

    }
}
