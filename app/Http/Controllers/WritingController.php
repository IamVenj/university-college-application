<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\writing;

use App\award_status;

class WritingController extends Controller
{
    public function __construct()
    {

    	$this->middleware('auth');

    }

    public function index()
    {
        $submission = award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

        if($submission == 0)
        {

        	$writing = new writing();

        	$writings = $writing->get_writing();

        	$count = $writing->count();

        	return view('main_layout.Main-Questions.writing', compact('writings', 'count'));
        }
        else
        {
            return redirect('/cannot-access');
        }

    }


    public function store($id)
    {

    	$writing = new writing();

    	$disciplinary_status = request('disciplinary_status');

    	$crime_status = request('crime_status');

    	$violation_description = request('violation_description');

    	$crime_description = request('crime_description');

    	$essay = request('essay');

    	$this->validate(request(), [

    		'disciplinary_status' => 'required',

    		'crime_status' => 'required',

    		'essay' => 'required'

    	]);

    	if($disciplinary_status == 'no' && $crime_status == 'no')
    	{
    		if($writing->check_if_data_exists($id) > 0)
    		{

    			$save_writing = $writing->get_data($id);

    			$save_writing->disciplinary_violation_description = null;

    			$save_writing->disciplinary_violation = $disciplinary_status;

    			$save_writing->conviction = $crime_status;

    			$save_writing->conviction_description = null;

    			$save_writing->save();

    		}
    	}
    	elseif($disciplinary_status == 'no')
    	{
    		if($writing->check_if_data_exists($id) > 0){

    			$save_writing = $writing->get_data($id);

    			$save_writing->disciplinary_violation = $disciplinary_status;

    			$save_writing->disciplinary_violation_description = null;

    			$save_writing->save();

    		}
    	}
    	elseif($crime_status == 'no')
    	{
    		if($writing->check_if_data_exists($id) > 0)
    		{

    			$save_writing = $writing->get_data($id);

    			$save_writing->conviction = $crime_status;

    			$save_writing->conviction_description = null;

    			$save_writing->save();

    		}
    	}

    	if($disciplinary_status == 'yes' && $crime_status == 'yes')
    	{

    		$this->validate(request(), [

    			'violation_description' => 'required',

    			'crime_description' => 'required'

    		]);

    		if($writing->check_if_data_exists($id) == 0){

	    		writing::create([

	    			'user_id' => $id,

	    			'disciplinary_violation' => $disciplinary_status,

	    			'disciplinary_violation_description' => $violation_description,

	    			'conviction' => $crime_status,

	    			'conviction_description' => $crime_description,

	    			'essay' => $essay

	    		]);

    		}
    		else
    		{

    			$save_writing = $writing->get_data($id);

    			$save_writing->disciplinary_violation = $disciplinary_status;

    			$save_writing->disciplinary_violation_description = $violation_description;

    			$save_writing->conviction = $crime_status;

    			$save_writing->conviction_description = $crime_description;

    			$save_writing->essay = $essay;

    			$save_writing->save();

    		}

    	}
    	elseif($disciplinary_status == 'yes')
    	{

    		$this->validate(request(), [

    			'violation_description' => 'required'

    		]);

    		if($writing->check_if_data_exists($id) == 0){

	    		writing::create([

	    			'user_id' => $id,

	    			'disciplinary_violation' => $disciplinary_status,

	    			'disciplinary_violation_description' => $violation_description,

	    			'essay' => $essay

	    		]);

    		}
    		else
    		{

    			$save_writing = $writing->get_data($id);

    			$save_writing->disciplinary_violation = $disciplinary_status;

    			$save_writing->disciplinary_violation_description = $violation_description;

    			$save_writing->essay = $essay;

    			$save_writing->save();

    		}

    	}
    	elseif($crime_status == 'yes')
    	{

    		$this->validate(request(), [

    			'crime_description' => 'required'

    		]);

    		if($writing->check_if_data_exists($id) == 0){

	    		writing::create([

	    			'user_id' => $id,

	    			'conviction' => $crime_status,

	    			'conviction_description' => $crime_description,

	    			'essay' => $essay

	    		]);

	    	}
	    	else
	    	{

	    		$save_writing = $writing->get_data($id);

    			$save_writing->conviction = $crime_status;

    			$save_writing->conviction_description = $crime_description;

    			$save_writing->essay = $essay;

    			$save_writing->save();

	    	}

    	}
    	else{

    		if($writing->check_if_data_exists($id) == 0){

	    		writing::create([

	    			'user_id' => $id,

	    			'conviction' => $crime_status,

	    			'disciplinary_violation' => $disciplinary_status,

	    			'essay' => $essay

	    		]);

    		}
    		else
	    	{

	    		$save_writing = $writing->get_data($id);

    			$save_writing->conviction = $crime_status;

    			$save_writing->disciplinary_violation = $disciplinary_status;

    			$save_writing->essay = $essay;

    			$save_writing->save();

	    	}

    	}

    	return redirect()->back()->with('success', 'Writing Saved Successfully');

    }

}
