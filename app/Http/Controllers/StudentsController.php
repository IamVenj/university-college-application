<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\university_membership;

use App\education;

use App\previous_education;

use App\highschool;

use App\community_based_organization;

use App\honors;

use App\family;

use App\children;

use App\siblings;

use App\future_plans;

use App\activities;

use App\writing;

use App\Questions;

use App\answer_for_university_questions;

use App\document_upload;

use Response;

use App\award_status;

class StudentsController extends Controller
{
	public function __construct()
    {

        $this->middleware('auth');

    }
    public function index()
    {


    	$university_membership = new university_membership();

    	$user = new User();

        $university_id = $university_membership->get_university_id_of_this();

        $users = $user->get_users_for_panel($university_id, null);


    	return view('main_layout.students.students', compact('users', 'university_id'));

    }


    public function show($id)
    {
    	
        $user = User::find($id);

        $document = new document_upload();

        $doc = $document->getDoc($id);

    	return view('main_layout.students.details', compact('user', 'doc'));

    }

    public function show_education($id)
    {

        $user = User::find($id);

        $education = education::where('user_id', $id)->first();

        $previous_education = previous_education::where('user_id', $id)->first();

        $highschool = highschool::where('user_id', $id)->get();

        $cbo = community_based_organization::where('user_id', $id)->get();

        $honor = honors::where('user_id', $id)->get();

        $future_plans = future_plans::where('user_id', $id)->get();

        return view('main_layout.students.education_info', compact('user', 'education', 'previous_education', 'highschool', 'cbo', 'honor', 'future_plans'));

    }

    public function show_fam($id)
    {

        $user = User::find($id);

        $education = education::where('user_id', $id)->first();

        $fam = family::where('user_id', $id)->first();

        $children = children::where('user_id', $id)->get();

        $siblings = siblings::where('user_id', $id)->get();

        return view('main_layout.students.fam_info', compact('user', 'education', 'fam', 'children', 'siblings'));

    }

    public function show_activity($id)
    {

        $user = User::find($id);

        $activities = activities::where('user_id', $id)->get();

        return view('main_layout.students.activity_info', compact('user', 'activities'));

    }

    public function show_writing($id)
    {

        $user = User::find($id);

        $writings = writing::where('user_id', $id)->first();

        return view('main_layout.students.writing_info', compact('user', 'writings'));

    }

    public function show_step_one($id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $university_membership = new university_membership();

        $education = new education();

        $answer = new answer_for_university_questions();

        $university_id = $university_membership->get_university_id_of_this();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 1, $university_id, $member->course_id);

        // dd($member->academics_id);

        

        return view('main_layout.students.step_one_info', compact('user', 'show_question'));

    }

    public function show_step_two($id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $university_membership = new university_membership();

        $education = new education();

        $answer = new answer_for_university_questions();

        $university_id = $university_membership->get_university_id_of_this();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 2, $university_id, $member->course_id);


        return view('main_layout.students.step_two_info', compact('user', 'show_question'));

    }

    public function show_step_three($id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $university_membership = new university_membership();

        $education = new education();

        $answer = new answer_for_university_questions();

        $university_id = $university_membership->get_university_id_of_this();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 3, $university_id, $member->course_id);


        return view('main_layout.students.step_three_info', compact('user', 'show_question'));

    }


    public function show_step_four($id)
    {

        $user = User::find($id);

        $Question = new Questions();

        $university_membership = new university_membership();

        $education = new education();

        $answer = new answer_for_university_questions();

        $university_id = $university_membership->get_university_id_of_this();

        $member = university_membership::where('user_id', $id)->where('university_id', $university_id)->first();

        $show_question = $Question->show_question($member->academics_id, 4, $university_id, $member->course_id);


        return view('main_layout.students.step_four_info', compact('user', 'show_question'));

    }

    public function download_doc($document)
    {

        $file_path = public_path()."/uploads/documents/".$document;

        if(file_exists($file_path))
        {

            return Response::download($file_path, $document, [

                'Content-Type: application/pdf',

                'Content-Length: '.filesize($file_path)

            ]);

        }
        else
        {

            return redirect()->back()->withErrors('Requested File Does not Exist on our server!');

        }

    }

}
