<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\university_membership;

use App\information_posts;

class InformationPostsController extends Controller
{

	public function __construct()
	{

		$this->middleware('auth');

	}

    public function create()
    {

        $post = new information_posts();

        $university_membership = new university_membership();

        $university_id = $university_membership->get_university_id_of_this();

        $info_post = $post->get_posts($university_id);

    	return view('main_layout.post.create_post', compact('info_post'));

    }

    public function store()
    {

        $university_membership = new university_membership();

        $this->validate(request(), [

            'title' => ['required', 'unique:information__posts'],

            'info_type' => 'required',

            'post_description' => 'required'

        ]);


        $info_type = request('info_type');

        $post_title = request('title');

        $post_description = request('post_description');

        $university_id = $university_membership->get_university_id_of_this();

        

        information_posts::create([

            'title' => $post_title,

            'university_id' => $university_id,

            'description' => $post_description,

            'info_type' => $info_type

        ]);




        return redirect()->back()->with('success', 'Information is posted Successfully');
    	
    }

    public function update($id)
    {

        $post = information_posts::find($id);

        $post->title = request('title_update');

        $post->info_type = request('info_type_update');
        
        $post->description = request('post_description_update');

        $post->save();

        return redirect()->back()->with('success', 'Information is Successfully Updated');

    }

    public function destroy($id)
    {

        $post = information_posts::find($id);

        $post->delete();

        return redirect()->back()->with('success', 'Information is Successfully Deleted');

    }

}
