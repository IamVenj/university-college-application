<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\document_upload;

use File;

use App\award_status;

class UploadDocController extends Controller
{

	public function __construct()
	{

		$this->middleware('auth');

	}

    public function index()
    {
        $submission = award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

        if($submission == 0)
        {

        	$document = new document_upload();

        	$docc = $document->getDoc(auth()->user()->id);

        	$count = $document->check_if_user_exists(auth()->user()->id);

        	return view('main_layout.upload_document.create_upload', compact('docc', 'count'));

        }
        else
        {
            return redirect('/cannot-access');
        }

    }

    public function store()
    {

    	$upload = new document_upload();

    	$this->validate(request(), [

    		'upload_document' => 'required|mimes:pdf|max:5000',

    		'document_type' => 'required'


    	]);

    	if(request('document_type') == 'h_transcript')
    	{

    		$document = auth()->user()->firstname.'_'.'transcript'.'.'.request('upload_document')->getClientOriginalExtension();

    	}

    	if(request('document_type') == 'n_e_doc')
    	{

    		$document = auth()->user()->firstname.'_'.'NationalExamResult'.'.'.request('upload_document')->getClientOriginalExtension();

    	}

    	if(request('document_type') == 'degree')
    	{

    		$document = auth()->user()->firstname.'_'.'Certificate'.'.'.request('upload_document')->getClientOriginalExtension();

    	}   

    	if($upload->check_if_document_exists(auth()->user()->id, $document) == 0)
    	{ 	

	    	document_upload::create([

	    		'document' => $document,

	    		'document_type' => request('document_type'),

	    		'user_id' => auth()->user()->id

	    	]);

	    	request('upload_document')->move(public_path("uploads/documents"), $document);

	    	return redirect()->back()->with('success', 'File Is Uploaded Successfully');

	    }
	    else
	    {

	    	return redirect()->back()->withErrors($document.' already exists!');

	    }

    }


    public function destroy($id)
    {

    	$destroy_document = document_upload::find($id);

    	File::delete(public_path("uploads/documents/".$destroy_document->document));

    	$destroy_document->delete();

    	return redirect()->back()->with('success', 'Document is Successfully Deleted!');

    }


}
