<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\university_membership;

class UniversitiesController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }
    public function create()
    {

        $universities = universities::all();

    	return view('main_layout.Universities.create_universities', compact('universities'));

    }
    
    public function store()
    {
    	$this->validate(request(), [

    		'university_name' => ['required', 'string', 'max:255', 'unique:universities'],

    	]);


    	universities::create([

    		'university_name' => request('university_name')

    	]);

    	return redirect()->back()->with('success','Successfully Created');

    }

    public function update($id)

    {

        $update_university = universities::find($id);

        $update_university->university_name = request('university_name');

        $update_university->save();

        return redirect()->back()->with('success', 'University is Updated Successfully');

    }

    public function destroy($id)
    {

        $delete_university_from_universities = universities::find($id);

        $delete_university_from_university_membership_count = university_membership::where('university_id', $id)->count();

        if($delete_university_from_university_membership_count > 0)
        {

            $delete_university_from_university_membership = university_membership::where('university_id', $id);

            $delete_university_from_university_membership->delete();            
        
        }

        $delete_university_from_universities->delete();



        return redirect()->back()->with('success', 'University is Deleted Successfully!');

    }

}
