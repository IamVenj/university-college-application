<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\User;

use App\university_membership;

class CreateEmployeeController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }
    
    public function create()
    {
        if(auth()->user()->role_id == 2)
        {

        	$uni_membership = new university_membership();

        	$universities = universities::all();

            $users = new User();

            $university_id = $uni_membership->get_university_id_of_this();
        
            $user = $users->get_users_for_panel($university_id, 3);


        	return view('main_layout.user.create_employee', compact('universities', 'user'));

        }
        else
        {

            return view('main_layout.error.error-403');

        }
    }


    public function store()
    {

    	$uni_membership = new university_membership();

    	$this->validate(request(),[

    		'email' => ['required', 'email', 'max:255', 'unique:users'],

    		'password' => ['required', 'min:6']

    	]);

    	User::create([

    		'email' => request('email'),

    		'password' => bcrypt(request('password')),

    		'role_id' => 3

    	]);


    	if($uni_membership->check_created_user(request('email')) > 0){

    		university_membership::create([

    			'user_id' => $uni_membership->get_created_user(request('email'))->first()->id,

    			'university_id' => $uni_membership->get_university_id_of_this(),

    			'role_id' => 3

    		]);

    	}

    	return redirect()->back()->with('success', 'Employee Account Successfully Created');


    }

    public function update($id)

    {

        $this->validate(request(),['email'=>'required|unique:users|email']);

        $update_employee = User::find($id);

        $update_employee->email = request('email');

        $update_employee->save();

        return redirect()->back()->with('success', 'Employee is updated Successfully');

    }


    public function destroy($id)

    {


        $delete_employee_from_university_membership = university_membership::where('user_id', $id);

        $delete_employee_from_university_membership->delete();
        
        $delete_employee_from_user = User::find($id);   

        $delete_employee_from_user->delete();

        return redirect()->back()->with('success', 'Account is Deleted Successfully');

    }

}
