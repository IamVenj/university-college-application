<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\activities;

use DB;

class ActivityController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

    	if(is_null(auth()->user()->role_id))
    	{

	    	$activity = new activities();

	    	$activities = $activity->get_all_activities();

	    	$count = $activity->check_data();

	    	return view('main_layout.Main-Questions.activity', compact('activities' , 'count'));

    	}
	    else
	    {
	    	return view('main_layout.error.error-403');
	    }
    }

    public function store($id)
    {

    	$activity = new activities();

    	$number_of_activities = request('number_of_activities');

    	$_number_of_activities = request('number_of_activities') + 1;

    	$activity_name = request('activity_name');

    	$activity_description = request('activity_description');

    	$activity_hours_spent = request('activity_hours_spent');

    	// dd($activity_name.' + '.$activity_description.' + '.$activity_hours_spent);

    	if(request('number_of_activities') != 0)
    	{

	    	$this->validate(request(), [

	    		'activity_name' => 'required',

	    		'activity_description' => 'required',

	    		'activity_hours_spent' => 'required'

	    	]);

	    	if($activity->check_if_data_exists($id) == 0)
	    	{

	    		$this->validate(request(), [

	    			'number_of_activities' => 'required'

	    		]);

		    	foreach ($activity_name as $key => $value) {
		    		
			    	activities::create([

			    		'user_id' => $id,

			    		'activity_name' => $activity_name[$key],

			    		'number_of_activities' => $number_of_activities,

			    		'description' => $activity_description[$key],

			    		'hours_spent_per_week' => $activity_hours_spent[$key]

			    	]);

			    }

			}
			else
			{

				 activities::create([

			    	'user_id' => $id,

		    		'activity_name' => $activity_name,

		    		'description' => $activity_description,

		    		'hours_spent_per_week' => $activity_hours_spent

			    ]);

			}

		}
		else
		{

			if($activity->check_if_data_exists($id) == 0)
	    	{

		    	activities::create([

		    		'user_id' => $id,

		    		'number_of_activities' => $number_of_activities,

		    	]);

			}
			else
			{

				$this->validate(request(), [

		    		'activity_name' => 'required',

		    		'activity_description' => 'required',

		    		'activity_hours_spent' => 'required'

		    	]);

				activities::create([

			    	'user_id' => $id,

		    		'activity_name' => $activity_name,

		    		'description' => $activity_description,

		    		'hours_spent_per_week' => $activity_hours_spent

			    ]);

			}

		}

		return redirect()->back()->with('success', 'Activity is Saved Successfully');

    }

    public function update($id)
    {

    	$update_activity = activities::find($id);

    	$update_activity->activity_name = request('activity_name_update');
    	
    	$update_activity->description = request('activity_description_update');
    	
    	$update_activity->hours_spent_per_week = request('activity_hour_update');

    	$update_activity->save();

    	return redirect()->back()->with('success', 'Activity '.request('activity_name_update').' is updated Successfully');

    }


    public function destroy($id)

    {

    	$delete_activity = activities::find($id);

    	$delete_activity->delete();

    	return redirect()->back()->with('success', 'Activity Deleted Successfully!');

    }

}
