<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\universities;

use App\education;

use App\previous_education;

use App\User;

use Carbon\Carbon;

use App\highschool;

use App\honors;

use App\community_based_organization;

use App\future_plans;

use App\award_status;

use DB;

class EducationController extends Controller
{

	public function __construct()
    {

        $this->middleware('auth');

    }
	
    public function index()
    {

        if(is_null(auth()->user()->role_id))
        {

            $submission = award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

            if($submission == 0)
            {

                $previous_education = new previous_education();

                $user = new User();

                $education = new education();

                $highschool_ = new highschool();

                $cbo = new community_based_organization();

                $honor = new honors();

                $date = null;

                if($previous_education->check_if_user_exists() > 0){

                    $date_from_pe = $previous_education->get_date();

                    $date = $user->changeDateFormat($date_from_pe);

                }

                $ed = $education->get_all_own_education();

                $count = $education->count();

                $count_hs = $highschool_->check_if_data_exists(auth()->user()->id);

                $count_cbo = $cbo->check_if_data_exists_cbo(auth()->user()->id);

                $count_honor = $honor->check_if_data_exists(auth()->user()->id);

            	$universities = universities::all();

            	return view('main_layout.Main-Questions.education', compact('universities', 'date', 'ed', 'count', 'count_hs', 'count_cbo', 'count_honor'));
            }
            else
            {
                return redirect('/cannot-access');
            }
        }
        else
        {

            return view('main_layout.error.error-403');

        }

    }

    public function fetch(Request $request)
    {
        $select = $request->get('select');
        
        $value = $request->get('value');
        
        $dependent = $request->get('dependent');

        $data = DB::table('academics')->where($select, $value)->groupBy($dependent)->get();

        $previous_education = previous_education::where('user_id', auth()->user()->id)->first();
    
        $output = '<option selected disabled>Choose Certificate</option>';
        
        foreach ($data as $row) {
        
            $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
        
        }
        
        
        return $output;
    }


    public function update($id){

        $previous_education = new previous_education();

        $cbo = new community_based_organization();

        $highschool = new highschool();

        $_honor_ = new honors();

        $future_plans_m = new future_plans();

        $ed = new education();



        

        $_future_plans_ = future_plans::where('user_id', $id)->first();

        if($ed->count_this() == 0)
        {

            $this->validate(request(),[

                'number_of_CBO' => 'required',

                'num_of_honor' => 'required'

            ]);

        }

        if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')
        {

            if($ed->count_this() == 0 && education::where('user_id', $id)->where('number_of_highschools', empty(''))->count() > 0)
            {

                $this->validate(request(), [

                    'num_high_schools' => 'required',

                ]);

            }

            $this->validate(request(),[

                'certificate_status' => 'required',

                'cgpa' => 'required',

                'national_exam_result' => 'required',

                'future_plans_description' => 'required',

                'highest_degree_intended_to_be_earned' =>'required'

            ]);

        }
        else
        {

            $this->validate(request(),[

                'certificate_status' => 'required',

                'cgpa' => 'required',

                'future_plans_description' => 'required',

                'highest_degree_intended_to_be_earned' =>'required'

            ]);

        }

        

        ##########################  previous education   #########################


        $prev_ed = previous_education::where('user_id', $id)->first();

        if(request('certificate_status') == 'yes'){

            $this->validate(request(), [

                'id_of_university_certificate' => 'required',

                'name_of_certificate' => 'required',

                'date_obtain_certificate' => 'required',

            ]);

            if($previous_education->check_if_user_exists() == 0){

                previous_education::create([

                    'user_id' => auth()->user()->id,

                    'university_id' => request('id_of_university_certificate'),

                    'certificate_name' => request('name_of_certificate'),

                    'date_obtain' => Carbon::createFromFormat('m/d/Y', request('date_obtain_certificate'))->format('Y-m-d')

                ]);

            }
            else
            {

                $prev_ed->university_id = request('id_of_university_certificate');

                $prev_ed->certificate_name = request('name_of_certificate');

                $prev_ed->date_obtain = Carbon::createFromFormat('m/d/Y', request('date_obtain_certificate'))->format('Y-m-d');

                $prev_ed->save();

            }

        }



        ###################################### highschool #########################################

        if(auth()->user()->certificate == 'Bachelor' || auth()->user()->certificate == 'Associate')
        {

            $highschool_name = request('name_of_highschool');

            $highschool_email = request('email_of_highschool');

            $highschool_address = request('address_of_highschool');


            if(!empty($highschool_name)){


                $this->validate(request(), [

                    'name_of_highschool' => 'required',

                    'email_of_highschool' => 'required',

                    'address_of_highschool' => 'required',

                ]);

                if($highschool->check_if_data_exists($id) > 0)
                {

                    if($highschool->check_if_email_exists($highschool_email, $id) == 0){

                        highschool::create([

                            'user_id' => auth()->user()->id,

                            'name' => $highschool_name,

                            'email' => $highschool_email,

                            'address' => $highschool_address

                        ]);
                        
                    }

                    else
                    {

                        return redirect()->back()->withErrors('Duplicate Email Entry');

                    }

                }
                else
                {

                    if(is_array($highschool_name))
                    {

                        foreach ($highschool_name as $key => $value) {

                            highschool::create([

                                'user_id' => auth()->user()->id,

                                'name' => $highschool_name[$key],

                                'email' => $highschool_email[$key],

                                'address' => $highschool_address[$key]

                            ]);

                        }

                    }
                    else
                    {

                        highschool::create([

                            'user_id' => auth()->user()->id,

                            'name' => $highschool_name,

                            'email' => $highschool_email,

                            'address' => $highschool_address

                        ]);

                    }

                }

            }

        }

        ############################# Community Based Organization ############################

        $cbo_name = request('CBO');
        
        $counselor_fullname = request('counselor_fullname');
        
        $counselor_prefix = request('counselor_prefix');
        
        $counselor_email = request('counselor_email');

        if(!empty($cbo_name))
        {

            $this->validate(request(), [

                'CBO' => 'required',

                'counselor_prefix' => 'required',

                'counselor_fullname' => 'required',

                'counselor_email' => 'required'

            ]);


            if($cbo->check_if_data_exists_cbo($id) == 0){

                foreach ($cbo_name as $key => $value) {

                    community_based_organization::create([

                        'user_id' => auth()->user()->id,

                        'counselor_prefix' => $counselor_prefix[$key],

                        'fullname' => $counselor_fullname[$key],

                        'email' => $counselor_email[$key],

                        'cbo_name' => $cbo_name[$key]

                    ]);

                }

            }
            else
            {

                community_based_organization::create([

                    'user_id' => auth()->user()->id,

                    'counselor_prefix' => $counselor_prefix,

                    'fullname' => $counselor_fullname,

                    'email' => $counselor_email,

                    'cbo_name' => $cbo_name

                ]);
                
            }

        }

        ################################ honor ############################

        $honor_name = request('honor_name');

        $honor_grade_level = request('honor_grade_level');

        $honor_level_of_recognition = request('honor_level_of_recognition');

        if(!empty($honor_name))
        {

            $this->validate(request(), [

                'honor_name' => 'required',

                'honor_grade_level' => 'required',

                'honor_level_of_recognition' => 'required',

            ]);


            if($_honor_->check_if_data_exists($id) == 0)
            {

                foreach ($honor_name as $key => $value) {
                    
                    honors::create([

                        'user_id' => auth()->user()->id,

                        'name' => $honor_name[$key],

                        'grade_level' => $honor_grade_level[$key],

                        'level_of_recognition' => $honor_level_of_recognition[$key]

                    ]);

                }

            }
            else
            {

                honors::create([

                    'user_id' => auth()->user()->id,

                    'name' => $honor_name,

                    'grade_level' => $honor_grade_level,

                    'level_of_recognition' => $honor_level_of_recognition

                ]);

            }

        }

        ############################ future plans #########################

        if($future_plans_m->check_if_data_exists($id) > 0)
        {

            $_future_plans_->description = request('future_plans_description');

            $_future_plans_->highest_degree_intended = request('highest_degree_intended_to_be_earned');

            $_future_plans_->save();

        }

        else
        {

            future_plans::create([

                'user_id' => auth()->user()->id,

                'description' => request('future_plans_description'),

                'highest_degree_intended' => request('highest_degree_intended_to_be_earned')

            ]);
        }

 

        


        ############################## education ########################

        $education = education::where('user_id', $id)->first();

        $education->previous_education = request('certificate_status');

        $education->number_of_highschools = request('num_high_schools');
        
        $education->number_of_CBO = request('number_of_CBO');

        $education->cgpa = request('cgpa');

        $education->national_exam_result = request('national_exam_result');

        $education->number_of_honors = request('num_of_honor');

        $education->save();

        return redirect()->back()->with('success', 'Education is Saved Successfully');

    }


    public function update_highschool($id)
    {

        $update_highschool = highschool::find($id);

        $update_highschool->name = request('name_of_highschool_update');

        $update_highschool->email = request('highschool_email_update');

        $update_highschool->address = request('highschool_address_update');

        $update_highschool->save();

        return redirect()->back()->with('success', 'Highschool '.request('name_of_highschool_update').' saved Successfully');


    }

    public function destroy_highschool($id)
    {

        $delete_highschool = highschool::find($id);

        $delete_highschool->delete();

        return redirect()->back()->with('success', 'Highschool is Successfully Deleted!');

    }

    public function update_cbo($id)
    {
        $this->validate(request(), ['counselor_prefix_update' => 'required']);

        $update_cbo = community_based_organization::find($id);

        $update_cbo->cbo_name = request('cbo_name_update');

        $update_cbo->counselor_prefix = request('counselor_prefix_update');

        $update_cbo->fullname = request('counselor_fullname_update');

        $update_cbo->email = request('counselor_email_update');

        $update_cbo->save();

        return redirect()->back()->with('success', 'Organization '.request('cbo_name_update').' saved Successfully');


    }

    public function destroy_cbo($id)
    {

        $delete_cbo = community_based_organization::find($id);

        $delete_cbo->delete();

        return redirect()->back()->with('success', 'Organization is Successfully Deleted!');

    }

    public function update_honor($id)
    {

        $this->validate(request(), ['honor_grade_level_update' => 'required', 'honor_level_of_recognition_update' => 'required']);

        $update_honor = honors::find($id);

        $update_honor->name = request('honor_name_update');

        $update_honor->grade_level = request('honor_grade_level_update');

        $update_honor->level_of_recognition = request('honor_level_of_recognition_update');

        $update_honor->save();

        return redirect()->back()->with('success', 'Honor '.request('honor_name_update').' saved Successfully');


    }

    public function destroy_honor($id)
    {

        $delete_honor = honors::find($id);

        $delete_honor->delete();

        return redirect()->back()->with('success', 'Honor is Successfully Deleted!');

    }

}
