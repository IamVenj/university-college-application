<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Questions;

use App\answer_for_university_questions;

use App\award_status;

use App\education;

class Step_4_Controller extends Controller
{
    public function __construct()
    {

        $this->middleware('auth');

    }
    public function index()
    {
        $submission = award_status::where('user_id', auth()->user()->id)->where('university_id', auth()->user()->university_id)->count();

        if($submission == 0)
        {

        	$Questions = new Questions();

            $education = new education();


    		// $university_id = $Questions->get_university_id();

        	$course_id = $education->get_course_id();

            $show_questions = $Questions->show_question(auth()->user()->academics_id, 3, auth()->user()->university_id, $course_id);



        	return view('main_layout.steps.step_four', compact('show_questions'));

    	}
        else
        {
            return redirect('/cannot-access');
        }

    }

    public function store()

    {

    	// $Questions = new Questions();

    	$uni_answers = new answer_for_university_questions();


    	// $university_id = $Questions->get_university_id();

    	$question_id = request('question_id');
    	
    	$answerz = request('answer');


    	$this->validate(request(),[

    		'answer' => 'required',

    	]);


        if($uni_answers->get_answers_count(auth()->user()->id, 4, auth()->user()->university_id) == 0)
        {

    		foreach ($answerz as $key => $value) {
    		 		
    	    	answer_for_university_questions::create([

    	    		'user_id' => auth()->user()->id,

    	    		'question_id' => $question_id[$key],

    	    		'answer' => $answerz[$key],

    	    		'university_id' => auth()->user()->university_id,

                    'step' => $step[$key]

    	    	]);

        	}	

        }
        else
        {

            $answer_id = request('answer_id');            

            foreach($answer_id as $key=>$value){

                $update_answer = answer_for_university_questions::find($answer_id[$key]);

                $update_answer->answer = $answerz[$key];

                $update_answer->save();

            }
            
        }	

		
		return redirect()->back()->with('success', ' Your data is Succussfully Saved');

    }
}
