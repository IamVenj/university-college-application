<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
        
            if(auth()->user()->firstname == '' || auth()->user()->middlename == '' || auth()->user()->lastname == '' || auth()->user()->phone_number == 0){

                return redirect('/info');

            }else{

                return redirect('/dashboard');

            }

        }

        return $next($request);
    }
}
