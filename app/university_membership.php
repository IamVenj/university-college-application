<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Collection;

class university_membership extends Model
{

    protected $guarded = [];

    public function members()

    {

        return $this->belongsToMany('App\User', 'user_id');

    }

    public function university()

    {

        return $this->hasMany('App\universities', 'id', 'university_id');

    }

    public function role()

    {

        return $this->belongsToMany('App\role', 'role_id');

    }

    public function get_created_user($request)
    {
        $get_created_user = DB::table('users')->where('email',  '=', $request)->get();

        return $get_created_user;
    }

    public function get_university($request)
    {
        $get_university = DB::table('universities')->where('id', '=', $request)->get();

        return $get_university;
    }

    public function get_university_id_of_this()
    {
        $get_user = $this->where('user_id', auth()->user()->id)->first();

        $get_university = $get_user->university_id;

        return $get_university;
    }

    public function check_created_user($request)
    {
        $check_created_user = DB::table('users')->where('email',  '=', $request)->count();

        return $check_created_user;

    }

    public function user_membership()
    {

        $get_admin = $this->where('role_id', '=', 2)->get();

        return $get_admin;

    }

    public function check_if_university_has_an_admin($university_id)
    {

        $get_admins = university_membership::where('role_id',  2)->where('university_id', $university_id)->count();

        return $get_admins;

    }

    public function user_employee_membership()
    {

        $get_employee = User::where('role_id', '=', 3)->get();

        return $get_employee;
    }

}
