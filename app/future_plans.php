<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class future_plans extends Model
{
    protected $fillable = ['user_id', 'description', 'highest_degree_intended'];

    protected $hidden = ['user_id'];

    public function check_if_data_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }

}
