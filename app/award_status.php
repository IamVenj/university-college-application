<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class award_status extends Model
{

	protected $fillable = ['user_id', 'submission', 'university_id', 'award'];

	protected $hidden = ['user_id', 'university_id'];


    public function check_if_account_has_passed_to_the_next_level($level)
    {

    	$award_status = $this->where('user_id', auth()->user()->id)->where($level, 1)->count();

    	return $award_status;

    }

    public function get_users($id, $university_id, $award)
    {
    	return $this->where('user_id', $id)->where('university_id', $university_id)->where('award', $award)->get();
    }

}