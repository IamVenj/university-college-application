<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{

    protected $guarded = [];

    public function get_university_id()
    {
        $university_membership = university_membership::where('user_id', auth()->user()->id)->get();

        $university_idz = $university_membership->pluck('university_id')->all();

        foreach ($university_idz as $university_id) {
            
            return $university_id;
        }
    }

    public function check_order_usage($order, $academics_id, $step, $course_id)
    {
        $question = $this->where('order', $order)->where('academics_id', $academics_id)->where('step', $step)->where('course_id', $course_id)->count();

        return $question;
    }

    public function check_question_usage($question, $academics_id, $step, $course_id)
    {

        $question = $this->where('question', $question)->where('academics_id', $academics_id)->where('step', $step)->where('course_id', $course_id)->count();

        return $question;

    }

    public function show_question($academics_id, $step, $university_id, $course_id)
    {

        $question = $this->where('academics_id', $academics_id)->where('step', $step)->where('university_id', $university_id)->where('course_id', $course_id)->orderBy('order', 'asc')->get();

        // dd($question);

        return $question;

    }

    public function get_question_count($academics_id, $step, $university_id, $course_id)
    {

        $question = $this->where('academics_id', $academics_id)->where('step', $step)->where('university_id', $university_id)->where('course_id', $course_id)->count();

        return $question;

    }

    public function universities()
    {
    	return $this->belongsTo('App\universities', 'university_id');
    }
    public function academics()
    {
    	return $this->hasMany('App\academics', 'id', 'academics_id');
    }   
}
