<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class father extends Model
{
    protected $fillable = ['father_first_name', 'father_middle_name', 'father_last_name', 'father_email', 'father_phone_number', 'father_address'];
}
