<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class previous_education extends Model
{
    protected $fillable = ['user_id', 'university_id', 'certificate_name', 'date_obtain'];

    public function check_if_user_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->count();

    }

    public function get_date()
    {

    	$previous_education = $this->where('user_id', auth()->user()->id)->get();

    	$education = $previous_education->pluck('date_obtain')->get(0);

    	return $education;
    }

}
