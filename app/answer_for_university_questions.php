<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Database\Eloquent\Collection;

class answer_for_university_questions extends Model
{
    protected $fillable = ['user_id', 'university_id', 'question_id', 'answer', 'step'];


    public function return_question_and_answer($index)
    {

    	for($i=0; $i<$index; $i++) {

			return $i;

		}	
		
    }

    public function get_answers($user_id, $step, $university_id, $question_id)
    {

    	$answer = $this->where('user_id', $user_id)->where('step', $step)->where('university_id', $university_id)->where('question_id', $question_id)->get();

    	return $answer;

    }

    public function get_answers_count($user_id, $step, $university_id)
    {

    	$answer = $this->where('user_id', $user_id)->where('step', $step)->where('university_id', $university_id)->count();

    	return $answer;

    }

}
