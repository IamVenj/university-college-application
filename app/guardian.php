<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class guardian extends Model
{
    protected $fillable = ['guardian_first_name', 'guardian_middle_name', 'guardian_last_name', 'guardian_email', 'guardian_phone_number', 'guardian_address'];
}
