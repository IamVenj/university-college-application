<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class universities extends Model
{

	protected $fillable = [
		'university_name', 'user_id'
	];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function get_university_name($university_id)
    {
    	$Query_university = $this->where('id', $university_id)->get();

    	$university_names = $Query_university->pluck('university_name');

    	foreach ($university_names as $university_name) {
    	
    		return $university_name;

    	}

    	
    }
}
