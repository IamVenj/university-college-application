<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class other extends Model
{
    protected $fillable = ['first_name', 'middle_name', 'last_name', 'email', 'phone_number', 'address'];
}
