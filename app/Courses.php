<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $fillable = ['university_id', 'academics_id', 'course_name', 'start_date', 'end_date'];

    protected $hidden = ['university_id', 'academics_id'];

    public function courses($university_id)
    {
    	$courses = $this->where('university_id', $university_id)->get();

    	return $courses;
    }

}
