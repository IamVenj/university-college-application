<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;

use App\university_membership;

class academics extends Model
{

    public function check_if_academics_has_been_used($academics, $university_id)
    {
        $check = $this->where('university_id', $university_id)->where('academics_name', $academics)->count();

        return $check;
    }

    public function get_academics_that_university_registered($university_id)
    {
        $academics = $this->where('university_id', $university_id)->get();

        return $academics;
    }

	protected $fillable = ['academics_name', 'user_id', 'university_id'];

    protected $hidden = ['user_id', 'university_id'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function university()
    {
    	return $this->belongsTo('App\universities', 'university_id');
    }


}
