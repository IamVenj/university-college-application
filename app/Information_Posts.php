<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information_Posts extends Model
{

	protected $fillable = [

		'title', 'user_id', 'university_id', 'description', 'info_type', 'image'

	];

	protected $hidden = [

		'user_id', 'university_id'

	];

    public function user()
    {

    	return $this->belongsTo('App\User', 'user_id');

    }

    public function university()
    {

    	return $this->belongsTo('App\universities', 'university_id');

    }

    public function get_posts($university_id)
    {
        return $this->where('university_id', $university_id)->get();
    }

}
