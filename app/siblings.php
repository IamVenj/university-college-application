<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siblings extends Model
{
    protected $fillable = ['user_id', 'fullname', 'email', 'phone_number'];

    protected $hidden = ['user_id'];

    public function check_if_data_exists(){

    	return $this->where('user_id', auth()->user()->id)->count();

    }
}
