<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class education extends Model
{

    protected $fillable = [
        'academics_id','user_id', 'course_id', 'previous_education', 'previous_education_id', 'number_of_highschools', 'number_of_cbo', 'cgpa', 'national_exam_result', 'future_plans_id', 'number_of_honors'
    ];

    protected $hidden = ['academics_id', 'user_id', 'course_id'];



    public function get_all_own_education()
    {
        $education = $this->where('user_id', auth()->user()->id)->get();

        return $education;
    }

    public function count_this()
    {
        $education = $this->where('user_id', auth()->user()->id)->where('previous_education', !empty(''))->count();

        return $education;
    }

    public function course_id($id)
    {

        $course_id = $this->where('user_id', $id)->first();

        return $course_id->course_id;

    }

    public function get_course_id()
    {

        $course_id = $this->where('user_id', auth()->user()->id)->get();

        $get_course_id = $course_id->pluck('course_id')->get(0);

        return $get_course_id;

    }

    public function academics()
    {
    	return $this->hasOne('App\academics', 'id','academics_id');
    }

    public function user()
    {
    	return $this->belongsToMany('App\User', 'id','user_id');
    }

    public function community_based_organization()
    {
    	return $this->hasMany('App\community_based_organization', 'id', 'community_based_organization_id');
    }

}
