<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\university_membership;

class step_quantities extends Model
{

	protected $guarded = [];

    public function universities()
    {
    	return $this->belongsTo('App\universities', 'university_id');
    }

    public function get_university_id()
    {
    	 $university_id = university_membership::where('user_id', auth()->user()->id)->get();

    	 return $university_id->pluck('university_id')->get(0);
    }
}
