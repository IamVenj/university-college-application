<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mother extends Model
{
    protected $fillable = ['mother_first_name', 'mother_middle_name', 'mother_last_name', 'mother_email', 'mother_phone_number', 'mother_address'];
}
