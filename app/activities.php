<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class activities extends Model
{
    protected $fillable = ['user_id', 'number_of_activities', 'activity_name', 'description', 'hours_spent_per_week'];

    protected $hidden = ['user_id'];

    public function check_if_data_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }

    public function get_data($id)
    {

    	return $this->where('user_id', $id)->first();

    }

    public function check_data()
    {

        return $this->where('user_id', auth()->user()->id)->count();

    }

    public function get_all_activities()
    {

        return $this->where('user_id', auth()->user()->id)->get();

    }
}
