<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class honors extends Model
{
    protected $fillable = ['user_id', 'name', 'grade_level', 'level_of_recognition'];

    protected $hidden = ['user_id'];

    public function check_if_data_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }
}
