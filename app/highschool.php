<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class highschool extends Model
{
    protected $fillable = ['user_id', 'name', 'email', 'address', 'number_of_highschools'];

    protected $hidden = ['user_id'];


    public function check_if_data_exists($id)
    {

    	return $this->where('user_id', $id)->count();

    }

    public function check_if_email_exists($email, $id)
    {

    	return $this->where('user_id', $id)->where('email', $email)->count();

    }

}
