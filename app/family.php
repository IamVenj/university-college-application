<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class family extends Model
{
    protected $fillable = ['user_id', 'father_id', 'mother_id', 'guardian_id', 'other_id', 'currently_living_with', 'marital_status', 'children', 'number_of_children', 'number_of_siblings'];

    protected $hidden = ['user_id'];

    public function check_if_father_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->where('father_id', !null)->count();

    }

    public function check_if_mother_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->where('mother_id', !null)->count();

    }

    public function check_if_guardian_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->where('guardian_id', !null)->count();

    }

    public function check_if_other_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->where('other_id', !null)->count();

    }

    public function check_if_data_exists()
    {

    	return $this->where('user_id', auth()->user()->id)->count();

    }

    public function get_data()
    {

    	return $this->where('user_id', auth()->user()->id)->first();

    }

    public function get_family()
    {

    	return $this->where('user_id', auth()->user()->id)->get();

    }

}
