<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Check_and_Radio_Question_List extends Model
{

	protected $fillable = ['question_id', 'input_type', 'quest_box'];

    public function get_Question_id($question, $university_id, $step, $order)
    {
    	$getQuestion = Questions::where('question', $question)->where('university_id', $university_id)->where('step', $step)->where('order', $order)->get();

    	$Question_id = $getQuestion->pluck('id')->get(0);

    	return $Question_id;
    }
}
