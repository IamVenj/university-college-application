<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class universities_applied extends Model
{
    protected $fillable = ['user_id', 'university_id', 'course_id'];

    protected $hidden = ['user_id', 'university_id', 'course_id'];
}
