<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class community_based_organization extends Model
{
 	protected $fillable = ['user_id', 'cbo_name', 'counselor_prefix', 'fullname', 'email'];

    protected $hidden = ['user_id', 'email'];

    public function check_if_data_exists_cbo($id)
    {

        $am = $this->where('user_id', $id)->count();

        return $am;

    }
}
