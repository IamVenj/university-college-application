<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('academics_id')->unsigned()->nullable();
            $table->integer('course_id')->unsigned()->nullable();
            $table->string('previous_education')->nullable();
            $table->integer('previous_education_id')->unsigned()->nullable();
            $table->integer('number_of_highschools')->unsigned()->nullable();
            $table->integer('number_of_cbo')->unsigned()->nullable();
            $table->float('cgpa')->nullable();
            $table->float('national_exam_result')->nullable();
            $table->string('number_of_honors')->nullable();
            $table->integer('future_plans_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education');
    }
}
