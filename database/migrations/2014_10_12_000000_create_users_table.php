<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname')->nullable();
            $table->string('middlename')->nullable();
            $table->string('lastname')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('country_code')->nullable();
            $table->integer('phone_number')->nullable();
            $table->string('sex')->nullable();
            $table->string('address_line')->nullable();
            $table->string('city')->nullable();
            $table->string('subcity')->nullable();
            $table->string('house_number')->nullable();
            $table->string('woreda')->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->integer('academics_id')->unsigned()->nullable();
            $table->string('certificate')->nullable();
            $table->integer('activation_status')->unsigned()->default(1);
            $table->integer('university_id')->unsigned()->nullable();
            $table->string('token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
