<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('families', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('father_id')->unsinged()->nullable();
            $table->integer('mother_id')->unsigned()->nullable();
            $table->integer('guardian_id')->unsigned()->nullable();
            $table->integer('other_id')->unsigned()->nullable();
            $table->string('currently_living_with')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('children')->nullable();
            $table->integer('number_of_children')->unsigned()->nullable();
            $table->integer('number_of_siblings')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('families');
    }
}
