<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'remember_token' => str_random(10),
        'firstname' => $faker->firstname,
        'middlename' => $faker->firstname,
        'lastname' => $faker->lastname,
        'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'password' => bcrypt('admin'),
        'country_code' => $faker->areacode,
        'phone_number' => $faker->e164PhoneNumber,
        'address_line' => $faker->address,
        'city' => $faker->city,
        'subcity' => $faker->citySuffix,
        'house_number' => $faker->buildingNumber,
        'woreda' => $faker->cityPrefix,
        'activation_status' => 1,
   		'token' => null,
   		// 'role_id' => 1
    ];
});

$factory->define(App\universities::class, function(Faker $faker){
	return [
		'university_name' => ucfirst($faker->word)
	];
});
