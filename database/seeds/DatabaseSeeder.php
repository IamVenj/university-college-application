<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

    	$universities = [

    		['name' => 'The University of Adigrat'],

    		['name' => 'The University of Mekelle'],

    		['name' => 'Addis Ababa University'],

    		['name' => 'The University of Adama'],

    		['name' => 'The University of Haremaya'],

    		['name' => 'The University of Gondar'],

    		['name' => 'The University of Axum'],

    		['name' => 'Wello University'],

    		['name' => 'The University of Hawassa'],

    		['name' => 'The University of Bahir Dar']

    	];


    	foreach($universities as $university){

            DB::table('universities')->delete();

	    	factory(App\universities::class)->create([

	    		'university_name' => $university['name']

	    	]);

    	}

        // factory(App\User::class, 30)->create();

    	factory(App\User::class)->create([

    		'email' => 'admin@admin.com',
	        'email_verified_at' => now(),
	        'remember_token' => str_random(10),
	        'firstname' => 'edit',
	        'middlename' => 'admin',
	        'lastname' => 'name',
	        'date_of_birth' => now(),
	        'password' => bcrypt('admin'),
	        'country_code' => '+251',
	        'sex' => 'F',
	        'address_line' => 'African Avenue',
	        'city' => 'Addis Ababa',
	        'subcity' => 'Kirkos',
	        'house_number' => '3233',
	        'woreda' => '5',
	        'activation_status' => 1,
	   		'token' => null,
	   		'role_id' => 1

    	]);
    }
}
