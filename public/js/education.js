$(document).ready(function(){


	$('#certificate_status_yes').on('click', function(){

		document.getElementById('certificate_status_length').style.display = 'block';

	});

	$('#certificate_status_no').on('click', function(){

		document.getElementById('certificate_status_length').style.display = 'none';

	});


	$('#honor').on('change', function(){

	    var input = $(this).val();

	    $('.honor_name').remove();
	    
	    $('.honor_grade_level').remove();
	    
	    $('.honor_level_of_recognition').remove();


	    for(var i=0; i<input; i++)
	    {
	      $('#InputAreaForHonorTitle').append('<div class="honor_name"><label class="col-form-label">Honor Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><input type="text" name="honor_name[]" class="form-control form-control-sm" placeholder="Honor Name" required/></div></div>');
	      
	      $('#InputAreaForHonorGradeLevel').append('<div class="honor_grade_level"><label class="col-form-label">Grade Level</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><select class="form-control form-control-lg" name="honor_grade_level[]"><option selected disabled>Choose Grade Level</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="Associates">Associates(AA, AS)</option><option value="Bachelors">Bachelors(BA, BS)</option><option value="Masters">Masters(MA, MS)</option><option value="Business">Business(MBA, MAcc)</option><option value="Law">Law(JD, LLM)</option><option value="Medicine">Medicine(MD, DO, DVM, DDS)</option><option value="Doctorate" >Doctorate(PhD, EdD)</option></select></div></div>');
	      
	      $('#InputAreaForHonorLevelOfRecognition').append('<div class="honor_level_of_recognition"><label class="col-form-label">Level of Recognition</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><select class="form-control form-control-lg" name="honor_level_of_recognition[]"><option selected disabled>Choose Level of Recognition</option><option value="School">School</option><option value="State/Regional">State/Regional</option><option value="National">National</option><option value="International">International</option></select></div></div>');

	    }

	}).change();


	$('#CBO').on('change', function(){

	    var inputs = $(this).val();

	    $('.CBO1').remove();

	    $('.CBO2').remove();
	    
	    $('.CBO3').remove();
	    
	    $('.CBO4').remove();


	    for(var i=0; i < inputs; i++)
	    {

	      
	      
	      $("#InputAreaForCBOName").append('<div class="CBO1"><label class="col-form-label">Name of Organization</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-google-circles-communities text-danger"></i></span></div><input type="text" name="CBO[]" class="form-control form-control-sm" placeholder="Name of Organization" required/></div></div></div>');
	      
	      $("#InputAreaForCBOCounselorPrefix").append('<div class="CBO2"><label class="col-form-label">Counselor/Mentor Prefix</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-map-marker text-danger"></i></span></div><select class="form-control form-control-lg" name="counselor_prefix[]"><option selected disabled>Choose Prefix</option><option value="Mr">Mr</option><option value="Dr" >Dr</option><option value="Professor">Professor</option></select></div></div></div>');

	      $("#InputAreaForCBOCounselorFullName").append('<div class="CBO3"><label class="col-form-label">Counselor/Mentor Fullname</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-danger"></i></span></div><input type="text" name="counselor_fullname[]" class="form-control form-control-sm" placeholder="Fullname" required/></div></div></div>');

	      $("#InputAreaForCBOCounselorEmail").append('<div class="CBO4"><label class="col-form-label">Counselor/Mentor Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-danger"></i></span></div><input type="email" name="counselor_email[]" class="form-control form-control-sm" placeholder="Email" required/></div></div></div>');



	    }


  	}).change();

	// add more highschool names

	$('#num_high_schools').on('change', function(){

	    var inputs = $(this).val();



	    $('.highschool_name').remove();

	    $('.highschool_email').remove();
	    
	    $('.highschool_phone').remove();

	    for(var i=0; i < inputs; i++)
	    {


	        $("#InputAreaForHighSchoolName").append('<div class="highschool_name"><label class="col-form-label">High Schools Name</label><div class="form-group HighSchoolName"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-school text-success"></i></span></div><input type="text" class="form-control form-control-sm" name="name_of_highschool[]" placeholder="Name of Your HighSchool" required/></div></div></div>');

	        $('#InputAreaForHighSchoolEmail').append('<div class="highschool_email"><label class="col-form-label">High Schools Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="text" class="form-control form-control-sm" name="email_of_highschool[]" placeholder="High Schools Email" required/></div></div></div>');

	        $('#InputAreaForHighSchoolPhone').append('<div class="highschool_phone"><label class=" col-form-label">High Schools Address</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-map-marker text-danger"></i></span></div><input type="text" class="form-control form-control-sm" name="address_of_highschool[]" placeholder="High Schools Address" required/></div></div></div>');

	    }

  	}).change();

	$('#highschool_plus').on('click', function(){

		$("#InputAreaForHighSchoolName").append('<div class="highschool_name"><label class="col-form-label">High Schools Name</label><div class="form-group HighSchoolName"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-school text-success"></i></span></div><input type="text" class="form-control form-control-sm" name="name_of_highschool" placeholder="Name of Your HighSchool" required/></div></div></div>');

        $('#InputAreaForHighSchoolEmail').append('<div class="highschool_email"><label class="col-form-label">High Schools Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="text" class="form-control form-control-sm" name="email_of_highschool" placeholder="High Schools Email" required/></div></div></div>');

        $('#InputAreaForHighSchoolPhone').append('<div class="highschool_phone"><label class=" col-form-label">High Schools Address</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-map-marker text-danger"></i></span></div><input type="text" class="form-control form-control-sm" name="address_of_highschool" placeholder="High Schools Address" required/></div></div></div>');

		document.getElementById('highschool_plus').style.display = 'none';

	});

	$('#CBO_plus').on('click', function(){

		$("#InputAreaForCBOName").append('<div class="CBO1"><label class="col-form-label">Name of Organization</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-google-circles-communities text-danger"></i></span></div><input type="text" name="CBO" class="form-control form-control-sm" placeholder="Name of Organization" required/></div></div></div>');
      
      	$("#InputAreaForCBOCounselorPrefix").append('<div class="CBO2"><label class="col-form-label">Counselor/Mentor Prefix</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-map-marker text-danger"></i></span></div><select class="form-control form-control-lg" name="counselor_prefix"><option selected disabled>Choose Prefix</option><option value="Mr">Mr</option><option value="Dr" >Dr</option><option value="Professor">Professor</option></select></div></div></div>');

      	$("#InputAreaForCBOCounselorFullName").append('<div class="CBO3"><label class="col-form-label">Counselor/Mentor Fullname</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-danger"></i></span></div><input type="text" name="counselor_fullname" class="form-control form-control-sm" placeholder="Fullname" required/></div></div></div>');

      	$("#InputAreaForCBOCounselorEmail").append('<div class="CBO4"><label class="col-form-label">Counselor/Mentor Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-danger"></i></span></div><input type="email" name="counselor_email" class="form-control form-control-sm" placeholder="Email" required/></div></div></div>');

		document.getElementById('CBO_plus').style.display = 'none';

	});


	// drop highschools

	$('#highschool_minus').on('click', function(){

		$('.highschool_name').remove();

		$('.highschool_email').remove();
		
		$('.highschool_phone').remove();

	});



	// how many CBO?


	// drop CBO

	$('#CBO_minus').on('click',function(){

		$('.CBO1').remove();

		$('.CBO2').remove();
		
		$('.CBO3').remove();
		
		$('.CBO4').remove();

	});

	


	

	// honor




	// drop honor

	$('#honor_minus').on('click', function(){

		$('.honor_name').remove();
		
		$('.honor_grade_level').remove();
		
		$('.honor_level_of_recognition').remove();

	});


	$('#honor_plus').on('click', function(){

		$('#InputAreaForHonorTitle').append('<div class="honor_name"><label class="col-form-label">Honor Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><input type="text" name="honor_name" class="form-control form-control-sm" placeholder="Honor Name" required/></div></div>');
      
      	$('#InputAreaForHonorGradeLevel').append('<div class="honor_grade_level"><label class="col-form-label">Grade Level</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><select class="form-control form-control-lg" name="honor_grade_level"><option selected disabled>Choose Grade Level</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="Associates">Associates(AA, AS)</option><option value="Bachelors">Bachelors(BA, BS)</option><option value="Masters">Masters(MA, MS)</option><option value="Business">Business(MBA, MAcc)</option><option value="Law">Law(JD, LLM)</option><option value="Medicine">Medicine(MD, DO, DVM, DDS)</option><option value="Doctorate" >Doctorate(PhD, EdD)</option></select></div></div>');
      
      	$('#InputAreaForHonorLevelOfRecognition').append('<div class="honor_level_of_recognition"><label class="col-form-label">Level of Recognition</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-account text-success"></i></span></div><select class="form-control form-control-lg" name="honor_level_of_recognition"><option selected disabled>Choose Level of Recognition</option><option value="School">School</option><option value="State/Regional">State/Regional</option><option value="National">National</option><option value="International">International</option></select></div></div>');

		document.getElementById('honor_plus').style.display = 'none';

	})



	

	$('.dynamics').on('change', function(){

      if($(this).val() != '')
      {

        
        var select = $(this).attr('id');
        
        var value = $(this).val();
        
        var dependent = $(this).data('dependent');

        var _token = $('input[name="_token').val();


        $.ajax({
          
          url:"education/fetch",

          method: "POST",
          
          data:{select:select, value:value, _token:_token, dependent:dependent},

          success:function(result)
          {

            $('#'+dependent).html(result);
          
          } 
        
        })
      
      }

    }).change();



	

});