// drop activities

$('#activity_minus').on('click', function(){

	$('.activity_name').remove();

	$('.activity_recognition').remove();
	
	$('.activity_hours_spent').remove();
	
	$('.activity_weeks_spent').remove();

});

// activity

$('#activity').on('change', function(){

var inputs = $(this).val();

$('.activity_name').remove();

$('.activity_recognition').remove();

$('.activity_hours_spent').remove();

$('.activity_weeks_spent').remove();

for(var i=0; i < inputs; i++)
{

  
    $("#InputAreaForActivityName").append('<div class="activity_name"><label class="col-form-label">Activity Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-guitar-electric text-success"></i></span></div><input type="text" name="activity_name[]" class="form-control form-control-sm" placeholder="Activity Name" required/></div></div>');
  
    $("#InputAreaForActivityRecognition").append('<div class="activity_recognition"><label class="col-form-label">Please Describe This Activity</label><div class="form-group"><textarea name="activity_description[]" rows="1" class="form-control" required></textarea></div></div></div>');

    $("#InputAreaForActivityHoursSpent").append('<div class="activity_hours_spent"><label class="col-form-label">Hours Spent Per Week</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-guitar-electric text-success"></i></span></div><input type="text" name="activity_hours_spent[]" class="form-control form-control-sm" placeholder="Hours Spent Per Week" required/></div></div>');

  
}

});

$('#activity_plus').on('click', function(){

	$("#InputAreaForActivityName2").append('<div class="activity_name"><label class="col-form-label">Activity Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-guitar-electric text-success"></i></span></div><input type="text" name="activity_name" class="form-control form-control-sm" placeholder="Activity Name" required/></div></div>');

	$("#InputAreaForActivityRecognition2").append('<div class="activity_recognition"><label class="col-form-label">Please Describe This Activity</label><div class="form-group"><textarea name="activity_description" rows="1" class="form-control" required></textarea></div></div></div>');

	$("#InputAreaForActivityHoursSpent2").append('<div class="activity_hours_spent"><label class="col-form-label">Hours Spent Per Week</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class=" mdi mdi-guitar-electric text-success"></i></span></div><input type="text" name="activity_hours_spent" class="form-control form-control-sm" placeholder="Hours Spent Per Week" required/></div></div>');

	document.getElementById('activity_plus').style.display='none';

});