$(document).ready(function(){

    $('.dynamic').on('change', function(){



      if($(this).val() != '')
      {
        
        var select = $(this).attr('id');
        
        var value = $(this).val();
        
        var dependent = $(this).data('dependent');

        var table = $(this).data('dbtable');

        var academics = $(this).data('academics');

        var course = $(this).data('course');
        
        var courses = $(this).data('courses');

        var _token = $('input[name="_token').val();

        $('.ad').remove();

        $.ajax({
          
          url:"info/fetch",

          method: "POST",
          
          data:{select:select, value:value, _token:_token, dbtable:table, academics:academics, course:course, courses:courses},

          success:function(result)
          {

            $('#'+dependent).html(result);
          
          } 
        
        })
      
      }

    });



}).change();