$(document).ready(function(){
// family_parent

	$('#family_parent').on('change', function(){

		var inputs = $(this).val();

		if(inputs == 'Father')
		{			
			document.getElementById('father_about').style.display = 'block';

			document.getElementById('mother_about').style.display = 'none';

			document.getElementById('other_about').style.display = 'none';

			document.getElementById('guardian_about').style.display = 'none';	
		}
		else if(inputs == 'Mother')
		{
			document.getElementById('father_about').style.display = 'none';

			document.getElementById('mother_about').style.display = 'block';

			document.getElementById('other_about').style.display = 'none';

			document.getElementById('guardian_about').style.display = 'none';	
		}
		else if(inputs == 'Both')
		{
			document.getElementById('father_about').style.display = 'block';

			document.getElementById('mother_about').style.display = 'block';

			document.getElementById('other_about').style.display = 'none';

			document.getElementById('guardian_about').style.display = 'none';	
		}
		else if(inputs == 'Guardian')
		{
			document.getElementById('father_about').style.display = 'none';

			document.getElementById('mother_about').style.display = 'none';

			document.getElementById('other_about').style.display = 'none';

			document.getElementById('guardian_about').style.display = 'block';			
		}
		else if(inputs == 'Other')
		{
			document.getElementById('father_about').style.display = 'none';

			document.getElementById('mother_about').style.display = 'none';

			document.getElementById('other_about').style.display = 'block';

			document.getElementById('guardian_about').style.display = 'none';
		}
		else{

			document.getElementById('father_about').style.display = 'none';

			document.getElementById('mother_about').style.display = 'none';

			document.getElementById('other_about').style.display = 'none';

			document.getElementById('guardian_about').style.display = 'none';

		}

	}).change();

});
	
$(document).ready(function(){
	// marital status

	$('#marital_status').on('change', function(){

		var input = $(this).val();

		if(input == 'Married' || input == 'Divorced' || input == 'Separated' || input == 'Widowed' || input == 'Never Married')
		{

			document.getElementById('children_stats').style.display = 'block';

		}

	}).change();

});

$(document).ready(function(){

	// children status

	$('#children_status_yes').on('click', function(){

		document.getElementById('children_status_length').style.display = 'block';

		$('.children_name').remove();

		$('.children_email').remove();

		$('.children_phone').remove();

	});

	$('#children_status_no').on('click', function(){

		document.getElementById('children_status_length').style.display = 'none';

		$('.children_name').remove();

		$('.children_email').remove();

		$('.children_phone').remove();

	});


	$('#_children_').on('change', function(){

		var inputs = $(this).val();

		$('.children_name').remove();

		$('.children_email').remove();

		$('.children_phone').remove();

		for(var i=0; i < inputs; i++)
		{
			
			$("#InputAreaForChildsName").append('<div class="children_name"><label class="col-form-label">FullName</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-success"></i></span></div><input type="text" name="child_name[]" class="form-control form-control-sm" placeholder="FullName" required/></div></div>');
			
			$("#InputAreaForChildsEmail").append('<div class="children_email"><label class="col-form-label">Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="email" name="child_email[]" class="form-control form-control-sm" placeholder="Email"/></div></div>');
			
			$("#InputAreaForChildsPhoneNum").append('<div class="children_phone"><label class="col-form-label">Phone Number</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-cellphone-iphone text-danger"></i></span></div><input type="text" name="child_phone[]" class="form-control form-control-sm" placeholder="Phone Number"/></div></div>');

		}

	});

	$('#children_plus').on('click', function(){

		$("#InputAreaForChildsName").append('<div class="children_name"><label class="col-form-label">FullName</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-success"></i></span></div><input type="text" name="child_name" class="form-control form-control-sm" placeholder="FullName" required/></div></div>');
			
		$("#InputAreaForChildsEmail").append('<div class="children_email"><label class="col-form-label">Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="email" name="child_email" class="form-control form-control-sm" placeholder="Email"/></div></div>');
		
		$("#InputAreaForChildsPhoneNum").append('<div class="children_phone"><label class="col-form-label">Phone Number</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-cellphone-iphone text-danger"></i></span></div><input type="text" name="child_phone" class="form-control form-control-sm" placeholder="Phone Number"/></div></div>');		

		document.getElementById('children_plus').style.display = 'none';

	});

	// siblings status

	$('#siblings').on('change', function(){

		var inputs = $(this).val();

		$('.siblings_name').remove();
		
		$('.siblings_email').remove();

		$('.siblings_phone').remove();

		for(var i=0; i < inputs; i++)
		{
			
			$("#InputAreaForSiblingName").append('<div class="siblings_name"><label class="col-form-label">Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-success"></i></span></div><input type="text" name="sibling_name[]" class="form-control form-control-sm" placeholder="Name" required/></div>');
			
			$("#InputAreaForSiblingEmail").append('<div class="siblings_email"><label class="col-form-label">Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="email" name="sibling_email[]" class="form-control form-control-sm" placeholder="Email" required/></div>');
			
			$("#InputAreaForSiblingPhoneNumber").append('<div class="siblings_phone"><label class="col-form-label">Phone Number</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-cellphone-iphone text-danger"></i></span></div><input type="text" name="sibling_phone_number[]" class="form-control form-control-sm" placeholder="Phone Number" required/></div>');

		}

	});

	$('#siblings_plus').on('click', function(){

		$("#InputAreaForSiblingName").append('<div class="siblings_name"><label class="col-form-label">Name</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-account text-success"></i></span></div><input type="text" name="sibling_name" class="form-control form-control-sm" placeholder="Name" required/></div>');
			
		$("#InputAreaForSiblingEmail").append('<div class="siblings_email"><label class="col-form-label">Email</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-email text-warning"></i></span></div><input type="email" name="sibling_email" class="form-control form-control-sm" placeholder="Email" required/></div>');
		
		$("#InputAreaForSiblingPhoneNumber").append('<div class="siblings_phone"><label class="col-form-label">Phone Number</label><div class="form-group"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i class="mdi mdi-cellphone-iphone text-danger"></i></span></div><input type="text" name="sibling_phone_number" class="form-control form-control-sm" placeholder="Phone Number" required/></div>');

		document.getElementById('siblings_plus').style.display = 'none';

	});

	// drop siblings

	$('#siblings_minus').on('click', function(){

		$('.siblings_name').remove();
		
		$('.siblings_email').remove();

		$('.siblings_phone').remove();

	});


});