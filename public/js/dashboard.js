(function($) {
  'use strict';
  $(function() {

    Chart.defaults.global.legend.labels.usePointStyle = true;

    ajaxGetData();

    ajaxGetGender();

    function ajaxGetData() {

      $.ajax({

        url:"dashboard/fetch",

        method:'GET',

        success:function(result)
        {

          university(result);
        
        } 

      });

    }

    function ajaxGetGender()
    {

      $.ajax({

        url:"dashboard/gender",

        method:'GET',

        success:function(result)
        {

          gender(result);

        }

      });

    }

    function gender(response)
    {

      if ($("#traffic-chart").length) {
        Chart.defaults.global.legend.labels.usePointStyle = true;
        var ctx = document.getElementById('visit-sale-chart').getContext("2d");

        var gradientStrokeBlue = ctx.createLinearGradient(0, 0, 0, 181);
        gradientStrokeBlue.addColorStop(0, 'rgba(54, 215, 232, 1)');
        gradientStrokeBlue.addColorStop(1, 'rgba(177, 148, 250, 1)');
        var gradientLegendBlue = 'linear-gradient(to right, rgba(54, 215, 232, 1), rgba(177, 148, 250, 1))';

        var gradientStrokeRed = ctx.createLinearGradient(0, 0, 0, 50);
        gradientStrokeRed.addColorStop(0, 'rgba(255, 191, 150, 1)');
        gradientStrokeRed.addColorStop(1, 'rgba(254, 112, 150, 1)');
        var gradientLegendRed = 'linear-gradient(to right, rgba(255, 191, 150, 1), rgba(254, 112, 150, 1))';

        var gradientStrokeGreen = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeGreen.addColorStop(0, 'rgba(6, 185, 157, 1)');
        gradientStrokeGreen.addColorStop(1, 'rgba(132, 217, 210, 1)');
        var gradientLegendGreen = 'linear-gradient(to right, rgba(6, 185, 157, 1), rgba(132, 217, 210, 1))';      

        var trafficChartData = {
          datasets: [{
            data: response.percentage,
            backgroundColor: [
              gradientStrokeBlue,
              gradientStrokeGreen,
              gradientStrokeRed
            ],
            hoverBackgroundColor: [
              gradientStrokeBlue,
              gradientStrokeGreen,
              gradientStrokeRed
            ],
            borderColor: [
              gradientStrokeBlue,
              gradientStrokeGreen,
              gradientStrokeRed
            ],
            legendColor: [
              gradientLegendBlue,
              gradientLegendGreen,
              gradientLegendRed
            ]
          }],
      
          // These labels appear in the legend and in the tooltips when hovering different arcs
          labels: response.gender
        };
        var trafficChartOptions = {
          responsive: true,
          animation: {
            animateScale: true,
            animateRotate: true
          },
          legend: false,
          legendCallback: function(chart) {
            var text = []; 
            text.push('<ul>'); 
            for (var i = 0; i < trafficChartData.datasets[0].data.length; i++) { 
                text.push('<li><span class="legend-dots" style="background:' + 
                trafficChartData.datasets[0].legendColor[i] + 
                            '"></span>'); 
                if (trafficChartData.labels[i]) { 
                    text.push(trafficChartData.labels[i]); 
                }
                text.push('<span class="float-right">'+trafficChartData.datasets[0].data[i]+"%"+'</span>')
                text.push('</li>'); 
            } 
            text.push('</ul>'); 
            return text.join('');
          }
        };
        var trafficChartCanvas = $("#traffic-chart").get(0).getContext("2d");
        var trafficChart = new Chart(trafficChartCanvas, {
          type: 'doughnut',
          data: trafficChartData,
          options: trafficChartOptions
        });
        $("#traffic-chart-legend").html(trafficChart.generateLegend());      
      }

    }

    function university(response){

      if ($("#visit-sale-chart").length) {
        Chart.defaults.global.legend.labels.usePointStyle = true;
        var ctx = document.getElementById('visit-sale-chart').getContext("2d");

        var gradientStrokeViolet = ctx.createLinearGradient(0, 0, 0, 181);
        gradientStrokeViolet.addColorStop(0, 'rgba(218, 140, 255, 1)');
        gradientStrokeViolet.addColorStop(1, 'rgba(154, 85, 255, 1)');
        var gradientLegendViolet = 'linear-gradient(to right, rgba(218, 140, 255, 1), rgba(154, 85, 255, 1))';
        
        var gradientStrokeBlue = ctx.createLinearGradient(0, 0, 0, 360);
        gradientStrokeBlue.addColorStop(0, 'rgba(54, 215, 232, 1)');
        gradientStrokeBlue.addColorStop(1, 'rgba(177, 148, 250, 1)');
        var gradientLegendBlue = 'linear-gradient(to right, rgba(54, 215, 232, 1), rgba(177, 148, 250, 1))';

        var gradientStrokeRed = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed.addColorStop(0, 'rgba(255, 191, 150, 1)');
        gradientStrokeRed.addColorStop(1, 'rgba(254, 112, 150, 1)');
        var gradientLegendRed = 'linear-gradient(to right, rgba(255, 191, 150, 1), rgba(254, 112, 150, 1))';

        var gradientStrokeRed2 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed2.addColorStop(0, 'rgba(255, 251, 10, 1)');
        gradientStrokeRed2.addColorStop(1, 'rgba(254, 192, 50, 1)');
        var gradientLegendRed2 = 'linear-gradient(to right, rgba(255, 251, 10, 1), rgba(254, 92, 150, 1))';

        var gradientStrokeRed3 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed3.addColorStop(0, 'rgba(255, 251, 10, 1)');
        gradientStrokeRed3.addColorStop(1, 'rgba(24, 192, 150, 1)');
        var gradientLegendRed3 = 'linear-gradient(to right, rgba(255, 251, 10, 1), rgba(24, 192, 150, 1))';

        var gradientStrokeRed4 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed4.addColorStop(0, 'rgba(255, 251, 10, 1)');
        gradientStrokeRed4.addColorStop(1, 'rgba(124, 12, 250, 1)');
        var gradientLegendRed4 = 'linear-gradient(to right, rgba(255, 251, 10, 1), rgba(124, 12, 250, 1))';

        var gradientStrokeRed5 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed5.addColorStop(0, 'rgba(255, 251, 10, 1)');
        gradientStrokeRed5.addColorStop(1, 'rgba(224, 122, 250, 1)');
        var gradientLegendRed5 = 'linear-gradient(to right, rgba(255, 251, 10, 1), rgba(224, 122, 250, 1))';

        var gradientStrokeRed6 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed6.addColorStop(0, 'rgba(14, 62, 150, 1)');
        gradientStrokeRed6.addColorStop(1, 'rgba(14, 12, 150, 1)');
        var gradientLegendRed6 = 'linear-gradient(to right, rgba(14, 62, 150, 1), rgba(14, 12, 150, 1))';

        var gradientStrokeRed7 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed7.addColorStop(0, 'rgba(124, 162, 150, 1)');
        gradientStrokeRed7.addColorStop(1, 'rgba(124, 162, 50, 1)');
        var gradientLegendRed7 = 'linear-gradient(to right, rgba(124, 162, 50, 1), rgba(124, 162, 150, 1))';

        var gradientStrokeRed8 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed8.addColorStop(0, 'rgba(84, 52, 150, 1)');
        gradientStrokeRed8.addColorStop(1, 'rgba(240, 92, 150, 1)');
        var gradientLegendRed8 = 'linear-gradient(to right, rgba(84, 52, 150, 1), rgba(240, 92, 150, 1))';

        var gradientStrokeRed9 = ctx.createLinearGradient(0, 0, 0, 300);
        gradientStrokeRed9.addColorStop(0, 'rgba(244, 92, 50, 1)');
        gradientStrokeRed9.addColorStop(1, 'rgba(124, 122, 80, 1)');
        var gradientLegendRed9 = 'linear-gradient(to right, rgba(244, 92, 50, 1), rgba(124, 122, 80, 1))';

        var legendColor_array = [gradientLegendRed, gradientLegendViolet, gradientLegendBlue, gradientLegendRed2, gradientLegendRed3, gradientLegendRed4, gradientLegendRed5, gradientLegendRed6, gradientLegendRed7, gradientLegendRed8, gradientLegendRed9];
        var strokeColor_array = [gradientStrokeRed, gradientStrokeViolet, gradientStrokeBlue, gradientStrokeRed2, gradientStrokeRed3, gradientStrokeRed4, gradientStrokeRed5, gradientStrokeRed6, gradientStrokeRed7, gradientStrokeRed8, gradientStrokeRed9];

        var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: response.months,
              datasets: [
                {
                  label: "Applicants",
                  borderColor: strokeColor_array,
                  backgroundColor: strokeColor_array,
                  hoverBackgroundColor: strokeColor_array,
                  legendColor: legendColor_array,
                  pointRadius: 0,
                  fill: false,
                  borderWidth: 1,
                  fill: 'origin',
                  data: response.applicants
                }
            ]
          },
          options: {
            responsive: true,
            legend: false,
            legendCallback: function(chart) {
              var text = []; 
              text.push('<ul>'); 
              for (var i = 0; i < chart.data.datasets.length; i++) { 
                  text.push('<li><span style="background:' + 
                             chart.data.datasets[i].legendColor + 
                             '"></span>'); 
                  if (chart.data.datasets[i].label) { 
                      text.push(chart.data.datasets[i].label); 
                  } 
                  text.push('</li>'); 
              } 
              text.push('</ul>'); 
              return text.join('');
            },
            scales: {
                yAxes: [{
                    ticks: {
                        display: true,
                        min: 0,
                        stepSize: 20,
                        max: response.max
                    },
                    gridLines: {
                      drawBorder: false,
                      color: 'rgba(235,237,242,1)',
                      zeroLineColor: 'rgba(235,237,242,1)'
                    }
                }],
                xAxes: [{
                    gridLines: {
                      display:false,
                      drawBorder: false,
                      color: 'rgba(0,0,0,1)',
                      zeroLineColor: 'rgba(235,237,242,1)'
                    },
                    ticks: {
                        padding: 20,
                        fontColor: "#9c9fa6",
                        autoSkip: true,
                    },
                    categoryPercentage: 0.5,
                    barPercentage: 0.5
                }]
              }
            },
            elements: {
              point: {
                radius: 0
              }
            }
        })
        $("#visit-sale-chart-legend").html(myChart.generateLegend());
      }

    }
    
    if ($("#serviceSaleProgress").length) {
      var bar = new ProgressBar.Circle(serviceSaleProgress, {
        color: 'url(#gradient)',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 8,
        trailWidth: 8,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: false
        },
        from: { color: '#aaa', width: 6 },
        to: { color: '#57c7d4', width: 6 }
      });

      bar.animate(.65);  // Number from 0.0 to 1.0
      bar.path.style.strokeLinecap = 'round';
      let linearGradient = '<defs><linearGradient id="gradient" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse"><stop offset="20%" stop-color="#da8cff"/><stop offset="50%" stop-color="#9a55ff"/></linearGradient></defs>';
      bar.svg.insertAdjacentHTML('afterBegin', linearGradient);
    }
    if ($("#productSaleProgress").length) {
      var bar = new ProgressBar.Circle(productSaleProgress, {
        color: 'url(#productGradient)',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 8,
        trailWidth: 8,
        easing: 'easeInOut',
        duration: 1400,
        text: {
          autoStyleContainer: false
        },
        from: { color: '#aaa', width: 6 },
        to: { color: '#57c7d4', width: 6 }
      });

      bar.animate(.6);  // Number from 0.0 to 1.0
      bar.path.style.strokeLinecap = 'round';
      let linearGradient = '<defs><linearGradient id="productGradient" x1="0%" y1="0%" x2="100%" y2="0%" gradientUnits="userSpaceOnUse"><stop offset="40%" stop-color="#36d7e8"/><stop offset="70%" stop-color="#b194fa"/></linearGradient></defs>';
      bar.svg.insertAdjacentHTML('afterBegin', linearGradient);
    }
    if ($("#points-chart").length) {
      var ctx = document.getElementById('points-chart').getContext("2d");

      var gradientStrokeViolet = ctx.createLinearGradient(0, 0, 0, 181);
      gradientStrokeViolet.addColorStop(0, 'rgba(218, 140, 255, 1)');
      gradientStrokeViolet.addColorStop(1, 'rgba(154, 85, 255, 1)');

      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: [1, 2, 3, 4, 5, 6, 7, 8],
              datasets: [
                {
                  label: "North Zone",
                  borderColor: gradientStrokeViolet,
                  backgroundColor: gradientStrokeViolet,
                  hoverBackgroundColor: gradientStrokeViolet,
                  pointRadius: 0,
                  fill: false,
                  borderWidth: 1,
                  fill: 'origin',
                  data: [20, 40, 15, 35, 25, 50, 30, 20]
                },
                {
                  label: "South Zone",
                  borderColor: '#e9eaee',
                  backgroundColor: '#e9eaee',
                  hoverBackgroundColor: '#e9eaee',
                  pointRadius: 0,
                  fill: false,
                  borderWidth: 1,
                  fill: 'origin',
                  data: [40, 30, 20, 10, 50, 15, 35, 20]
                }
            ]
          },
          options: {
              legend: {
                  display: false
              },
              scales: {
                  yAxes: [{
                      ticks: {
                          display: false,
                          min: 0,
                          stepSize: 10
                      },
                      gridLines: {
                        drawBorder: false,
                        display: false
                      }
                  }],
                  xAxes: [{
                      gridLines: {
                        display:false,
                        drawBorder: false,
                        color: 'rgba(0,0,0,1)',
                        zeroLineColor: '#eeeeee'
                      },
                      ticks: {
                          padding: 20,
                          fontColor: "#9c9fa6",
                          autoSkip: true,
                      },
                      barPercentage: 0.7
                  }]
                }
              },
              elements: {
                point: {
                  radius: 0
                }
              }
            })
    }
    if ($("#events-chart").length) {
      var ctx = document.getElementById('events-chart').getContext("2d");

      var gradientStrokeBlue = ctx.createLinearGradient(0, 0, 0, 181);
      gradientStrokeBlue.addColorStop(0, 'rgba(54, 215, 232, 1)');
      gradientStrokeBlue.addColorStop(1, 'rgba(177, 148, 250, 1)');

      var myChart = new Chart(ctx, {
          type: 'bar',
          data: {
              labels: [1, 2, 3, 4, 5, 6, 7, 8],
              datasets: [
                {
                  label: "Domestic",
                  borderColor: gradientStrokeBlue,
                  backgroundColor: gradientStrokeBlue,
                  hoverBackgroundColor: gradientStrokeBlue,
                  pointRadius: 0,
                  fill: false,
                  borderWidth: 1,
                  fill: 'origin',
                  data: [20, 40, 15, 35, 25, 50, 30, 20]
                },
                {
                  label: "International",
                  borderColor: '#e9eaee',
                  backgroundColor: '#e9eaee',
                  hoverBackgroundColor: '#e9eaee',
                  pointRadius: 0,
                  fill: false,
                  borderWidth: 1,
                  fill: 'origin',
                  data: [40, 30, 20, 10, 50, 15, 35, 20]
                }
            ]
          },
          options: {
              legend: {
                  display: false
              },
              scales: {
                  yAxes: [{
                      ticks: {
                          display: false,
                          min: 0,
                          stepSize: 10
                      },
                      gridLines: {
                        drawBorder: false,
                        display: false
                      }
                  }],
                  xAxes: [{
                      gridLines: {
                        display:false,
                        drawBorder: false,
                        color: 'rgba(0,0,0,1)',
                        zeroLineColor: '#eeeeee'
                      },
                      ticks: {
                          padding: 20,
                          fontColor: "#9c9fa6",
                          autoSkip: true,
                      },
                      barPercentage: 0.7
                  }]
                }
              },
              elements: {
                point: {
                  radius: 0
                }
              }
            })
    }
    
    
    if ($("#inline-datepicker").length) {
      $('#inline-datepicker').datepicker({
        enableOnReadonly: true,
        todayHighlight: true,
      });
    }
  });
})(jQuery);