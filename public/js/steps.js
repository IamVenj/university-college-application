$(document).ready(function(){

	$('#InputType').on('change', function(){

		var inputs = $(this).val();

		if(inputs == 'dropdown' || inputs == 'radio')
		{
			document.getElementById('how-many').style.display = 'block';
		}
		else
		{
			
			$('.check_radio_question').remove();

			document.getElementById('how-many').style.display = 'none';	
		
		}

	});

	$('#numInputs').on('change', function(){

		var inputs = $(this).val();

		$('.check_radio_question').remove();

		for(var i=0; i < inputs; i++)
		{
			
			$("#InputArea").append('<div class="form-group check_radio_question"><label for="check_radio_question_id" style="font-size: 20px;"><i class="fa fa-angle-right mr-3"></i>Add Options</label><input name="answer_inputs[]" type="text" class="form-control" id="check_radio_question_id" required/></div>');

		}

	});

	$('#drop_selected_input').on('click', function(){

		$('.check_radio_question').remove();

	});

	

});