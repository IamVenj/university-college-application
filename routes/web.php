
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
/----------------------------------------------------------------------------
/ MASTER_INDEX
/----------------------------------------------------------------------------
/
/ # Home Page
/ # Login
/ # logout
/ # Signup
/
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'welcomeController@index')->name('home');


// admission notice and available courses
Route::get('/universities', 'admissionNoticeAndCoursesController@index');
Route::get('/universities/{id}', 'admissionNoticeAndCoursesController@show');


// login
Route::get('/login', 'loginController@index')->name('login');
Route::post('/login', 'loginController@store');


// logout
Route::get('/logout', 'loginController@destroy');



// signup
Route::get('/signup', 'signupController@index');
Route::post('/signup', 'signupController@store');


// reset-password
Route::get('/forgot-password', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');

Route::get('/reset-password/{token}', 'Auth\ResetPasswordController@showResetForm')->name('PasswordReset');
Route::post('/reset-password', 'Auth\ResetPasswordController@reset');



/*
/----------------------------------------------------------------------------
/ Verify Email
/----------------------------------------------------------------------------
/
/ # email verification - if verification successfull redirect to first time login page
/
/
*/


Route::get('/verify/{token}', 'VerifyController@verify')->name('verify_email');



/*
/----------------------------------------------------------------------------
/ INDEX
/----------------------------------------------------------------------------
/
/ # index/Home
/ # First Time Login
/ # Dashboard
/ # Create Academics
/ # Create Admin
/ # Create Employee
/ # Create University
/ # Create Role
/ # Create Information Post
/ # Admin Control Panel
/ # Employee Control Panel
/ # Add Steps
/ # create step-one
/ # create step-two
/ # create step-three
/ # create step-four
/ # create step-five
/ # step-one
/ # step-two
/ # step-three
/ # step-four
/ # step-five
/ # students
/ # my-info / personal information
/ # education
/ # family
/ # activities
/ # writing
/ # change-university
/ # rejected
/ # accepted
/ # submit all forms
/ # uppload document
/ # not allowed to access
/
*/

// profile
Route::get('/profile', 'ProfileController@index');
Route::patch('/profile/{id}', 'ProfileController@update');

// first time login
Route::get('/info', 'firstTimeController@index');
Route::patch('/info/{id}', 'firstTimeController@update');
Route::post('/info/fetch', 'firstTimeController@fetch');

// change-password
Route::get('/change-password', 'ChangePasswordController@create');
Route::post('/change-password', 'ChangePasswordController@update');


// dashboard
Route::get('/dashboard', 'dashboardController@index');
Route::get('/dashboard/fetch', 'dashboardController@getApplicantsDataPerMonth');
Route::get('/dashboard/gender', 'dashboardController@MaleAndFemaleApplicantsComparison');

// create academics
Route::get('/create-academics', 'academicsController@create');
Route::post('/create-academics', 'academicsController@store');
Route::delete('/create-academics/{id}', 'academicsController@destroy');
Route::patch('/create-academics/{id}', 'academicsController@update');

// create courses
Route::get('/create-course', 'CourseController@create');
Route::post('/create-course', 'CourseController@store');
Route::delete('/create-course/{id}', 'CourseController@destroy');
Route::patch('/create-course/{id}', 'CourseController@update');


// create admin and employees
// admin users are created by super admin && employees are created by the university admins
Route::get('/create-admin', 'CreateAdminUserController@create');
Route::post('/create-admin', 'CreateAdminUserController@store');


Route::get('/create-employee', 'CreateEmployeeController@create');
Route::post('/create-employee', 'CreateEmployeeController@store');
Route::delete('/create-employee/{id}', 'CreateEmployeeController@destroy');
Route::patch('/create-employee/{id}', 'CreateEmployeeController@update');


// create Universities
Route::get('/create-university', 'UniversitiesController@create');
Route::post('/create-university', 'UniversitiesController@store');
Route::delete('/create-university/{id}', 'UniversitiesController@destroy');
Route::patch('/create-university/{id}', 'UniversitiesController@update');


// create role
Route::get('/create-role', 'RoleController@create');
Route::post('/create-role', 'RoleController@store');


// create information post
Route::get('/post', 'InformationPostsController@create');
Route::post('/post', 'InformationPostsController@store');
Route::patch('/post/{id}', 'InformationPostsController@update');
Route::delete('/post/{id}', 'InformationPostsController@destroy');


// adminControlPanel
Route::get('/admin-panel', 'AdminPanelController@index');
Route::delete('/admin-panel/{id}', 'AdminPanelController@destroy');
Route::patch('/admin-panel/reset/{id}', 'AdminPanelController@password_reset');
Route::patch('/admin-panel/activate/{id}', 'AdminPanelController@activate');
Route::patch('/admin-panel/deactivate/{id}', 'AdminPanelController@deactivate');
Route::patch('/admin-panel/change-university/{id}', 'AdminPanelController@change_university');

// employeeControlPanel
Route::get('/employee-panel', 'EmployeePanelController@index');
Route::delete('/employee-panel/{id}', 'EmployeePanelController@destroy');
Route::patch('/employee-panel/reset/{id}', 'EmployeePanelController@password_reset');
Route::patch('/employee-panel/activate/{id}', 'EmployeePanelController@activate');
Route::patch('/employee-panel/deactivate/{id}', 'EmployeePanelController@deactivate');

// Add Steps
Route::get('/add-steps', 'StepQuantityController@index');
Route::post('/add-steps', 'StepQuantityController@store');

// create step-one
Route::get('/create-step1', 'Create_Step_1_Controller@create');
Route::post('/create-step1', 'Create_Step_1_Controller@store');
Route::patch('/create-step1/{id}', 'Create_Step_1_Controller@update');
Route::delete('/create-step1/{id}', 'Create_Step_1_Controller@destroy');
Route::post('/create-step1/fetch', 'Create_Step_1_Controller@fetch');

// create step-two
Route::get('/create-step2', 'Create_Step_2_Controller@create');
Route::post('/create-step2', 'Create_Step_2_Controller@store');
Route::patch('/create-step2/{id}', 'Create_Step_2_Controller@update');
Route::delete('/create-step2/{id}', 'Create_Step_2_Controller@destroy');
Route::post('/create-step2/fetch', 'Create_Step_1_Controller@fetch');

// create step-three
Route::get('/create-step3', 'Create_Step_3_Controller@create');
Route::post('/create-step3', 'Create_Step_3_Controller@store');
Route::patch('/create-step3/{id}', 'Create_Step_3_Controller@update');
Route::delete('/create-step3/{id}', 'Create_Step_3_Controller@destroy');
Route::post('/create-step3/fetch', 'Create_Step_1_Controller@fetch');

// create step-four
Route::get('/create-step4', 'Create_Step_4_Controller@create');
Route::post('/create-step4', 'Create_Step_4_Controller@store');
Route::patch('/create-step4/{id}', 'Create_Step_4_Controller@update');
Route::delete('/create-step4/{id}', 'Create_Step_4_Controller@destroy');
Route::post('/create-step4/fetch', 'Create_Step_1_Controller@fetch');

// step-one
Route::get('/step-one', 'Step_1_Controller@index');
Route::post('/step-one', 'Step_1_Controller@store');
Route::patch('/step-one/{id}', 'Step_1_Controller@update');
Route::delete('/step-one/{id}', 'Step_1_Controller@destroy');

// step-two
Route::get('/step-two', 'Step_2_Controller@index');
Route::post('/step-two', 'Step_2_Controller@store');
Route::patch('/step-two/{id}', 'Step_2_Controller@update');
Route::delete('/step-two/{id}', 'Step_2_Controller@destroy');

// step-three
Route::get('/step-three', 'Step_3_Controller@index');
Route::post('/step-three', 'Step_3_Controller@store');
Route::patch('/step-three/{id}', 'Step_3_Controller@update');
Route::delete('/step-three/{id}', 'Step_3_Controller@destroy');

// step-four
Route::get('/step-four', 'Step_4_Controller@index');
Route::post('/step-four', 'Step_4_Controller@store');
Route::patch('/step-four/{id}', 'Step_4_Controller@update');
Route::delete('/step-four/{id}', 'Step_4_Controller@destroy');

// students
Route::get('/students', 'StudentsController@index');
Route::get('/students/{id}', 'StudentsController@show');
Route::get('/students/education-details/{id}', 'StudentsController@show_education');
Route::get('/students/fam-info/{id}', 'StudentsController@show_fam');
Route::get('/students/activity/{id}', 'StudentsController@show_activity');
Route::get('/students/writing/{id}', 'StudentsController@show_writing');
Route::get('/students/step-one/{id}', 'StudentsController@show_step_one');
Route::get('/students/step-two/{id}', 'StudentsController@show_step_two');
Route::get('/students/step-three/{id}', 'StudentsController@show_step_three');
Route::get('/students/step-four/{id}', 'StudentsController@show_step_four');
Route::get('/students/doc/{document}', 'StudentsController@download_doc');


// my info
Route::get('/my-info', 'PersonalInformationController@index');
Route::patch('/my-info/{id}', 'PersonalInformationController@update');

// education
Route::get('/education', 'EducationController@index');
Route::post('/education/fetch', 'EducationController@fetch');
Route::patch('/education/{id}', 'EducationController@update');
Route::patch('/education/highschool/{id}', 'EducationController@update_highschool');
Route::delete('/education/highschool/{id}', 'EducationController@destroy_highschool');
Route::patch('/education/cbo/{id}', 'EducationController@update_cbo');
Route::delete('/education/cbo/{id}', 'EducationController@destroy_cbo');
Route::patch('/education/honor/{id}', 'EducationController@update_honor');
Route::delete('/education/honor/{id}', 'EducationController@destroy_honor');

// family
Route::get('/fam', 'FamilyController@index');
Route::post('/fam', 'FamilyController@store');
Route::patch('/fam/child/{id}', 'FamilyController@update_child');
Route::delete('/fam/child/{id}', 'FamilyController@destroy_child');
Route::patch('/fam/sibling/{id}', 'FamilyController@update_sibling');
Route::delete('/fam/sibling/{id}', 'FamilyController@destroy_sibling');


// activities
Route::get('/activities', 'ActivityController@index');
Route::post('/activities/{id}', 'ActivityController@store');
Route::patch('/activities/{id}', 'ActivityController@update');
Route::delete('/activities/{id}', 'ActivityController@destroy');

// writing
Route::get('/writing', 'WritingController@index');
Route::post('/writing/{id}', 'WritingController@store');

// change-university
Route::get('/change-university', 'UniversityMembershipController@show');
Route::patch('/change-university/{id}', 'UniversityMembershipController@update');


// rejected
Route::get('/rejected', 'RejectedController@index');
Route::post('/students/reject/{id}', 'RejectedController@reject');
Route::get('/students/unreject/{id}', 'RejectedController@unreject');

// Accepted
Route::get('/accepted', 'AcceptedController@index');
Route::post('/students/accept/{id}', 'AcceptedController@accept');
Route::get('/students/unaccept/{id}', 'AcceptedController@unaccept');


// submit all
Route::post('/submit', 'SubmissionController@store');

// upload document
Route::get('/upload-doc', 'UploadDocController@index');
Route::post('/upload-doc', 'UploadDocController@store');
Route::delete('/upload-doc/{id}', 'UploadDocController@destroy');

// after submission
Route::get('/cannot-access', function(){
	$this->middleware('auth');
	return view('main_layout.error.after_submission_error');
});

Route::get('/institution', 'AnswerController@index');
Route::get('/institution/{id}', 'AnswerController@show');
Route::get('/institution/{university_id}/step-one/{id}', 'AnswerController@show_step_one');
Route::get('/institution/{university_id}/step-two/{id}', 'AnswerController@show_step_two');
Route::get('/institution/{university_id}/step-three/{id}', 'AnswerController@show_step_three');
Route::get('/institution/{university_id}/step-four/{id}', 'AnswerController@show_step_four');
